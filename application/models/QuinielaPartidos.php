<?php
/** @Entity */
//require "Doctrine/Common/Collections/ArrayCollection.php";
/** @Entity
 * @Table(name="QuinielaPartidos")
 * 
*/
class Default_Model_QuinielaPartidos{

   /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    
    
    /**
     * @ManyToOne(targetEntity="Default_Model_Quiniela",inversedBy="Quinielapartidos")
     * @JoinColumn(name="idQuiniela",referencedColumnName="id")
     */
    private $Quiniela;
    
    
    /**
     * @ManyToOne(targetEntity="Default_Model_Partido",inversedBy="Quinielapartidos")
     * @JoinColumn(name="idPartido",referencedColumnName="id")
     */
    private $Partido;
    
    
    public function setPartido($partido)	{	$this->Partido=$partido;    }
    public function setQuiniela($quiniela)	{	$this->Quiniela=$quiniela;	}
    
    public function getPartido()			{	return $this->Partido;		}
    public function getQuiniela()			{	return $this->Quiniela;		}
    public function getId()					{	return $this->id;			}
    
}