<?php
/**
 * Clase FeedBack
 * @author pablo
 *
 */

class cuenta_FeedbackController extends My_Controller_Action {
	
	/**
	 * Formulario de contacto
	 */
	
	public function contactoAction(){
		
		
		if($this->getRequest()->isPost()){
			$validar = new My_Validador();
			$params = $this->getRequest()->getParams();
			
			$transport = new Zend_Mail_Transport_Smtp();
			$mail = new Zend_Mail();
			$mail->addTo('contacto@megaquinielabrasil2014.com', 'Mensaje de contacto [Feed Back]');
			$mail->setFrom($params['email'], $params['name']);
			$mail->setSubject('Equipo de Megaquinielas');
			$mail->setBodyText($params['message']);
			$mail->send($transport);
			
			$this->_helper->flashMessenger->addMessage('success | Tu mensaje se envió correctamente.');
		}

	}
	
}