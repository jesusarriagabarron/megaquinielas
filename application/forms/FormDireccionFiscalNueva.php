<?php
/**
 * Clase que crea el formulario para dar de alta un nuevo emisor en el sistema
 * @author jarriaga
 *
 */
class Application_Form_FormDireccionFiscalNueva extends Zend_Form{
	
	public $mensajes;
	
	public function init(){
		
		
			
		$this->setMethod('post');
		$this->setAttrib('style','display:none');
		$this->setAttrib('id','Formulariodireccion');
		
		$decorate = array(
				array('ViewHelper'),
				array('Description'),
				array('Errors',array('style'=>'margin:-11px 0 20px 175px!important;')),
				array('Label', array( 'separator'=>' ','placement'=>'IMPLICIT_PREPEND')),
				array('HtmlTag', array('tag' => 'div', 'placement'=>'IMPLICIT_PREPEND'))
				);
		
		
	
		/** direccion fiscal**/
		
		$calle = new Zend_Form_Element_Text('calle');
		$calle->setLabel('Calle')
		->addFilter('StringToUpper')
		->setRequired(true)->setAttrib('class', 'input-xxlarge');
		$calle->setDecorators($decorate);
		
		$numerointerior = new Zend_Form_Element_Text('numeroint');
		$numerointerior->setLabel('No. Interior (opcional)')
		->setAttrib('class', 'input-mini');
		$numerointerior->setDecorators($decorate);
		
		$numeroexterior = new Zend_Form_Element_Text('numeroext');
		$numeroexterior->setLabel('Número Exterior')
		->setAttrib('class', 'input-mini')		->setRequired(true);
		$numeroexterior->setDecorators($decorate);
		
		$colonia = new Zend_Form_Element_Text('colonia');
		$colonia->setLabel('Colonia')
		->setAttrib('class', 'input-xxlarge')
		->addFilter('StringToUpper')
		->setRequired(true);
		$colonia->setDecorators($decorate);
		
		$localidad = new Zend_Form_Element_Text('localidad');
		$localidad->setLabel('Localidad/Ciudad')
		->setAttrib('class', 'input-xxlarge')
		->addFilter('StringToUpper');
		$localidad->setDecorators($decorate);
		
		$municipio = new Zend_Form_Element_Text('municipio');
		$municipio->setLabel('Delegación/Municipio')
		->setAttrib('class', 'input-xxlarge')
		->addFilter('StringToUpper')
		->setRequired(true);
		$municipio->setDecorators($decorate);
		
		$codigopostal = new Zend_Form_Element_Text('codigopostal');
		$codigopostal->setLabel('Codigo Postal')
		->setAttrib('class', 'input-small')
		->addFilter('StringToUpper')
		->setRequired(true);
		$codigopostal->setDecorators($decorate);
		
		$estado = new Zend_Form_Element_Select('estado');
		$estado->setLabel('Estado');
		$direcciones = new Application_Model_Direcciones();
		$estados=$direcciones->getEstados();
		$edos=array();
		foreach($estados as $edo){
			$estado->addMultiOptions( array($edo['idEstado']=>$edo['cNombre']));

		}
		$estado->setDecorators($decorate);
		
		
		$this->addElements(
				array(
						
						$calle,
						$numeroexterior,
						$numerointerior,
						$colonia,
						$localidad,
						$municipio,
						$codigopostal,
						$estado
				)
		);
		
			
		$this->addDisplayGroup(array(
				$calle,
				$numeroexterior,
				$numerointerior,
				$colonia,
				$localidad,
				$municipio,
				$codigopostal,
				$estado
				), 'direccionfiscal');
		$this->getDisplayGroup('direccionfiscal')->setLegend('Dirección Fiscal');
		
		
		
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Guardar Dirección Fiscal')->setAttrib('class', 'btn btn-large');
		$decorate = array(
				array('ViewHelper'),
				array('Description'),
				array('Errors',array('style'=>'margin:-11px 0 20px 0px!important;')),
				array('HtmlTag', array('tag' => 'div', 'placement'=>'IMPLICIT_PREPEND','class'=>'text-center'))
				);
		$submit->setDecorators($decorate);
		
		$this->addElement($submit);
		
		
	}
}
