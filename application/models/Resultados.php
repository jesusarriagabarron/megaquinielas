<?php


/** @Entity 
 * @Table(name="BoletaResultados")
 * */
class Default_Model_Resultados {
	
	/**
	 * @Id
	 * @GeneratedValue(strategy="AUTO")
	 * @Column(type="integer")
	 */
	private $id;
	
	
	/**
	 * @ManyToOne(targetEntity="Default_Model_Boleta",inversedBy="resultados")
	 * @JoinColumn(name="idBoleta",referencedColumnName="id")
	 */
	
	private $boleta;
	
	
	/**
	 * @ManyToOne(targetEntity="Default_Model_Partido",inversedBy="resultadosQuinielas")
	 * @JoinColumn(name="idPartido",referencedColumnName="id")
	 */
    private $partido;
    
    
   	/** @Column(type="string",length=2)**/
    private $pronostico;
    
    public function setPronostico($pronostico){ 		$this->pronostico	=	$pronostico;    }
    public function setBoleta(Default_Model_Boleta $boleta){	$this->boleta	=	$boleta;	}
    public function setPartido(Default_Model_Partido $partido){ $this->partido	=	$partido; }
    
    public function getPronostico()		{	return $this->pronostico;	}
    public function getBoleta()			{	return $this->boleta;	}
    public function getPartido()		{	return $this->partido;	}
    public function getId()				{	return $this->id;	}
	
}