<?php

//cargamos el sdk de Conekta
require_once("conektaSDK/Conekta.php");


class My_Model_CobrosConekta	{
	
	private $_em;
	private $_auth;
	
	public function __construct(){
		$registro = Zend_Registry::getInstance();
		$this->_em	=	$registro->entitymanager;
		$auth = Zend_Auth::getInstance();
		if($auth->getStorage()->read())
			$this->_auth = get_object_vars( $auth->getStorage()->read());
	}
	
	/**
	 * asignamos el tipo de cobro y ejecutamos su funcion
	 */
	public function getCargo($chargeId){
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$ck = $config->getOption("conekta");
		
		Conekta::setApiKey($ck["private"]);
		
		$charge = Conekta_Charge::find($chargeId);
		return $charge;
	}
	

	
}