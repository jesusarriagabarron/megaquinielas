<?php
/**
 * Devuelve una cantidad en tipo de moneda
 * @author jarriaga
 *
 */

class My_Helper_Moneda extends Zend_View_Helper_Abstract{

	
	public function moneda($monto,$pais='es_MX'){
		$moneda = new Zend_Currency($pais);
		return $moneda->setValue($monto); 		
	}
}