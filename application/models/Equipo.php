<?php

/** @Entity 
 * @Table(name="Equipo")
 * */
class Default_Model_Equipo {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    /** @Column(type="string",length=100) **/
    private $nombre;
    /** @Column(type="string",length=50,nullable=true) **/
    private $nombreCorto;
    /** @Column(type="string",length=50,nullable=true) **/
    private $escudo;
    /** @Column(type="string",length=100,nullable=true) **/
    private $pais;
   	/** @Column(type="datetime",nullable=true)**/ 
   	private	$fechaCreacion;
    

    public function setNombre($nombre){						$this->nombre				=		$nombre;				}
    public function setNombreCorto($nombreCorto){			$this->nombreCorto			=		$nombreCorto;			}
    public function setEscudo($escudo){						$this->escudo				=		$escudo;				}
    public function setPais($pais){							$this->pais					=		$pais;					}
    public function setFechaCreacion(){						$this->fechaCreacion		=		new DateTime("now");	}
    
    
    public function getId(){							return 		$this->id;						}
    public function getNombre(){						return 		$this->nombre;			 		}
    public function getNombreCorto(){					return 		$this->nombreCorto;				}
    public function getEscudo(){						return 		$this->escudo;					}
    public function getPais(){							return 		$this->pais;					}
    public function getFechaCreacion(){					return 		$this->fechaCreacion;			}
    
    
}