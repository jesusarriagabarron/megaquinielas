<?php
/**
 * Esta clase controla el manejo de notificaciones de los usuarios
 * @author jarriaga
 *
 */

class My_Model_Boleta {
	private $_em;
	private $_auth;
	
	public function __construct(){
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
		
		$auth = Zend_Auth::getInstance();
		
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {$
			$this->_auth = false;
		}
	}
	
	
	
	public function crearBoleta(Default_Model_Usuario $usuario,Default_Model_Quiniela $quiniela,$pronosticos){
		try{
		$Boleta 	=		new Default_Model_Boleta();
		$Boleta->setUsuario($usuario);
		$Boleta->setStatus(0);
		$Boleta->setQuiniela($quiniela);
		$Boleta->setFechaCreaction();
		$this->_em->persist($Boleta);
		//verificar que tiene el mimsmo numero de partidos
		if( count($quiniela->getQuinielaPartidos())!=count($pronosticos) ){
			return false;
		}
		
		//verificar que los partidos coincidan con los de la quiniela
		foreach ($quiniela->getQuinielaPartidos() as $quinielaPartido){
			$partidoID = $quinielaPartido->getPartido()->getId();
			$encontrado=false;
			foreach ($pronosticos as $resultado){
					if($resultado["idPartido"]==$partidoID)
					{$encontrado=true;break;}
			}
			if(!$encontrado)
				return false;
		}

		//creamos los resultados
		foreach ($pronosticos as $resultado){
			$partido = $this->_em->find("Default_Model_Partido",$resultado["idPartido"]);
			if($partido){
				$partidoResultado 	=	new Default_Model_Resultados();
				$partidoResultado->setBoleta($Boleta);
				$partidoResultado->setPartido($partido);
				$partidoResultado->setPronostico($resultado["pronostico"]);
				$this->_em->persist($partidoResultado);
			}
		}
		
		$this->_em->flush();
		}catch(Exception $e){
			return false;
		}	
		
		return $Boleta;
	}
	
}