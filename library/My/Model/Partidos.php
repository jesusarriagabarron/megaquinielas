<?php
/**
 * Esta clase controla el manejo de partidos 
 * @author jarriaga
 *
 */

class My_Model_Partidos {
	private $_em;
	private $_auth;
	
	public function __construct(){
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
		
		$auth = Zend_Auth::getInstance();
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {
			$this->_auth = false;
		}
	}
	
	
	/**
	 * obtiene los partidos de una boleta
	 * @param unknown $resultados
	 * @return multitype:string
	 */
	public function getPartidosDeBoleta($resultados){
		$rowset		=	array();
		$rowset		=		$this->partidosresultadosArray($resultados);
		return $rowset;
	}
	
	/**
	 * Obtiene los partidos de una quiniela
	 * @param unknown $quiniela
	 * @return multitype:unknown
	 */
	public function getPartidosDeQuiniela($quiniela){
		$quinielapartidos=$quiniela->getQuinielaPartidos();
		$p=array();
		foreach($quinielapartidos as $qp){$p[]=$qp->getPartido();}
		$rowset 	=	array();
		$rowset	=	$this->partidosArray($p);
		return $rowset;
	}
	
	/**
	 * Obtiene los partidos para dar de alta los resultados
	 */
	public function getPartidosResultados(){
		$partidos	=	$this->_em->getRepository("Default_Model_Partido")->findBy(array(),array('fechaPartido'=>'ASC'));
		$rowset		=	array();
		$rowset		=	$this->partidosArray($partidos);
		return $rowset;
	}
	
	
	/**
	 * Obtiene los partidos de un evento
	 * @param unknown $idEvento
	 * @return multitype:unknown
	 */
	public function getPartidosDeEvento($idEvento){
		$partidos	=		$this->_em->getRepository("Default_Model_Partido")->findBy(array("evento"=>$idEvento));
		$rowset 	=	array();
		$rowset		=	$this->partidosArray($partidos);
		return $rowset;
	}
	
	/**
	 * Obtiene los partidos activos de un evento
	 * @param unknown $idEvento
	 * @return multitype:unknown
	 */
	public function getPartidosActivosDeEvento($idEvento){
		$partidos	=		$this->_em->getRepository("Default_Model_Partido")->findBy(array("evento"=>$idEvento));
		
		$p=array();
		foreach($partidos as $partido){
			if($partido->getActivo()==1){
				array_push($p, $partido);
			}
		}
		
		$rowset 	=	array();
		$rowset		=	$this->partidosArray($p);
		return $rowset;
	}
	
	
	
    private function partidosresultadosArray($resultados){
    	$rowset = array();
    	foreach ($resultados as $r){
    		$partido= $r->getPartido();
    		$row["activo"]	=	$partido->getActivo();
    		$row["id"]		=	$partido->getId();
    		$row["fecha"]	=	$partido->getFechaPartido();
    		$row["tipo"]	=	$partido->getTipoPartido();
    
    		$equipoL		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoLocalId());
    		$equipoLocal["nombre"]		=	$equipoL->getNombre();
    		$equipoLocal["nombreCorto"]	=	$equipoL->getNombreCorto();
    		$equipoLocal["escudo"]		=	$equipoL->getEscudo();
    
    		$equipoV		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoVisitaId());
    		$equipoVisita["nombre"]		=	$equipoV->getNombre();
    		$equipoVisita["nombreCorto"]	=	$equipoV->getNombreCorto();
    		$equipoVisita["escudo"]		=	$equipoV->getEscudo();
    
    
    		$row["equipoLocal"]		=	$equipoLocal;
    		$row["equipoVisita"]	=	$equipoVisita;
    		
    		$row["pronostico"]		=	$r->getPronostico();
    		
    		if($row["pronostico"]	 == "V" ){
    			$row['pronosticoEscudo']=$this->Escudo(  $equipoV->getEscudo(),"normal")." ".$equipoVisita["nombre"];
    		}
    		if($row["pronostico"]	 == "L" ){
    			$row["pronosticoEscudo"]=$this->Escudo( $equipoL->getEscudo(),"normal")." ".$equipoLocal["nombre"];
    		}
    		if($row["pronostico"]	 == "E" ){ $row["pronosticoEscudo"]= '<i class="fa fa-flag"></i> Empate'; }
    
    		$rowset[]	=	$row;
    	}
    	return $rowset;
    }
    
    private function partidosArray($partidos){
    	$rowset = array();
    	foreach ($partidos as $partido){
    		$row["activo"]				=	$partido->getActivo();
    		$row["id"]					=	$partido->getId();
    		$row["fecha"]				=	$partido->getFechaPartido();
    		$row["tipo"]				=	$partido->getTipoPartido();
    		$row["resultadoPartido"]	=	$partido->getResultadoPartido();
    		$row["golesLocal"]			=	$partido->getGolesLocal();
    		$row["golesVisita"]			=	$partido->getGolesVisita();
    		
    
    		$equipoL		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoLocalId());
    		$equipoLocal["nombre"]		=	$equipoL->getNombre();
    		$equipoLocal["nombreCorto"]	=	$equipoL->getNombreCorto();
    		$equipoLocal["escudo"]		=	$equipoL->getEscudo();
    
    		$equipoV		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoVisitaId());
    		$equipoVisita["nombre"]		=	$equipoV->getNombre();
    		$equipoVisita["nombreCorto"]	=	$equipoV->getNombreCorto();
    		$equipoVisita["escudo"]		=	$equipoV->getEscudo();
    
    
    		$row["equipoLocal"]		=	$equipoLocal;
    		$row["equipoVisita"]	=	$equipoVisita;
    
    		$rowset[]	=	$row;
    	}
    	return $rowset;
    }
	
    public function Escudo($filename,$tipo=""){
		switch ($tipo){
			case "small":
						$img	=	"<img src='/img/flags/icon/".$filename."' style='border: 1px #ddd solid;'>";
						break;
			case "normal":
						$img	=	"<img src='/img/flags/".$filename."' style='width:20px;height:auto;border: 1px #ddd solid;'>";
						break;
			
		}
		return $img;
	}
	
	
	/**
	 * Obtiene el resultado y pronosticos de los partidos
	 * @param object $quiniela
	 */
	
	public function getPronosticos($quiniela){
		 
		 
	
		$resultados = array();
		 
		//$partidos   = $quiniela->getQuinielaPartidos();
		$partidos   = $this->getPartidosqb($quiniela);
		$boletas    = $quiniela->getobjBoletas();
		$cpartidos  = 0;
	
	
		foreach($partidos as $partido){
	
			$resultados[$cpartidos]['idPartido']     = $partido['id'];
			$resultados[$cpartidos]['fecha']         = $partido['fechaPartido'];
			$resultados[$cpartidos]['escudo-local']  = $partido['escudoLocal'];
			$resultados[$cpartidos]['equipo-local']  = $partido['equipoLocal'];
			$resultados[$cpartidos]['equipo-visita'] = $partido['equipoVisitante'];
			$resultados[$cpartidos]['escudo-visita'] = $partido['escudoVisitante'];
			$resultados[$cpartidos]['resultado']     = $partido['resultadoPartido'];
	
	
	
			$equipoL		=	$this->_em->find("Default_Model_Equipo",$partido['equipoLocalId']);
			$resultados[$cpartidos]['equipo-local']  		=	$equipoL->getNombre();
			$resultados[$cpartidos]['escudo-local']	=	$this->Escudo($equipoL->getEscudo(),"normal");
	
			$equipoV		=	$this->_em->find("Default_Model_Equipo",$partido['equipoVisitanteId']);
			$resultados[$cpartidos]['equipo-visita']	=	$equipoV->getNombre();
			$resultados[$cpartidos]['escudo-visita']	=	$this->Escudo($equipoV->getEscudo(),"normal");
	
			$cboleta = 0;
			$resultados[$cpartidos]['boleta'][$cboleta]['aciertos']=0;
			foreach($boletas as $boleta) {
				if($boleta->getStatus()){
					$pronostico = $this->_em->getRepository("Default_Model_Resultados")->findBy(array("partido"=>$partido['id'], "boleta"=>$boleta));
					$resultados[$cpartidos]['boleta'][$cboleta]['idBoleta']     = $boleta->getId();
					$resultados[$cpartidos]['boleta'][$cboleta]['statusBoleta'] = $boleta->getStatus();
					$resultados[$cpartidos]['boleta'][$cboleta]['pronostico']   = (count($pronostico))? $pronostico[0]->getPronostico(): 'N';
					$resultados[$cpartidos]['boleta'][$cboleta]['idUsuario']    = $boleta->getUsuario()->getId();
					$resultados[$cpartidos]['boleta'][$cboleta]['facebookid']   = $boleta->getUsuario()->getfacebookid();
					$resultados[$cpartidos]['boleta'][$cboleta]['usuario']      = $boleta->getUsuario()->getNombreUsuario();
					$resultados[$cpartidos]['boleta'][$cboleta]['aciertos']=$boleta->getTotalAciertos() ;
					$cboleta++;
				}
			}
	
			$cpartidos++;
		}
		 
		return $resultados;
	}
	 
	 
	 
	private function getPartidosqb($quiniela){
		$query = $this->_em->createQuery("SELECT pq, p.id, p.fechaPartido, p.escudoLocal, p.equipoLocal,
   												 p.equipoVisitante, p.escudoVisitante, p.resultadoPartido,
   												 p.equipoLocalId, p.equipoVisitanteId
   										  FROM Default_Model_QuinielaPartidos pq JOIN pq.Partido p
   										  WHERE pq.Quiniela = '".$quiniela->getId()."'
   										  ORDER BY p.fechaPartido");
		return  $query->getResult();
	}
	
}