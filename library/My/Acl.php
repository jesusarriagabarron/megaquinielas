<?php
/**
 * Creación de ACL para permisos 
 * @author jarriaga
 *
 */
class My_Acl extends Zend_Acl{
	
	public function __construct(){
		//creamos los roles 
		$this->addRole(new Zend_Acl_Role('guest'));
		$this->addRole(new Zend_Acl_Role('user'),'guest');
		$this->addRole(new Zend_Acl_Role('admin'),'user');
		
		//Creamos los recursos
		$this->add(new Zend_Acl_Resource('cuenta-index'));
		$this->add(new Zend_Acl_Resource('cuenta-inbox'));
		$this->add(new Zend_Acl_Resource('cuenta-espaciopublicitario'));
		$this->add(new Zend_Acl_Resource('cuenta-ventas'));
		$this->add(new Zend_Acl_Resource('cuenta-notificaciones'));
		$this->add(new Zend_Acl_Resource('cuenta-compras'));
		
		$this->add(new Zend_Acl_Resource('quiniela-boleta'));
		$this->add(new Zend_Acl_Resource('quiniela-accion'));
		$this->add(new Zend_Acl_Resource('quiniela-detalle'));
		$this->add(new Zend_Acl_Resource('quiniela-invitacion'));
		
		$this->add(new Zend_Acl_Resource('default-index'));
		$this->add(new Zend_Acl_Resource('default-mostrarpubs'));
		$this->add(new Zend_Acl_Resource('default-error'));
		$this->add(new Zend_Acl_Resource('users-index'));
		$this->add(new Zend_Acl_Resource('users-registro'));
		$this->add(new Zend_Acl_Resource('users-perfil'));
		$this->add(new Zend_Acl_Resource('schedule-index'));
		$this->add(new Zend_Acl_Resource('admin-evento'));
		$this->add(new Zend_Acl_Resource('admin-partidos'));
		$this->add(new Zend_Acl_Resource('cuenta-perfil'));
		$this->add(new Zend_Acl_Resource('cuenta-chars'));
		$this->add(new Zend_Acl_Resource('cuenta-feedback'));
		
		//para megaquinielas
		$this->add(new Zend_Acl_Resource('cuenta-control'));
		$this->add(new Zend_Acl_Resource('admin-control'));
		
		
		//Creamos los permisos
		//permisos invitado
		$this->allow('guest','default-index');
		$this->allow('guest','default-error');
		$this->allow('guest','users-registro');
		$this->allow('guest','users-perfil','publico');
		$this->allow('guest','quiniela-detalle','quiniela');
		$this->allow('guest','cuenta-feedback');
		
		//permisos de usuario
		$this->allow('user','cuenta-index');
		$this->allow('user','cuenta-perfil');
		$this->allow('user','users-perfil');
		$this->allow('user','cuenta-espaciopublicitario');
		$this->allow('user','cuenta-ventas');
		$this->allow('user','quiniela-detalle');
		$this->allow('user','quiniela-invitacion');
		$this->allow('user','quiniela-boleta');
		$this->allow('user','quiniela-accion');
		$this->allow('user','cuenta-control');
		$this->allow('user','cuenta-compras');
		$this->allow('user','cuenta-inbox');
		$this->allow('user','cuenta-notificaciones');
		$this->allow('user','schedule-index');
		$this->allow('user','cuenta-chars');
		$this->allow('user','cuenta-feedback');
		
		//permisos de Administrador
		$this->allow('admin','admin-control');
		$this->allow('admin','admin-evento');
		$this->allow('admin','admin-partidos');
		
	}
	
}