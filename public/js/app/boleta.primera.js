
$(document).ready(function(){

	$('.result').click(function(){
			$(this).find("input").prop("checked",true);
			var radio = $(this).find("input");
			var nombre 	=	radio.attr("name");
			var inputs  =   $("input[name='"+nombre+"']");
			inputs.each(function(){
				var parent 	=	$(this).parentsUntil("tr",".result");
				parent.removeClass("tdselected");
				});
			$(this).addClass("tdselected");
			
		});
	
	$('.radio').click(function(){
				var nombre 	=	$(this).attr("name");
				var inputs  =   $("input[name='"+nombre+"']");
				inputs.each(function(){
					var parent 	=	$(this).parentsUntil("tr",".result");
					parent.removeClass("tdselected");
					});
				var parent 	=	$(this).parentsUntil("tr",".result");
				parent.addClass("tdselected");
		});
	
	
	$('#formBoleta').submit(function(event){
		$("#error").hide();
		var error=0;
		$('#btnPagar').button('loading');
		var partidosQuinielas 	=	$('.partidosQuinielas');
		partidosQuinielas.each(function(){
			var idp = $(this).attr('id');
			if(!$('input[name="ps'+idp+'"]:checked').val()){
				error=1;
			}
		});
		
		if(error){
			 event.preventDefault();
			$("#error").show();
			$('#btnPagar').button('reset');
		}else{
			
		}
		
	});
	
	
});