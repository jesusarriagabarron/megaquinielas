<?php
/**
 * Devuelve la fecha en formato
 * @author jarriaga
 *
 */

class My_Helper_Imagenprivada extends Zend_View_Helper_Abstract{

	public function Imagenprivada($filename,$tipo='normal'){
		$medida="";
		if($tipo!='normal')
			$medida="/size/thumb";
		if($filename){
			$imagen = "<img src='/cuenta/index/image/name/".$filename.$medida."'>";
		}else{
			$imagen =  "No hay ninguna imagen para esta mención";
		}
		return $imagen; 		
	}
}