<?php
/** @Entity
 * @Table(name="CatalogoNotificacion")
 * 
*/
class Default_Model_CatalogoNotificacion{

    /**
    * @Id
    * @Column(type="integer")
    */
    private $id;
    
    /** @Column(type="string",length=100) **/
    private $descripcionNotificacion;
    
    /** @Column(type="string",length=100) **/
    private $clase;
    
    /** @Column(type="string",length=100) **/
    private $metodo;
    
    /** @Column(type="string",length=100,nullable=true) **/
    private $icono;
    
    /**
     * @OneToMany(targetEntity="Default_Model_Notificacion",mappedBy="catalogoNotificacion")
     **/
    private $notificacion;

    // SETS
    public function setId($id){
    	$this->id	=	$id;
    }
    
    public function setDescripcion($descripcion){
    	$this->descripcionNotificacion	=	$descripcion;
    }
    
    public function setMetodo($metodo){
    	$this->metodo =	$metodo;
    }
    
    public function setClase($clase){
    	$this->clase	=	$clase;
    }
    
    public function setIcono($icono){
    	$this->icono 	=	$icono;
    }

    // GETS
    public function getId(){
    	return $this->id;
    }
    
    public function getDescripcion(){
    	return $this->descripcionNotificacion;
    }
    
    public function getMetodo(){
    	return $this->metodo;
    }
    
    public function getClase(){
    	return $this->clase;
    }
    
    public function getIcono(){
    	return $this->icono;
    }
}