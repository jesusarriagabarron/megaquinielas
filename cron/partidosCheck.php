<?php

defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));



class partidosCheck {

	private $_mysql;
	private $_desarrollo=true;


	public function conectarbase(){
		if($this->_desarrollo)
			$this->_mysql	=	new mysqli("127.0.0.1", "root", "root", "megaquinielasDB");
		else
			$this->_mysql	=	new mysqli("megaquinielas.db.9633276.hostedresource.com", "megaquinielas", "m3gAq1n3!as", "megaquinielas");
	}


	public function __construct(){
		$this->conectarbase();

		if($this->_mysql->connect_errno){
			throw new Exception("Error al conectar la base ".$this->_mysql->error);
		}
	}
	
	public function getPartidosActivos(){
		$sql	=	"	SELECT  		p.id, p.fechaPartido, p.activo
						FROM 			Partido p
						WHERE			p.activo=1
					";
		
		$partidos		=		$this->_mysql->query($sql);
		return $partidos;
	}
	
	public function getQuinielasActivas(){
		
		$fecha=new DateTime('now');
		$fecha->sub(new DateInterval('PT24H'));
		$date	=	$fecha->format("Y-m-d H:i:s");
		$sql	=	"
						SELECT 		id 	
						FROM 		Quiniela
						WHERE		fechaCierre<='".$date."'		
					";
		$quinielas	=	$this->_mysql->query($sql);
		return $quinielas;
	}
	
	public function PartidosPorIniciar(){
		
		$quinielas	=	$this->getQuinielasActivas();
		$quinielas->data_seek(0);
		//actualizamos las quinielas por status en progreso

		while($quiniela	=	$quinielas->fetch_assoc()){
				$sql	=	"	UPDATE 	Quiniela
								SET 	status=0
								WHERE	id='{$quiniela["id"]}'
								LIMIT	1";
				$this->_mysql->query($sql);
		}
		
		
		$partidos 	=	$this->getPartidosActivos();
		
		$fecha = new DateTime('NOW');
		$fecha->setTimeZone(new DateTimeZone("America/Mexico_city"));
		
		$fecha->sub(new DateInterval('PT24H'));
		
		$tmMenos1hora =	$fecha->getTimestamp();
		$partidos->data_seek(0);
		
		
		//recorremos los pagos
		while($partido	=	$partidos->fetch_assoc()){
			$fecha=new DateTime($partido["fechaPartido"]);
			$fecha->sub(new DateInterval('PT24H'));
			$tmMenos1hora =	$fecha->getTimestamp();

			$fActual = new DateTime('now');
			$tmActual	=	$fActual->getTimestamp();
			//si falta menos de una hora para iniciar el partido
			//entonces se desactiva el partido
			if($tmActual>= $tmMenos1hora){
				$sql	=	"	UPDATE 	Partido
				SET		activo=0
				WHERE	id={$partido["id"]}
				LIMIT	1";
				$this->_mysql->query( $sql );
			}
		}
	}
	
}


$partidos 	= new partidosCheck();
$partidos->PartidosPorIniciar();
