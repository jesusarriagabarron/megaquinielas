<?php
/** @Entity
 * @Table(name="Notificacion")
 * 
*/
class Default_Model_Notificacion{

    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    
    /** @Column(type="string",length=100) **/
    private $descripcion;
    
   /**
    * @ManyToOne(targetEntity="Default_Model_Usuario",inversedBy="notificaciones")
    * @JoinColumn(name="idUsuario",referencedColumnName="id")
    */
    private $usuario;
    
    /** RELACION Muchos a uno unidireccional
     * @ManyToOne(targetEntity="Default_Model_CatalogoNotificacion",inversedBy="notificacion")
     **/
    private $catalogoNotificacion;
    
    /** @Column(type="boolean")**/
    private $visto;
    
    /** @Column(type="datetime")  **/
    private $fecha;
    
    /** @Column(type="array") **/
    private $params;
    
    /** @Column(type="string",length=100, nullable=true) **/
    private $idquiniela;
    
    /** @Column(type="integer", nullable=true) **/
    private $idsolicitante;

    // SETS
    public function setFechaNotificacion()        { $this->fecha = new DateTime("now"); }
    public function setVisto($visto=0)            { $this->visto	=	$visto; }
    public function setDescripcion($descripcion)  { $this->descripcion	=	$descripcion; }
    public function setNotificarAlUsuario(Default_Model_Usuario $usuario){ $this->usuario = $usuario; }
    public function setCatalogoNotificacion(Default_Model_CatalogoNotificacion $catalogoNotificacion){ $this->catalogoNotificacion = $catalogoNotificacion; }
    public function setParams($params)            { $this->params = $params; }
    public function setQuiniela($quiniela)        { $this->idquiniela = $quiniela; }
    public function setidSolicitante($solicitante){ $this->idsolicitante = $solicitante; }
    
    
    //	GETS
    
    public function getParams()         { return $this->params; } 
    public function getDescripcion()    { return $this->descripcion; }
    public function getId()             { return $this->id; }
    public function getUsuario()        { return $this->usuario; }
    public function getCatalogoNotificacion(){ return $this->catalogoNotificacion; }
    public function getVisto()          { return $this->visto; }
    public function getFechaNotificacion(){ return $this->fecha; }
    public function getQuiniela()       { return $this->idquiniela; }
    public function getSolicitante()    { return $this->idsolicitante; }
    
    public function getClaseMetodoNotificacion(){
    	//$tipoNotificacion = $this->catalogoNotificacion;
    	//$nombreClase = $tipoNotificacion->getClase();
    	//$clase = new $nombreClase;
    	//$metodo = $tipoNotificacion->getMetodo();
    	//$resultado = $clase->$metodo($this->getParams());
    	//return $resultado;
    }
    
}