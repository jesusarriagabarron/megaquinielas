<?php
/**
 * Clase OrdenDecompra
 * @author jarriaga
 *
 */
class My_Model_OrdenDeCompra extends Zend_Db_Table_Abstract{
	protected $_name = 'OrdenDeCompra';
	protected $_primary = 'id';
	
	
	public function init() {
		$auth = Zend_Auth::getInstance();
		if($auth->getStorage()->read())
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
	}
	
	public function aprobarOrdenDeCompra(Default_Model_OrdenDeCompra $odc){
		//actualizar orden de compra a status 4   =   APROBADO
		$odc->setStatus_EnAprobacion($this->_em);
		//enviamos la notificacion al Comprador
		$notificaciones = new My_Model_Notificaciones();
		$mensajeNotificacion="Tu mención ha sido aprobada, click aquí para continuar la compra";
		$params[0]=$odc->getId();
		$notificaciones->enviarNotificacion($odc->getComprador(), $mensajeNotificacion,4,$params);
	}
	
	/**
	 * Actualiza la orden de compra en caso de modificarla por 
	 * solicitud del vendedor
	 * @param unknown $arreglo
	 */
	public function actualizarOrdenDeCompra($arreglo){
		$validador = new My_Validador();
		//sacamos los datos del arreglo
		$inputMencion 		= $arreglo['inputMencion'];
		$inputInstrucciones = $arreglo['inputInstrucciones'];
		$nombrearchivo 		= $arreglo['nombrearchivo'];
		$nombrearchivoviejo 		= $arreglo['nombrearchivoviejo'];
		$comprador			= $arreglo['comprador'];
		$odc				= $arreglo['odc'];
		$fechaenvio         = $validador->validDate($arreglo['fechadeenvio'], 'yy-mm-dd hh:ii:ss');
		//convertimos y filtramos los textarea
		$inputInstrucciones = nl2br(htmlspecialchars(stripslashes($inputInstrucciones)));
		$inputMencion= nl2br(htmlspecialchars(stripslashes($inputMencion)));
		//sanitizamos el nombre del archivo o imagen
		$nombrearchivo 		= filter_var($nombrearchivo,FILTER_SANITIZE_URL);
		$nombrearchivoviejo 		= filter_var($nombrearchivoviejo,FILTER_SANITIZE_URL);

		$ordenDeCompra		=	$odc;
		$ordenDeCompra->setInstruccionesMencion($inputInstrucciones);
		$ordenDeCompra->setFechaEnvio($fechaenvio);
		//si hay archivo viejo eliminarlo de la base;
		//y si hay archivo nuevo eliminarlo;
		
		$nombrearchivoviejo	=	urldecode($nombrearchivoviejo);
		
		if($nombrearchivoviejo && $nombrearchivo){
			$ordenDeCompra->setImagen($nombrearchivo);
			if(file_exists(APPLICATION_PATH.'/private/images/'.$nombrearchivoviejo))
			unlink(APPLICATION_PATH.'/private/images/'.$nombrearchivoviejo);
			if(file_exists(APPLICATION_PATH.'/private/images/thumbnail/'.$nombrearchivoviejo))
			unlink(APPLICATION_PATH.'/private/images/thumbnail/'.$nombrearchivoviejo);
		}
		
		if(!$nombrearchivoviejo && !$nombrearchivo){
			$nombrearchivoviejo=$ordenDeCompra->getImagen();
			$ordenDeCompra->setImagen(null);
			if(file_exists(APPLICATION_PATH.'/private/images/'.$nombrearchivoviejo))
			unlink(APPLICATION_PATH.'/private/images/'.$nombrearchivoviejo);
			if(file_exists(APPLICATION_PATH.'/private/images/thumbnail/'.$nombrearchivoviejo))
			unlink(APPLICATION_PATH.'/private/images/thumbnail/'.$nombrearchivoviejo);
		}
		
		if(!$nombrearchivoviejo && $nombrearchivo){
			$ordenDeCompra->setImagen($nombrearchivo);
		}
		
		$ordenDeCompra->setTextoMencion($inputMencion);
		$ordenDeCompra->setStatus(3);
		
		$this->_em->persist($ordenDeCompra);
		$this->_em->flush();
		
		return $ordenDeCompra->getId();
	}
	/**
	 * Función que crea una orden de compra de una mención
	 * @param unknown $arreglo
	 */
	public function crearOrdenDeCompra($arreglo){
		$validador = new My_Validador();
		//sacamos los datos del arreglo
		$inputMencion 		= $arreglo['inputMencion'];
		$inputInstrucciones = $arreglo['inputInstrucciones'];
		$nombrearchivo 		= $arreglo['nombrearchivo'];
		$comprador			= $arreglo['comprador'];
		$pub				= $arreglo['pub'];
		$fechaenvio         = $validador->validDate($arreglo['fechadeenvio'], 'yy-mm-dd hh:ii:ss');
		//obtenemos el precio del objeto pub
		$precio				= $pub->getPrecioMencion();
		//obtenemos el usuario del objeto usuario
		$vendedor			= $pub->getUsuario();				
		//convertimos y filtramos los textarea
		$inputInstrucciones = nl2br(htmlspecialchars(stripslashes($inputInstrucciones)));
		$inputMencion= nl2br(htmlspecialchars(stripslashes($inputMencion)));
		//sanitizamos el nombre del archivo o imagen
		$nombrearchivo 		= filter_var($nombrearchivo,FILTER_SANITIZE_URL);
		//creamos una nueva orden de compra con el Modelo Doctrine
		$ordenDeCompra  	= new Default_Model_OrdenDeCompra();
		$ordenDeCompra->setComprador($arreglo['comprador']);
		$ordenDeCompra->setInstruccionesMencion($inputInstrucciones);
		$ordenDeCompra->setPrecioMencion($precio);
		$ordenDeCompra->setVendedor($vendedor);
		$ordenDeCompra->setFechaEnvio($fechaenvio);
		$ordenDeCompra->setFecha();
		
		if($nombrearchivo)
			$ordenDeCompra->setImagen($nombrearchivo);
		$ordenDeCompra->setTextoMencion($inputMencion);
		$ordenDeCompra->setPub($pub);
		$ordenDeCompra->setStatus(1);
		//Guardarmos y flusheamos el entity manager de doctrine
		$this->_em->persist($ordenDeCompra);
		$this->_em->flush();
		//Retornamos el id de la orden de compra
		return $ordenDeCompra->getId();
	}
	
	/**
	 * Función que obtiene todas las ordenes de compra
	 */
	public function getAll() {
		$rows   = $this->fetchAll();
		return $rows->toArray();
	}
	
	/**
	 * devuelve un array con el alert y el texto del status de una orden de compra.... informacion solo para vendedor
	 * @param Default_Model_OrdenDeCompra $Odc
	 * @return multitype:string
	 */
	public function getStatusActualODC_vendedor(Default_Model_OrdenDeCompra $Odc){
		if(!$Odc instanceof Default_Model_OrdenDeCompra)
			die("Error al obtener el status de la orden de compra");
		$resultado = array();
		switch($Odc->getStatus()){ 
			case 1 :
				$resultado["alert"]="alert-info";
				$resultado["texto"]="Status: No has aprobado esta mención";
				break;
			case 2 :
				$resultado["alert"]="";
				$resultado["texto"]="Status: Solicitud de cambios enviada al comprador";
				break;
			case 3 :
				$resultado["alert"]="alert-info";
				$resultado["texto"]="Status: Se realizaron cambios a la mención, revísala y apruébala";
				break;
			case 4 :
					$resultado["alert"]="alert-success";
					$resultado["texto"]="Status: Haz aprobado esta mención.";
					break;
		}
		
		return $resultado;
	}

	/**
	 * devuelve un array con el alert y el texto del status de una orden de compra.... informacion solo para el comprador
	 * @param Default_Model_OrdenDeCompra $Odc
	 * @return multitype:string
	 */
	public function getStatusActualODC_comprador(Default_Model_OrdenDeCompra $Odc){
		if(!$Odc instanceof Default_Model_OrdenDeCompra)
			die("Error al obtener el status de la orden de compra");
		$resultado = array();
		switch($Odc->getStatus()){
			case 1 :
				$resultado["alert"]="alert-info";
				$resultado["texto"]="Status: la mención aun no ha sido aprobada por el propietario del pub";
				break;
			case 2 :
				$resultado["alert"]="";
				$resultado["texto"]="Status: El propietario del pub te ha solicitado cambios a esta mención";
				break;
			case 3 :
					$resultado["alert"]="alert-info";
					$resultado["texto"]="Status: Los cambios fueron enviados, en espera de Aprobación";
					break;
			case 4 :
					$resultado["alert"]="alert-success";
					$resultado["texto"]="Status: Mención APROBADA, ya puedes realizar el pago.";
					break;
		}
	
		return $resultado;
	}
}