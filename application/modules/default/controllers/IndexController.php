<?php
/**
 * Modelo para controlar el Home y el login de los usuarios
 * @author jarriaga
 *
 */


class IndexController extends My_Controller_Action
{
	public function indexAction(){
		//Obtenemos solo los anuncios publicitarios ACTIVOS o status=1
		$quiniela	=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array('activo'=>1));
		$this->view->quinielas =  $this->ordenaElementos($quiniela);
		
		$quiniela	=	$this->_em->getRepository("Default_Model_Quiniela")
		->findBy(array('activo'=>1),array('premio'=>'desc'),5,0);
		$this->view->quinielasMasLana =  $this->ordenaElementos($quiniela);
	}
	
	public function todasLasQuinielasAction(){
	}
	
	public function quinielasjsonAction(){
		$this->_helper->layout()->disableLayout();
		//obtenemos el parámetro de la página solicitada
		$page = $this->getRequest()->getParam("p",1);
		
		$quiniela	=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array('activo'=>1));
		$data =  $this->ordenaElementos($quiniela);
		
		$quinielasG=array();
		$paginador = Zend_Paginator::factory($data);
		if($page<=$paginador->count()){
			//ponemos la pagina actual
			$paginador->setCurrentPageNumber($page);
			//obtenemos el numero de items por pagina
			$paginador->setItemCountPerPage(20);
		
			foreach ($paginador as $pag){
				$quinielasG[] = $pag;
			}
		}else{
			$quinielasG=array();
		}
		$this->getHelper('json')->sendJson(array('pubs' => $quinielasG,'paginas'=>$paginador->getPages()));
	} 
	
	
	public function avisoDePrivacidadAction(){
		
	}
	
	public function terminosYCondicionesAction(){
		
	}
	
	/**
	 * Ordena los elementos de la quiniela para mandar los datos a la vista
	 * @param object $quiniela
	 */
	private function ordenaElementos($quiniela){
		$datosQuiniela = array();
		$count = 0;
		foreach($quiniela as $row){
			$datosQuiniela[$count]['invitado']    = $row->verificaInvitacion($this->_auth["id"]);
			$datosQuiniela[$count]['idquiniela']  = $row->getId();
			$datosQuiniela[$count]['titulo']      = $row->getTitulo();
			$t=$row->getTitulo();
			if(strlen($t)>30)
				$t=substr($t, 0,30);
			$datosQuiniela[$count]['tituloS']	  = $t;
			$datosQuiniela[$count]['descripcion'] = $row->getDescripcion();
			$datosQuiniela[$count]['status']      = $row->getStatus();
			$datosQuiniela[$count]['tipo']        = $row->getTipo();
			$datosQuiniela[$count]['premio']      = "MX$".number_format($row->getPremio(),2,'.',',');
			$datosQuiniela[$count]['precio']      = "MX$".number_format($row->getPrecioBoleta(),2,'.',',');
	
			$usuario = $row->getUsuario();
			$datosQuiniela[$count]['idUsuario']      = $usuario->getId();
			$datosQuiniela[$count]['idFacebook']     = $usuario->getfacebookid();
			$datosQuiniela[$count]['nombreUsuario']	 = $usuario->getNombreUsuario();
			$datosQuiniela[$count]['activo']         = $row->getActivo();
			$datosQuiniela[$count]['boletas']        = $row->getNumeroBoletasParticipando();
			$datosQuiniela[$count]['participantes']  = count($row->getUsuariosParticipando());
	
			$datosQuiniela[$count]['partidos'] = $row->getNumeroPartidos();
	
			$count++;
		}
		return $datosQuiniela;
	}
  
	
	public function whAction(){
		$this->_helper->layout()->disableLayout();
		
		// Realiza el parse a la llamada de notificación como JSON
		$body = @file_get_contents('php://input');
		$o = json_decode($body);
		if ($o && $o->id){
				try {
					$e = new Default_Model_Webhook();
					$e->setChargeId($o->id);
					$e->setCreateAt($o->created_at);
					$e->setType($o->type);
					$e->setObjectType($o->data->object->object);
					$e->setObjectId($o->data->object->id);
					$e->setObjectLivemode($o->data->object->livemode);
					$e->setObjectCreatedAt($o->data->object->created_at);
					$e->setObjectStatus($o->data->object->status);
					$e->setObjectDescription($o->data->object->description);
					$e->setObjectReferenceId($o->data->object->reference_id);
					$e->setObjectAmount($o->data->object->amount);
					$e->setObjectFee($o->data->object->fee);
					$e->setObjectPaymentObject($o->data->object->payment_method->object);
					$e->setObjectPaymentType(  $o->data->object->payment_method->type);
					$e->setObjectPreviousStatus($o->data->previous_attributes->status);
					
					$e->setData($o);
					
					$this->_em->persist($e);
					
					
					
					if($e->getOBjectPaymentType()=="oxxo" && $e->getType()=="charge.paid"){
							$chargeId = $o->data->object->id;
							$cargo    = $this->_em->find("Default_Model_Cargo",$chargeId);
							$boleta	  = $this->_em->getRepository("Default_Model_Boleta")->findBy(array("charge"=>$cargo,"status"=>0));
							
							if($boleta){
								try{
								
									$fp = fopen(APPLICATION_PATH.'/../cron/log.txt', 'a+');
									fwrite($fp, "cargos(".count($cargo)."): ".$chargeId." --- boleta(".count($boleta)."): ".$boleta[0]->getId().PHP_EOL);
									fclose($fp);
									
								$boleta = $boleta[0];
								$parametros["email"]			=   $boleta->getUsuario()->getEmail();	
								$parametros["nombreUsuario"]	=	$boleta->getUsuario()->getNombreUsuario();
								$parametros["idFacebook"]		=	$boleta->getUsuario()->getfacebookid();
								$parametros["monto"]			=	$e->getObjectAmount();
								$parametros["idBoleta"]			=	$boleta->getId();
								$parametros["idQuiniela"]		=	$boleta->getQuiniela()->getId();
								
								$boleta->setStatus(1);
								$cargo = $boleta->getCargo();
								$cargo->setStatus($e->getObjectStatus());
								
								if($boleta->getQuiniela()->getActivo()==0){
									$Quiniela = $boleta->getQuiniela();
									$Quiniela->setActivo(1);
									$this->_em->persist($Quiniela);
								}
								
								$boleta->getQuiniela()->actualizaPremio($this->_em);
								
								$this->_em->persist($boleta);
								$this->_em->persist($cargo);
								
								$this->_em->flush();
								
								$correo 	= 	new My_Model_CorreosMandrill();
								$correo->enviarConfirmacionPagoOxxo($parametros);
								$correo->nuevaBoletaEnQuiniela($boleta->getQuiniela()->getUsuario(), $boleta);
								
								$fp = fopen(APPLICATION_PATH.'/../cron/log.txt', 'a+');
								fwrite($fp, "ENVIADOOOOO-confirmaboleta-".$boleta->getId().PHP_EOL);
								fclose($fp);
								
								
								}catch (Exception $e){
									$this->getResponse()->setHttpResponseCode(500);
									$fp = fopen(APPLICATION_PATH.'/../cron/error.txt', 'a+');
									fwrite($fp,"boleta:".$boleta->getId()." error####". $e->getMessage().PHP_EOL);
									fclose($fp);
								}
							}
					}
					
					$this->_em->flush();
					
				}catch (Exception $e){
					$this->getResponse()->setHttpResponseCode(500);
					$fp = fopen(APPLICATION_PATH.'/../cron/error.txt', 'a+');
					fwrite($fp, $e->getMessage().PHP_EOL);
					fclose($fp);
				}
		}
		else{
				$this->getResponse()->setHttpResponseCode(500);
		}
	}
	

} 