<?php
/** @Entity */
//require "Doctrine/Common/Collections/ArrayCollection.php";
/** @Entity
 * @Table(name="QuinielaInvitados")
 * 
*/
class Default_Model_Invitados{

   /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    
    /**
     * @ManyToOne(targetEntity="Default_Model_Quiniela",inversedBy="invitados")
     * @JoinColumn(name="idQuiniela",referencedColumnName="id")
     */
    private $Quiniela;
    
     /** @Column(type="integer", nullable=true) **/
    private $idUsuario;
    
    /** @Column(type="bigint", nullable=true) **/
    private $facebookid;
    
    /** @Column(type="integer") **/
    private $aceptado;
    
    /** @Column(type="string", length=300, nullable=true) **/
    private $mensaje;

    public function setQuiniela($quiniela)	  {	  $this->Quiniela		=	$quiniela;		}
    public function setIdUsuario($idUsuario)  {	  $this->idUsuario	    =	$idUsuario;		}
    public function setfacebookid($facebookid){   $this->facebookid	    =	$facebookid;    }
    public function setMensaje($mensaje)      {   $this->mensaje        =   $mensaje;       }
    public function setAceptado($aceptado)     {   $this->aceptado      =   $aceptado;      }
    
    public function getQuiniela()			{	return $this->Quiniela;		}
    public function getIdUsuario()			{	return $this->idUsuario;	}
    public function getId()					{	return $this->id;			}
    public function getfacebookid()			{   return $this->facebookid;   }
    public function getMensaje()            {   return $this->mensaje;      }
    public function getAceptado()           {   return $this->aceptado;     }
    
}