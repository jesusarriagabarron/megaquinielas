<?php
/**
 * Clase que crea el formulario para dar de alta un nuevo emisor en el sistema
 * @author jarriaga
 *
 */
class Application_Form_emisorNuevo extends Zend_Form{
	
	public $mensajes;
	
	public function init(){
			
		$this->setMethod('post');
		$rfc = new Zend_Form_Element_Text('rfc');
		$rfc->setLabel('RFC')
		->setRequired(true)
		
		->addFilters(array('StringTrim', 'StripTags'))
		->addFilter('StringToUpper')
		->addValidator(new Zend_Validate_Db_NoRecordExists(
					array(	'table'=>'EmisorReceptor',
							'field'=>'cRfc',
							'exclude' => array(
            							'field' => 'iTipo',
            							'value' => 1
									        )
							)
						)
					);

		$decorate = array(
				array('ViewHelper'),
				array('Description'),
				array('Errors',array('style'=>'margin:-11px 0 20px 175px!important;')),
				array('Label', array( 'separator'=>' ','placement'=>'IMPLICIT_PREPEND')),
				array('HtmlTag', array('tag' => 'div', 'placement'=>'IMPLICIT_PREPEND'))

		);
		
		$rfc->setDecorators($decorate);
		
		
		$razonsocial = new Zend_Form_Element_Text('razonsocial');
		$razonsocial->setLabel('Nombre ó Razón Social')
		->setRequired(true)
		->setAttrib('size','80')->setAttrib('class', 'input-xxlarge')
		->addFilter('StringToUpper');
		$razonsocial->setDecorators($decorate);
		
		$regimen = new Zend_Form_Element_Select('regimen');
		$regimen->setLabel('Regimen Fiscal')
		->setRequired(true);
		$regimen->addMultiOptions(array(
								'0'=>'Persona Fisica',
								'1'=>'Persona Moral'
							))
		->addFilter('StringToUpper');
		$regimen->setDecorators($decorate);
		
		$tipocfd = new Zend_Form_Element_Select('tipocfd');
		$tipocfd->setLabel('Tipo de Comprobante Fiscal')
		->setRequired(true);
		$tipocfd->addMultiOptions(array(
								'0'=>'(CFDi) Comprobante Fiscal Digital por Internet',
								'1'=>'(CFD) Comprobante Fiscal Digital',
								'2'=>'(CBB) Comprobante Fiscal en papel con código de barras'
							));
		$tipocfd->setDecorators($decorate)->setAttrib('class', 'span4');						
	
		$descripcion = new Zend_Form_Element_Textarea('descripcion');
		$descripcion->setLabel('Anotaciones (opcional puede ingresar texto adicional a este emisor con fines informativos)')
		->addFilter('StripTags')
		->setAttrib('rows','5')
		->setAttrib('cols', '100')->setAttrib('class', 'span5');
		$descripcion->setDecorators($decorate);
		/** direccion fiscal**/
		
		$calle = new Zend_Form_Element_Text('calle');
		$calle->setLabel('Calle')
		->addFilter('StringToUpper')
		->setRequired(true)->setAttrib('class', 'input-xxlarge');
		$calle->setDecorators($decorate);
		
		$numerointerior = new Zend_Form_Element_Text('numeroint');
		$numerointerior->setLabel('Número Interior (opcional)')
		->setAttrib('class', 'input-mini');
		$numerointerior->setDecorators($decorate);
		
		$numeroexterior = new Zend_Form_Element_Text('numeroext');
		$numeroexterior->setLabel('Número Exterior')
		->setAttrib('class', 'input-mini')		->setRequired(true);
		$numeroexterior->setDecorators($decorate);
		
		$colonia = new Zend_Form_Element_Text('colonia');
		$colonia->setLabel('Colonia')
		->setAttrib('class', 'input-xxlarge')
		->addFilter('StringToUpper')
		->setRequired(true);
		$colonia->setDecorators($decorate);
		
		$localidad = new Zend_Form_Element_Text('localidad');
		$localidad->setLabel('Localidad/Ciudad')
		->setAttrib('class', 'input-xxlarge')
		->addFilter('StringToUpper');
		$localidad->setDecorators($decorate);
		
		$municipio = new Zend_Form_Element_Text('municipio');
		$municipio->setLabel('Delegación/Municipio')
		->setAttrib('class', 'input-xxlarge')
		->addFilter('StringToUpper')
		->setRequired(true);
		$municipio->setDecorators($decorate);
		
		$codigopostal = new Zend_Form_Element_Text('codigopostal');
		$codigopostal->setLabel('Codigo Postal')
		->setAttrib('class', 'input-small')
		->addFilter('StringToUpper')
		->setRequired(true);
		$codigopostal->setDecorators($decorate);
		
		$estado = new Zend_Form_Element_Select('estado');
		$estado->setLabel('Estado');
		$direcciones = new Application_Model_Direcciones();
		$estados=$direcciones->getEstados();
		$edos=array();
		foreach($estados as $edo){
			$estado->addMultiOptions( array($edo['idEstado']=>$edo['cNombre']));

		}
		$estado->setDecorators($decorate);
		
		
		$this->addElements(
				array(
						$rfc,
						$razonsocial,
						$regimen,
						$tipocfd,
						$descripcion,
						$calle,
						$numeroexterior,
						$numerointerior,
						$colonia,
						$localidad,
						$municipio,
						$codigopostal,
						$estado
				)
		);
		
		$this->addDisplayGroup(array(
				$rfc,
				$razonsocial,
				$regimen,
				$tipocfd,
				$descripcion
				), 'datosemisor');
		$this->getDisplayGroup('datosemisor')->setLegend('Información del Emisor');
		
		$this->addDisplayGroup(array(
					$calle,
				$numeroexterior,
				$numerointerior,
				$colonia,
				$localidad,
				$municipio,
				$codigopostal,
				$estado
				), 'direccionfiscal');
		$this->getDisplayGroup('direccionfiscal')->setLegend('Dirección Fiscal');
		
		
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Guardar Emisor')->setAttrib('class', 'btn btn-large');
		$decorate = array(
				array('ViewHelper'),
				array('Description'),
				array('Errors',array('style'=>'margin:-11px 0 20px 0px!important;')),
				array('HtmlTag', array('tag' => 'div', 'placement'=>'IMPLICIT_PREPEND','class'=>'text-center'))
				);
		$submit->setDecorators($decorate);
		
		$this->addElement($submit);
		
		
	}
}
