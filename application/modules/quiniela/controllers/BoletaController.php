<?php
/**
 * Clase Centro de Control 
 * @author jarriaga
 *
 */
class quiniela_BoletaController extends My_Controller_Action {
	/**
	 * Muestra la lista de notificaciones
	 */
    public function indexAction(){
    	
    }
    
    
    /**
     * Crear una nueva Boleta, para usuarios
     */
    public function nuevaAction(){
    	$validar    = new My_Validador();
    	
    	$request		=	$this->getRequest();
    	$idQuiniela		=	$validar->alphanumValido($request->getParam('id',''));
    	$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela,"activo"=>1));
    	//si se encontró la quiniela
    	if(!$Quiniela)
    	{
    		$this->_helper->flashMessenger->addMessage('error | No se encontró la quiniela');
    		$this->_redirect("/cuenta/control");
    	}
    	
    	if($Quiniela[0]->getStatus()==0){
    		$this->_helper->flashMessenger->addMessage('error | La Quiniela ya está cerrada');
    		$this->_redirect("/cuenta/control");
    	}
    	
    	if($Quiniela[0]->getUsuario()->getId() != $this->_auth["id"]) {
    		//si la quiniela es privada, verificar invitación
	    	if( $Quiniela[0]->getTipo() == 0){
	    		//verificamos que estes invitado
	    		$estasInvitado 		=		$Quiniela[0]->verificaInvitacion($this->_auth["id"]);
	    		//si NO estas invitado, entonces a la goma
	    		if(! $estasInvitado ){
	    			$this->_helper->flashMessenger->addMessage('error | Esta Quiniela es PRIVADA, requieres una invitación para entrar');
	    			$this->_redirect("/");
	    		}
	    	}
	    	
    	}
    	
    	$this->view->Quiniela	=	$Quiniela[0];
    	$partidosModel			=	new My_Model_Partidos();
    	$this->view->partidos	=	$partidosModel->getPartidosDeQuiniela($Quiniela[0]);
    }

    /**
     * Crear la primera boleta del creador
     */
    public function primeraAction(){
    	$validar    = new My_Validador();
    	$request		=	$this->getRequest();
    	$idQuiniela		=	$validar->alphanumValido($request->getParam('id',''));
    	$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela,"usuario"=>$usuario,"activo"=>0));
    	if(!$Quiniela)
    	{
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no existe o ya esta Activa');
    		$this->_redirect("/cuenta/control");
    	}
    	$this->view->Quiniela	=	$Quiniela[0];
    	
    	$p=array();
    	foreach($Quiniela[0]->getQuinielaPartidos() as $qp){$p[]=$qp->getPartido();}
    	$rowset	=	$this->partidosArray($p);
    	$this->view->partidos = $rowset;
    	
    }
    
    
    /**
     * Guardar la primera boleta del creador
     */
    public function savefirstAction(){
    	$validar    = new My_Validador();
    //si NO es un post redireccionamos con un error
    	if(!$this->getRequest()->isPost()){
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no puede ser creada');
    		$this->_redirect("/cuenta/control");
    	}
  	
    	$request	=	$this->getRequest();
    	$idQuiniela	=	$validar->alphanumValido($request->getParam("idQ",0));
    	$params		=	$request->getParams();
    	//sacamos los resultados
    	$resultados = array();
    	foreach (array_keys($params) as $data){
    		if(substr($data,0,2)=="ps"){
    			$key	=	substr($data,2,strlen($data));
    			$filtro	=	new Zend_Filter_Alpha();
    			$pronostico	= $filtro->filter($params["ps".$key]);
    			$resultados[]=array("idPartido"=>(int)$key,"pronostico"=>$pronostico );
    		}
    	}
    	//comprobamos que sea tu quiniela y que no tenga mas boletas ¬¬
    	$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela,"usuario"=>$usuario,"activo"=>0));
    	
    	
    	if( !count($resultados)>2){
    		$this->_helper->flashMessenger->addMessage('error | Hubo un problema en los resultados de tu quiniela.');
    		$this->_redirect("/quiniela/boleta/primera/id/".$idQuiniela);
    	}
    	
    	if(!$Quiniela && $Quiniela[0]->getNumeroBoletas()>0){
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no existe o ya esta activa');
    		$this->_redirect("/quiniela/boleta/primera/id/".$idQuiniela);
    	}

    	//creamos los resultados de la boleta y creamos la boleta
    	$tablaBoleta	=	new My_Model_Boleta();
    	$boleta 		=	$tablaBoleta->crearBoleta($usuario,$Quiniela[0],$resultados);
    	
    	if(!$boleta){
    		$this->_helper->flashMessenger->addMessage('error | No se pudo crear tu quiniela correctamente');
    		$this->_redirect("/quiniela/boleta/primera/id/".$idQuiniela);
    	}else{
    		$this->_redirect("/quiniela/boleta/payment/boleta/".$boleta->getId());
    	}
    }
    
    
    /**
     * Guardar NUEVAS quinielas
     */
    public function saveAction(){
    	$validar    = new My_Validador();
    	//si NO es un post redireccionamos con un error
    	if(!$this->getRequest()->isPost()){
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no puede ser creada');
    		$this->_redirect("/cuenta/control");
    	}
    	 
    	$request	=	$this->getRequest();
    	$idQuiniela	=	$validar->alphanumValido($request->getParam("idQ",0));
    	$params		=	$request->getParams();
    	//sacamos los resultados
    	$resultados = array();
    	foreach (array_keys($params) as $data){
    		if(substr($data,0,2)=="ps"){
    			$key	=	substr($data,2,strlen($data));
    			$filtro	=	new Zend_Filter_Alpha();
    			$pronostico	= $filtro->filter($params["ps".$key]);
    			$resultados[]=array("idPartido"=>(int)$key,"pronostico"=>$pronostico );
    		}
    	}
    	//comprobamos que sea tu quiniela y que no tenga mas boletas ¬¬
    	$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela,"activo"=>1));

    	
    	//si se encontró la quiniela
    	if(!$Quiniela)
    	{
    		$this->_helper->flashMessenger->addMessage('error | No se encontró la quiniela ');
    		$this->_redirect("/cuenta/control");
    	}
    	
    	if($Quiniela[0]->getStatus()==0){
    		$this->_helper->flashMessenger->addMessage('error | La Quiniela ya está cerrada ');
    		$this->_redirect("/cuenta/control");
    	}
    	
    	//Si no eres el propietario entonces validamos si eres un invitado
    	if($Quiniela[0]->getUsuario()->getId() != $this->_auth["id"]){
	    	//si la quiniela es privada, verificar invitación
	    	if( $Quiniela[0]->getTipo() == 0){
	    		//verificamos que estes invitado
	    		$estasInvitado 		=		$Quiniela[0]->verificaInvitacion($this->_auth["id"]);
	    		//si NO estas invitado, entonces a la goma
	    		if(! $estasInvitado ){
	    			$this->_helper->flashMessenger->addMessage('error | Esta Quiniela es PRIVADA, requieres una invitación para entrar');
	    			$this->_redirect("/");
	    		}
	    	}
    	}
    	 
    	if( !count($resultados)>2){
    		$this->_helper->flashMessenger->addMessage('error | Hubo un problema en los resultados de tu quiniela.');
    		$this->_redirect("/quiniela/boleta/nueva/id/".$idQuiniela);
    	}
    	 
    	if(!$Quiniela && $Quiniela[0]->getNumeroBoletas()>0){
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no existe o ya esta activa');
    		$this->_redirect("/quiniela/boleta/nueva/id/".$idQuiniela);
    	}
    
    	//creamos los resultados de la boleta y creamos la boleta
    	$tablaBoleta	=	new My_Model_Boleta();
    	$boleta 		=	$tablaBoleta->crearBoleta($usuario,$Quiniela[0],$resultados);
    	 
    	if(!$boleta){
    		$this->_helper->flashMessenger->addMessage('error | No se pudo crear tu quiniela correctamente');
    		$this->_redirect("/quiniela/boleta/nueva/id/".$idQuiniela);
    	}else{
    		$this->_redirect("/quiniela/boleta/payment/boleta/".$boleta->getId());
    	}
    }
    
    
    
    /**
     * Función que guarda un pago de una boleta
     */
    public function savepaymentAction(){
    	$request		=		$this->getRequest();
    	$conekta		=		new My_Model_CobrosConekta();
    	$filtro			= 		new Zend_Filter_Alnum();
    	$chargeId		=		$filtro->filter($request->getParam('ConektaChargeId'));
    	$tipoCobro		=		$filtro->filter($request->getParam('tipoCobro'));
    	$filtro			=		new Zend_Filter_Int();
    	$idBoleta		=	(int)	$filtro->filter($request->getParam('boleta'));
    	
    	if(!$idBoleta){
    		$this->_helper->flashMessenger->addMessage('error | No existe la boleta');
    		$this->_redirect("/quiniela/boleta/payment/boleta/".$idBoleta);
    	}
    	if(!$chargeId){
    		$this->_helper->flashMessenger->addMessage('error | No existe el cargo o no fué procesado');
    		$this->_redirect("/quiniela/boleta/payment/boleta/".$idBoleta);
    	}
    	//consultamos el cargo en conekta
    	$cargo 			=		$conekta->getCargo($chargeId);
    	
    	if(!$cargo->id){
    		$this->_helper->flashMessenger->addMessage('error |El cargo no pudo ser procesado por favor comunícate con nosotros contacto@megaquinielabrasil2014.com ');
    		$this->_redirect("/quiniela/boleta/payment/boleta/".$idBoleta);
    	}
    	
    	$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	// si el tipo de pago es con tarjeta de credito
    	if($tipoCobro=="TC"){
		    	if($cargo->status=="paid"){
		    		try{
		    		$nuevoCargo		=	new Default_Model_Cargo();
		    		$nuevoCargo->setAmount($cargo->amount);
		    		$nuevoCargo->setBrand($cargo->payment_method->brand);
		    		$nuevoCargo->setChargeId($cargo->id);
		    		$nuevoCargo->setComision($cargo->fee);
		    		$nuevoCargo->setCurrency($cargo->currency);
		    		$nuevoCargo->setDescription($cargo->description);
		    		$nuevoCargo->setFechaHoraTS($cargo->created_at);
		    		$nuevoCargo->setLast4($cargo->payment_method->last4);
		    		$nuevoCargo->setNameCard($cargo->payment_method->name);
		    		$nuevoCargo->setPaymentMethod($cargo->payment_method->object);
		    		$nuevoCargo->setReference($cargo->reference_id);
		    		$nuevoCargo->setStatus($cargo->status);
		    		$nuevoCargo->setUsuario($usuario);
		    		
		    		
		    		
		    		$this->_em->persist($nuevoCargo);
		    		
		    		$Boleta		=	$this->_em->getRepository("Default_Model_Boleta")->findBy(array("id"=>$idBoleta,"usuario"=>$usuario));
		    		$Boleta		=	$Boleta[0];
		    		$Boleta->setCargo($nuevoCargo);
		    		$Boleta->setStatus(1);
		    		
		    		if($Boleta->getQuiniela()->getActivo()==0){
		    			$Quiniela = $Boleta->getQuiniela();
		    			$Quiniela->setActivo(1);
		    			$this->_em->persist($Quiniela);
		    		}
		    			
			    		$this->_em->persist($Boleta);
			    		//actualizamos el premio
			    		$Quiniela	=	$Boleta->getQuiniela();
			    		$Quiniela->setPremio($Quiniela->getNumeroBoletasParticipando()*$Quiniela->getPrecioBoleta());
			    		$this->_em->persist($Quiniela);
			    		$this->_em->flush();
			    		//confirmamos via correo electrónico
			    		$correo		=		new My_Model_CorreosMandrill();
			    		$correo->enviarConfirmacionPago($usuario,$cargo->amount,$Boleta);
			    		//confirmamos nueva boleta al creador.
			    		$correo->nuevaBoletaEnQuiniela($Quiniela->getUsuario() ,$Boleta);
			    		
			    		$this->_helper->flashMessenger->addMessage('success | Tu pago se ha recibido correctamente, tu boleta ya esta participando! Mucha Suerte');
						$this->_redirect("/cuenta/control");
		    		}catch(Exception $e){
		    			$fp = fopen(APPLICATION_PATH.'/../cron/errorpagotc.txt', 'a+');
		    			fwrite($fp, $e->getMessage().'\n');
		    			fclose($fp);
		    			$this->_helper->flashMessenger->addMessage('error | Tu pago No pudo ser procesado, reporta el numero de boleta y la quiniela ');
		    			$this->_redirect("/cuenta/control");
		    		}
		    		
		    	} //fin IF cargo->status
    	}
    	//si el tipo de cobro es con oxxo
    	if($tipoCobro=="OXXO"){
    		try{
    			$nuevoCargo		=	new Default_Model_Cargo();
    			$nuevoCargo->setAmount($cargo->amount);
    			//$nuevoCargo->setBrand($cargo->payment_method->brand);
    			$nuevoCargo->setChargeId($cargo->id);
    			$nuevoCargo->setComision($cargo->fee);
    			$nuevoCargo->setCurrency($cargo->currency);
    			$nuevoCargo->setDescription($cargo->description);
    			$nuevoCargo->setFechaHoraTS($cargo->created_at);
    			//$nuevoCargo->setLast4($cargo->payment_method->last4);
    			//$nuevoCargo->setNameCard($cargo->payment_method->name);
    			$nuevoCargo->setPaymentMethod($cargo->payment_method->type);
    			$nuevoCargo->setReference($cargo->reference_id);
    			$nuevoCargo->setStatus($cargo->status);
    			$nuevoCargo->setBarcode($cargo->payment_method->barcode);
    			$nuevoCargo->setBarcodeUrl($cargo->payment_method->barcode_url);
    			$nuevoCargo->setExpiresAt($cargo->payment_method->expires_at);
    			$nuevoCargo->setUsuario($usuario);

    			$this->_em->persist($nuevoCargo);
    		
    			$Boleta		=	$this->_em->getRepository("Default_Model_Boleta")->findBy(array("id"=>$idBoleta,"usuario"=>$usuario));
    			$Boleta		=	$Boleta[0];
    			$Boleta->setCargo($nuevoCargo);
    			$Boleta->setStatus(0);

    			$this->_em->persist($Boleta);
    			$this->_em->flush();
    			//confirmamos via correo electrónico
    			$correo		=		new My_Model_CorreosMandrill();
    			$correo->enviarAvisoOxxo($usuario, $Boleta);
   			
    			$this->_redirect("/quiniela/boleta/oxxo/boleta/".$idBoleta);
    			
    		}catch(Exception $e){
    			$fp = fopen(APPLICATION_PATH.'/../cron/errorpagooxxo.txt', 'a+');
    			fwrite($fp, $e->getMessage().'\n');
    			fclose($fp);
    			$this->_helper->flashMessenger->addMessage('error | Tu pago No pudo ser procesado, reporta el numero de boleta y la quiniela ');
    			$this->_redirect("/cuenta/control");
    		}
    	}

    	$this->_redirect("/cuenta/control");
    	
    }
    
    /**
     * Confirmacion de pago con oxxo
     */
    public function oxxoAction(){
    	
    	$request		=		$this->getRequest();
    	$conekta		=		new My_Model_CobrosConekta();
    	$filtro			=		new Zend_Filter_Int();
    	$idBoleta		=	(int)	$filtro->filter($request->getParam('boleta'));
    	 
    	if(!$idBoleta){
    		$this->_helper->flashMessenger->addMessage('error | No existe la boleta');
    		$this->_redirect("/cuenta/control");
    	}

    	$Boleta		=	$this->_em->find("Default_Model_Boleta",$idBoleta);
   		//si no hay boleta a la goma
    	if(!$Boleta){
    		$this->_helper->flashMessenger->addMessage('error | No existe la boleta');
    		$this->_redirect("/cuenta/control");
    	}
    	//si no le pertenece la boleta al usuario a la goma
    	if($Boleta->getUsuario()->getId()<>$this->_auth["id"] || !$Boleta->getCargo() ){
    		$this->_helper->flashMessenger->addMessage('error | No existe la boleta');
    		$this->_redirect("/cuenta/control");
    	}
    	
    	$tipoCargo =  $Boleta->getCargo()->getPaymentMethod();
    	//si el cargo no fue OXXO a la goma
    	if($tipoCargo<>"oxxo"){
    		$this->_helper->flashMessenger->addMessage('error | No existe la boleta');
    		$this->_redirect("/cuenta/control");
    	}
    	//consultamos el cargo en conekta
    	$cargo 			=		$conekta->getCargo($Boleta->getCargo()->getChargeId());
    	
    	if(!$cargo->id){
    		$this->_helper->flashMessenger->addMessage('error | No existe la boleta');
    		$this->_redirect("/quiniela/boleta/payment/boleta/".$idBoleta);
    	}
    	
    	
    	if($cargo->status=="paid"){
    		$this->_helper->flashMessenger->addMessage('success | La boleta No.'.$idBoleta.' ya fue pagada, y esta participando');
    		$this->_redirect("/cuenta/control");
    	}
    	//ENVIAR MAIL DEL CODEBAR
    	
    	// YA DESPUES DE VALIDAR ahora si mostramos el codigo de barras en pantalla
    	$this->view->barcode 		= $cargo->payment_method->barcode;
    	$this->view->barcodeUrl		= $cargo->payment_method->barcode_url;
    	$this->view->expira			= $cargo->payment_method->expires_at;
    	$this->view->boletaId		=	$Boleta->getId();
    }
    
    /**
     * Función que genera el pago de una boleta
     */
    public function paymentAction(){
		$request	=	$this->getRequest();
		$idBoleta 	=	$request->getParam("boleta",0)+0;
		//obtenemos Boleta, el Usuario y la Quiniela
		$Boleta		=	$this->_em->find("Default_Model_Boleta",$idBoleta);
		//Si no hay boleta, a la goma
		if(!$Boleta){
			$this->_helper->flashMessenger->addMessage('error | La boleta no existe');
			$this->_redirect("/cuenta/control");
		}
		$usuario	= 	$Boleta->getUsuario();
		$quiniela	=	$Boleta->getQuiniela();

		$hoy = new DateTime("now");
		if($quiniela->getStatus()==0 || $quiniela->getFechaCierre()<$hoy)
		{
			$this->_helper->flashMessenger->addMessage('error | La quiniela ya está cerrada ');
			$this->_redirect("/cuenta/control");
		}
		
		//si el usuario no es dueño de la boleta  ó  la boleta esta activa ... a la goma!!! 
		if(  !($usuario->getId()==$this->_auth["id"]) || !($Boleta->getStatus()==0) ){
			$this->_helper->flashMessenger->addMessage('error | No se puede procesar esta compra de boleta, verifica tus datos');
			$this->_redirect("/cuenta/control");
		}
		$this->view->Boleta = $Boleta;
		
		$rowset	=	$this->partidosresultadosArray($Boleta->getResultados());
		$this->view->partidos = $rowset;
    }
    
    public function detallequinielaAction(){
    	
    	$request		=	$this->getRequest();
    	$idQuiniela		=	$request->getParam('id','');
    	$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela));
    	
    	if(!$Quiniela)
    	{
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no existe o ya esta Activa');
    		$this->_redirect("/cuenta/control");
    	}
    	
    	//Busca los amigos invitados a la quiniela
    	$ivitados = $this->_em->getRepository('Default_Model_Invitados')->findBy(array('Quiniela'=>$idQuiniela));
    	
    	$this->view->numinvitados = count($ivitados);
    	$this->view->invitados    = $ivitados;  
    	$this->view->Quiniela	  =	$Quiniela[0];
    }
    
    private function partidosresultadosArray($resultados){
    	$rowset = array();
    	foreach ($resultados as $r){
    		$partido= $r->getPartido();
    		$row["activo"]	=	$partido->getActivo();
    		$row["id"]		=	$partido->getId();
    		$row["fecha"]	=	$partido->getFechaPartido();
    		$row["tipo"]	=	$partido->getTipoPartido();
    
    		$equipoL		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoLocalId());
    		$equipoLocal["nombre"]		=	$equipoL->getNombre();
    		$equipoLocal["nombreCorto"]	=	$equipoL->getNombreCorto();
    		$equipoLocal["escudo"]		=	$equipoL->getEscudo();
    
    		$equipoV		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoVisitaId());
    		$equipoVisita["nombre"]		=	$equipoV->getNombre();
    		$equipoVisita["nombreCorto"]	=	$equipoV->getNombreCorto();
    		$equipoVisita["escudo"]		=	$equipoV->getEscudo();
    
    
    		$row["equipoLocal"]		=	$equipoLocal;
    		$row["equipoVisita"]	=	$equipoVisita;
    		
    		$row["pronostico"]		=	$r->getPronostico();
    
    		$rowset[]	=	$row;
    	}
    	return $rowset;
    }
    
    private function partidosArray($partidos){
    	$rowset = array();
    	foreach ($partidos as $partido){
    		$row["activo"]	=	$partido->getActivo();
    		$row["id"]		=	$partido->getId();
    		$row["fecha"]	=	$partido->getFechaPartido();
    		$row["tipo"]	=	$partido->getTipoPartido();
    
    		$equipoL		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoLocalId());
    		$equipoLocal["nombre"]		=	$equipoL->getNombre();
    		$equipoLocal["nombreCorto"]	=	$equipoL->getNombreCorto();
    		$equipoLocal["escudo"]		=	$equipoL->getEscudo();
    
    		$equipoV		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoVisitaId());
    		$equipoVisita["nombre"]		=	$equipoV->getNombre();
    		$equipoVisita["nombreCorto"]	=	$equipoV->getNombreCorto();
    		$equipoVisita["escudo"]		=	$equipoV->getEscudo();
    
    
    		$row["equipoLocal"]		=	$equipoLocal;
    		$row["equipoVisita"]	=	$equipoVisita;
    
    		$rowset[]	=	$row;
    	}
    	return $rowset;
    }
}