<?php


/** @Entity 
 * @Table(name="Usuario")
 * */
class Default_Model_Usuario {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;

    /** @Column(type="string", nullable=true,length=150) **/
    private $nombreCompleto;
    
    /** @Column(type="string",length=100) **/
    private $nombreUsuario;
    
	/** @Column(type="string",unique=true,length=100) **/
	private $email;
	
	/** @Column(type="string", length=300,nullable=true) **/
	private $acercademi;
	
	/** @Column(type="string") **/
	private $facebookId;
	
	/** @Column(type="string",nullable=true,length=200) **/
	private $password;
	
	/**
	 * @OneToMany(targetEntity="Default_Model_Quiniela",mappedBy="usuario")
	 **/
	private $quinielas;
	
	/**
	 * @OneToMany(targetEntity="Default_Model_Boleta",mappedBy="usuario")
	 **/
	private $boletas;
			
	/** @Column(type="integer") **/
	private $rol;
	
	/**
	 * @OneToMany(targetEntity="Default_Model_Notificacion",mappedBy="usuario")
	 **/
	private $notificaciones;
	/**
	 * @OneToMany(targetEntity="Default_Model_Cargo",mappedBy="usuario")
	 **/
	private $cargos;
	
	
	public function setQuiniela(Default_Model_Quiniela $Quiniela){		$this->quinielas[]=$Quiniela;$Quiniela->setUsuario($this);	}
	public function setNombreCompleto($nombre)		{   	$this->nombreCompleto=$nombre;   		}
    public function setNombreUsuario($nombreUsuario){   	$this->nombreUsuario=$nombreUsuario;    }
    public function setEmail($email)				{    	$this->email = $email;					}
    public function setacercademi($acercademi)		{    	$this->acercademi = $acercademi;		}
    public function setRol($rol)					{    	$this->rol	=	$rol;					}
    public function setFacebookId($fbid)			{    	$this->facebookId=$fbid;			   	}
    public function setPassword($password)			{    	$this->password = $password;			}
    
    public function setCargo(Default_Model_Cargo $Cargo){    	$this->cargos[]=$Cargo;    	$Cargo->setUsuario($this);   }
    
    public function getacercademi () 				{    	return $this->acercademi;				}
    public function getNombreUsuario()				{    	return $this->nombreUsuario;			}
    public function getRol()						{    	return $this->rol;						}
    public function getId()							{   	return $this->id;					    }
    public function getNombreCompleto()				{   	return $this->nombreCompleto;		    }
    public function getEmail()						{	   	return $this->email;				    }
    public function getfacebookid() 				{    	return $this->facebookId;			    }
	public function getPassword() 					{		return $this->password;					}
	public function getQuinielas()					{		return $this->quinielas;				}
	public function getNotificaciones()				{		return $this->notificaciones;			}
   	public function getBoletas()					{		return $this->boletas;					}	
	public function getCargos()						{		return $this->cargos;					}
	public function getQuinielasParticipando()		{
		$quinielas=array();		
		$objQuinielas=array();
		$idquinielas=array();
			foreach($this->boletas as $boleta){
					$id = $boleta->getQuiniela()->getId();
					$idquinielas[]=$id;
					$objQuinielas[$id]=$boleta->getQuiniela();
			}
		if($idquinielas)
		$quinielas	=	array_count_values($idquinielas);
		
		$quinielasFinales = array();
		foreach($quinielas as $key=>$value){
			$result	=	array("idQuiniela"=>$key,"objQuiniela"=>$objQuinielas[$key],"boletas"=>$value);	
			$quinielasFinales[]=$result;
		}
		return $quinielasFinales;
	}
}
