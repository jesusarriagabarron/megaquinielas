<?php
/**
 * Devuelve el numero de notificaciones nuevas
 * @author jarriaga
 *
 */

class My_Helper_NotificacionesNuevas extends Zend_View_Helper_Abstract{

	private $_auth;
	private $_em;
	
	public function init(){
		$auth = Zend_Auth::getInstance();
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {
			$this->_auth = false;
		}
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
	}
	
	public function notificacionesNuevas(){
			$this->init();
				$usuario = $this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
				$notificacionesNuevas = $this->_em->getRepository("Default_Model_Notificacion")->findBy(array("usuario"=>$usuario,"visto"=>0));
				$notificacionesNuevas = count($notificacionesNuevas);
				$data = $notificacionesNuevas;
			return $data; 		
	}
}