<?php

/** @Entity 
 * @Table(name="Partido")
 * */
class Default_Model_Partido {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    /** @Column(type="string",length=100,nullable=true) **/
    private $equipoLocal;
    /** @Column(type="string",length=100,nullable=true) **/
    private $equipoVisitante;
    /** @Column(type="string",length=100,nullable=true) **/
    private $escudoLocal;
    /** @Column(type="string",length=100,nullable=true) **/
    private $escudoVisitante;
    /** @Column(type="datetime")  **/
    private $fechaPartido;
    //Opciones Local=L		VISITA=V		EMPATE=E       NO JUGADO / ACTIVO = N
    /** @Column(type="string",length=2, options={"default":"N"}) **/
    private $resultadoPartido="N";
    /** @Column(type="integer",nullable=true) **/
    private $golesLocal;
    /** @Column(type="integer",nullable=true) **/
    private $golesVisitante;
    
    /** @Column(type="string",length=100,nullable=true) **/
    private $tipoPartido;
    
    /** @Column(type="integer",options={"default":0}) **/
    private $activo;
    
    /**
     * @ManyToOne(targetEntity="Default_Model_Evento",inversedBy="partidos")
     * @JoinColumn(name="idEvento",referencedColumnName="id")
     */
    private $evento;    
    
    /**
     * @OneToMany(targetEntity="Default_Model_QuinielaPartidos",mappedBy="Partido")
     **/
    private $Quinielapartidos;
    
    /**
     * @OneToMany(targetEntity="Default_Model_Resultados",mappedBy="partido")
     **/
    private $resultadosQuinielas;
    
    /** @Column(type="integer",nullable=true) **/
    private $equipoLocalId=0;
    
    /** @Column(type="integer",nullable=true) **/
    private $equipoVisitanteId=0;
    
    /**
     * @OneToOne(targetEntity="Default_Model_Equipo",
     * @param unknown $equipolocal
     */
    
    
    

    public function setEquipoLocal($equipolocal)		{		$this->equipoLocal		=	$equipolocal;	}
    public function setEquipoVisita($equipovisita)		{		$this->equipoVisitante	=	$equipovisita;	}
    public function setEscudoLocal($escudoLocal)		{		$this->escudoLocal		=	$escudoLocal;	}
    public function setEscudoVisita($escudovisita)		{		$this->escudoVisitante	=	$equipovisita;	}
    public function setFechaPartido($fechapartido)		{		$this->fechaPartido		=	$fechapartido;	}
    public function setResultadoPartido($resultado)		{		$this->resultadoPartido	=	$resultado;		}
    public function setGolesLocal($goleslocal)			{		$this->golesLocal		=	$goleslocal;	}
    public function setGolesVisita($golesVisita)		{		$this->golesVisitante		=	$golesVisita;	}
    public function setTipoPartido($tipopartido)		{		$this->tipoPartido		=	$tipopartido;	}
    public function setActivo($activo)					{		$this->activo			=	$activo;	}
    public function setEvento(Default_Model_Evento $evento){	$this->evento			=	$evento;		}
    public function setEquipoLocalId($equipoLocalID){			$this->equipoLocalId	=	$equipoLocalID;  }
    public function setEquipoVisitaId($equipoVisitanteID){	$this->equipoVisitanteId=	$equipoVisitanteID;	}
    
    
    public function getEquipoLocal()					{		return $this->equipoLocal;			}
    public function getEquipoVisita()					{		return $this->equipoVisitante;		}
    public function getEquipoLocalId()					{		return $this->equipoLocalId;		}
    public function getEquipoVisitaId()					{		return $this->equipoVisitanteId;	}
    
    public function getEscudoLocal()					{		return $this->escudoLocal;			}
    public function getEscudoVisita()					{		return $this->escudoVisitante;		}
    public function getFechaPartido()					{		return $this->fechaPartido;			}
    public function getResultadoPartido()				{		return $this->resultadoPartido;		}
    public function getGolesLocal()						{		return $this->golesLocal;			}
    public function getGolesVisita()					{		return $this->golesVisitante;		}
    public function getEvento()							{		return $this->evento;				}
    public function getId()								{		return $this->id;					}
   	public function getActivo()							{		return $this->activo;				}
    public function getTipoPartido()					{		return $this->tipoPartido;			}    
}