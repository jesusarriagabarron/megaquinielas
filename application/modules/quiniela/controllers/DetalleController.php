<?php
/**
 * Clase Centro de Control 
 * @author pablo
 *
 */
class quiniela_DetalleController extends My_Controller_Action {
	/**
	 * Detalle de la quiniela
	 */
	public function quinielaAction() {
		
		$validar = new My_Validador();
		$valido  = true;
		 
		$request     = $this->getRequest();
		$idQuiniela	 = $validar->alphanumValido($request->getParam('idquiniela',''));
		$idBoleta	 = $validar->alphanumValido($request->getParam('boleta',''));
		$usuario     = $validar->intValido($request->getParam('usuario','0'));
		$pagina      = $validar->intValido($request->getParam('pagina','0'));

		$this->view->efecto 	= 	false;
		
		if($usuario || $idBoleta){
			$this->view->efecto		=	true;
		}	

		$Quiniela =	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela));
		
		if ($Quiniela) {

			if($idBoleta){ 
				$params =  array('quiniela'=>$Quiniela[0]->getId(),'id'=>$idBoleta, 'status'=>1);
				
			} elseif($usuario) {
				$params =  array('quiniela'=>$Quiniela[0]->getId(),'usuario'=>$usuario, 'status'=>1);
			} else {
				$params =  array('quiniela'=>$Quiniela[0]->getId(), 'status'=>1);
			}
			
			$boleta = $this->_em->getRepository("Default_Model_Boleta")->findBy($params);
			
			//Si el id de boleta no es valido regresamos al panel de control
			if (!$boleta) {
				$this->_helper->flashMessenger->addMessage('error | Dirección no valida!');
				$this->_redirect("/");
			}
			
			
			
			if(!$idBoleta){
				$this->view->hayboleta=false;
				$idBoleta = $boleta[0]->getId();
				$tablaPartidos=new My_Model_Partidos();
				$resultados = $tablaPartidos->getPronosticos($Quiniela[0]);
				$this->view->resultados	=	$resultados;
			}else{
				$this->view->hayboleta = true;
			}
			if($usuario){
				$this->view->hayboleta = true;
			}
			
			if($Quiniela[0]->getUsuario()->getId() == $this->_auth["id"]){
				//Eres el propietario de la quiniela
				$this->view->invitacion = 2;
			}else {
				//No se ha enviado una solicitud de participación
				$this->view->invitacion = 0;
				$solicitud 	   = $this->_em->getRepository("Default_Model_Notificacion")->findBy(array("idquiniela"=>$Quiniela[0]->getId(), "idsolicitante"=>$this->_auth["id"]));
				if(count($solicitud)>0){
					//La solicitud fue enviada
					$this->view->invitacion = 1;
					$invitacion = $this->_em->getRepository("Default_Model_Invitados")->findBy(array("Quiniela"=>$Quiniela[0], "idUsuario"=>$this->_auth["id"]));
					if(count($invitacion) > 0) {
						//La solicitud fué aceptada
						$this->view->invitacion = 2;
					}
				}
			}
			
			$resultados    = $this->_em->getRepository("Default_Model_Resultados")->findBy(array('boleta'=>$idBoleta));
			$boletausuario = $this->_em->getRepository("Default_Model_Boleta")->findBy(array('quiniela'=>$idQuiniela,'usuario'=>$boleta[0]->getUsuario()->getId(), 'status'=>1));
			$boletasActivas	=	$Quiniela[0]->getBoletasActivas();
			
			$this->view->boletasactivas  = $Quiniela[0]->getBoletasActivas();
			$this->view->Bolsa           = $Quiniela[0]->getNumeroBoletasParticipando() * $Quiniela[0]->getPrecioBoleta();
			$this->view->Quiniela        = $Quiniela[0];
			$this->view->Participantes   = $Quiniela[0]->getUsuariosParticipando();
			$this->view->boletasquiniela = $boletasActivas;
			$this->view->usuario         = $boleta[0]->getUsuario();
			
			$this->view->boletaId		 = $resultados[0]->getBoleta()->getId();
			
			$partidosModel 					=	new My_Model_Partidos();
			$this->view->partidos 			= 	$partidosModel->getPartidosDeQuiniela($Quiniela[0]);
			$this->view->boletaresultado 	=   $partidosModel->getPartidosDeBoleta($resultados);
			
			$arrayboleta = array();
			foreach ($boletasActivas as $b){
				$arrayboleta[]=$b["id"];
			}
			
			$this->view->anterior	=	$arrayboleta[0];
			for($i=0;$i<count($arrayboleta);$i++){
				if($arrayboleta[$i]==$idBoleta){
					$this->view->anterior = (($i-1)<0)?$arrayboleta[(count($arrayboleta) -1)]:$arrayboleta[($i-1)];
					$this->view->siguiente = (  ($i+1)>=count($arrayboleta) )? $arrayboleta[0]: $arrayboleta[$i+1];
					
				}
			}
			
			$this->view->totalboletasuser= count($boletausuario);
			$this->view->boletas         = $boletausuario;
			
		} else {
			$this->_helper->flashMessenger->addMessage('error | URL no valida!');
			$this->_redirect("/cuenta/control/");
		}
	}
	
	/**
	 * otbtenemos el detalle de boleta
	 * @param object $resultados
	 */
	private function resultadosboleta($resultados) {
		$data = array();
		$count=0;
		
		foreach($resultados as $item) {
			$data[$count]['fecha']        = $item->getPartido()->getFechaPartido();
			$data[$count]['escudolocal']  = $item->getPartido()->getEscudoLocal();
			$data[$count]['equipolocal']  = $item->getPartido()->getEquipoLocal();
			$data[$count]['equipovisita'] = $item->getPartido()->getEquipoVisita();
			$data[$count]['escudovisita'] = $item->getPartido()->getEscudoVisita();
			$data[$count]['escudovisita'] = $item->getPartido()->getEscudoVisita();
			if($item->getPronostico() == "V" ){ 
			$data[$count]['pronostico']='<span class="flagsp '.$item->getPartido()->getEscudoVisita().'"></span> '.$item->getPartido()->getEquipoVisita();  
			}
			if($item->getPronostico() == "L" ){ 
			$data[$count]['pronostico']='<span class="flagsp '.$item->getPartido()->getEscudoLocal().'"></span> '.$item->getPartido()->getEquipoLocal();
			}
			if($item->getPronostico() == "E" ){ $data[$count]['pronostico'] = '<i class="fa fa-flag"></i> Empate'; }
			$count++;
		}
		return $data;
	}
}
	