<?php
/**
 * Esta clase controla el manejo de notificaciones de los usuarios
 * @author jarriaga
 *
 */

class My_Model_Notificaciones {
	private $_em;
	
	public function __construct(){
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
	}
	
	public function enviarNotificacion(Default_Model_Usuario $Destinatario, $mensajeDeNotificacion, $tipo=2,$params=array()){
		//Catalogo Notificación de Orden de compra
		$tipoDeNotificacion = $this->_em->find('Default_Model_CatalogoNotificacion',$tipo);
		//creamos una nueva notificacion
		$notificacion = new Default_Model_Notificacion();
		$notificacion->setDescripcion($mensajeDeNotificacion);
		$notificacion->setNotificarAlUsuario($Destinatario);
		$notificacion->setVisto(0);
		$notificacion->setParams(serialize($params));
		$notificacion->setCatalogoNotificacion($tipoDeNotificacion);
		$notificacion->setFechaNotificacion();
		//guardamos la notificacion
		$this->_em->persist($notificacion);
		$this->_em->flush();
		return true;
	}
	
	/**
	 * Devuelve la url para pagar la orden de compra
	 * @param unknown $params
	 * @return string
	 */
	public function pagarOrdenDeCompra($params){
		$params = unserialize($params);
		$url	=	"/cuenta/compras/paso3/odc/".$params[0];
		return $url;
	}
	/**
	 * Devuelve la url para el detalle de una orden de compra
	 * @param unknown $params
	 * @return string
	 */
	public function verOrdenDeCompra($params){
		$params = unserialize($params);
		$url = "/cuenta/ventas/detalle/odc/".$params[0];
		return $url;
	}
	
	/**
	 * Devuelve la url para visualizar una charla
	 * @param unknown $params
	 * @return string
	 */
	public function verConversacion($params){
		$params	=	unserialize($params);
		$url	=	"/cuenta/inbox/conversacion/id/".$params[0];
		return $url;
	}
	

}