<?php

/**
 * Formulario para registro externo
 * @author Pablo
 *
 */

class Application_Form_usuarios extends Zend_Form{
	
	public function init(){
		$this->setMethod('post');
		// Datos principales de la cuenta
		$email = new Zend_Form_Element_Text('e-amil');
		$email->setLabel('')->setAttrib('placeholder','Correo electrónico')
			->setOptions (array('class'=>'input-xlarge'))
			->setRequired(true)->addErrorMessages(array())
			->addFilters(array('StringTrim', 'StripTags'))
			->addValidator('EmailAddress')
			->addValidator(new Zend_Validate_Db_NoRecordExists(array(
                                                                'field'=>'email',
                                                                'table'=>'Usuario'
                                                               )));
			
		$username = new Zend_Form_Element_Text('user-name');
		$username->setLabel('')->setAttrib('placeholder','Nombre de usuario')
			->setOptions(array('class'=>'input-xlarge'))
			->setRequired(true)->addErrorMessage('Nombre de usuario')
			->addFilters(array('StringTrim', 'StripTags'));
		
		$passwd = new Zend_Form_Element_Password('password');
		$passwd->setLabel('')->setAttrib('placeholder','Contraseña')
			->setOptions(array('class'=>'input-xlarge'))
			->setRequired(true)->addErrorMessage('Escribe tu contraseña')
			->addFilters(array('StringTrim', 'StripTags', 'HtmlEntities'));
		
		$passwdconf = new Zend_Form_Element_Password('password2');
		$passwdconf->setLabel('')->setAttrib('placeholder','Confirma tu contraseña')
			->setOptions(array('class'=>'input-xlarge'))
			->setRequired(true)->addErrorMessage('Confirma tu contraseña')
			->addFilter('HtmlEntities')
			->addValidator('Identical',false,array('token'=>'password'));
		
		$submit = new Zend_Form_Element_Button('submit');
		$submit->setlabel('Enviar')
			->setOptions(array('class'=>'btn btn-success btn-large'))
			->setAttrib('id',   'saveregistro')
			->setAttrib('type', 'submit');
		
		
		$this->addElements(array(
				$email,
				$username,
				$passwd,
				$passwdconf,
				$submit));
	}
}