<?php
/**
 *	archivo de clase auth.php 
 * 
 * Clase facebookSDK_auth
 * adaptador para validar una cuenta facebook mediante su id y accesstoken
 * la clase implementa la interface de zend auth adapter
 * 
 * @package My.facebookSDK.auth.php
 * @author  Azteca Digital [Jesus-Arriaga]
 *
 */


class My_facebookSDK_auth implements Zend_Auth_Adapter_Interface
{
	protected 	$user;
	protected	$appid;
	protected	$appsecret;
	protected	$jugador;
	protected 	$facebook;
	

	/**
	 * Metodo para autentificar un usuario de Facebook 
	 * 
	 */
    
    public function __construct($user){
			$this->user=$user;

    }
    
	public function	authenticate()
	{
        //$user = $this->facebook->getUser();
        $this->_facebookId=$user;
 					
        //$params = array( 'next' => 'http://'.$_SERVER['SERVER_NAME'].'/laislaelreality/eljuego/cerrarsesion/' );
  		//$this->jugador['logoutUrl']=$this->facebook->getLogoutUrl($params);

		$this->jugador['facebookId']=$this->user;
		$this->jugador['iFacebookid']=$this->user;

        if (!$this->user) 
    	{
    		return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID,null,array('Autenticación fallida'));
    	}
    	else
    	{
    		return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS,$this->jugador,array());
	   	}
	}
	
	
	
}