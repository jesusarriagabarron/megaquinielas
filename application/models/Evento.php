<?php


/** @Entity 
 * @Table(name="Evento")
 * */
class Default_Model_Evento {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    
    
    /** @Column(type="string",length=100) **/
    private $nombreEvento;
    
    /**
     * @OneToMany(targetEntity="Default_Model_Partido",mappedBy="evento")
     **/
    private $partidos;
    
    /**
     * @Column(type="datetime") 
     */
     private $dFechacreacion;
     
     /** @Column(type="integer") **/
     private $publicado=0;
     
     
     
     
     
    
    public function setPublicar(){$this->publicado=1;}
    public function setDespublicar(){$this->publicado=0;}
    public function setNombreEvento($nombreEvento)             {	$this->nombreEvento	 =	$nombreEvento;    }
    public function setPartido(Default_Model_Partido $partido) {	$this->partidos[]    =  $partido;	      }
    public function setFechacreacion($dFechacreacion)          {    $this->dFechacreacion =  $dFechacreacion = new DateTime("now"); }
    
    public function getId()							{       return $this->id;               }
    public function getNombreEvento()				{		return $this->nombreEvento;		}
    public function getPartidos()					{		return $this->partidos;			}
    public function getFechacreacion ()             {       return $this->dFechacreacion;   }
    public function getPublicado(){return $this->publicado;}
    
    public function getPartidosActivos(){
    	$partidos=array();
 				foreach($this->partidos as $partido){
 					if($partido->getActivo()==1){
 						array_push($partidos, $partido);
 					}
 				}
 		return $partidos;
    }
    
       
}