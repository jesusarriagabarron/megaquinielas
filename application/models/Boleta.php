<?php


/** @Entity 
 * @Table(name="Boleta")
 * */
class Default_Model_Boleta {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    
    
    /**
     * @ManyToOne(targetEntity="Default_Model_Usuario",inversedBy="boletas")
     * @JoinColumn(name="idUsuario",referencedColumnName="id")
     */
    private $usuario;
    
   /**
     * @ManyToOne(targetEntity="Default_Model_Quiniela",inversedBy="boletas")
     * @JoinColumn(name="idQuiniela",referencedColumnName="id")
     */
    
    private $quiniela;
    
   /**
     * @OneToMany(targetEntity="Default_Model_Resultados",mappedBy="boleta")
     **/
    private $resultados;
    
    
    /** @Column(type="integer",options={"default":0})**/
    private $status;
    
    /** @Column(type="datetime",nullable=false)**/
    private $fechaCreacion;
    
    /**
     * @OneToOne(targetEntity="Default_Model_Cargo")
     * @JoinColumn(name="chargeId", referencedColumnName="chargeId")
     */
    private $charge;
    
    
    public function setUsuario(Default_Model_Usuario $usuario){  $this->usuario	=	$usuario; }
    public function setQuiniela(Default_Model_Quiniela $quiniela){	$this->quiniela	=	$quiniela;}
    public function setResultado(Default_Model_Resultados $resultado){	$this->resultados[]=$resultado;	}
    public function setStatus($status)		{	$this->status	=	$status;	}
    public function setFechaCreaction()		{	$this->fechaCreacion	=	new DateTime('now');	}
    public function setCargo(Default_Model_Cargo $cargo)				{	$this->charge	=	$cargo;	}
    
    
    public function getUsuario()		{	return $this->usuario;	}
    public function getQuiniela()		{	return $this->quiniela;	}
    public function getResultados()		{	return $this->resultados;}
    public function getStatus()			{	return $this->status;	}
    public function getFechaCreacion()	{	return $this->fechaCreacion;}
    public function getCargo()			{	return $this->charge;	}
    public function getId()				{	return $this->id;		}
    public function getTotalAciertos()	{
    	$aciertos=0;					
    	foreach ($this->resultados as $resultado){
    		if($resultado->getPronostico()==$resultado->getPartido()->getResultadoPartido())
    			$aciertos++;
    	}
    	return $aciertos;
    }	
    
    
}