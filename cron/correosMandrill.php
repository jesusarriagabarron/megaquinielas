<?php

defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));

require_once(CRON_PATH."/../library/mandrillSDK/Mandrill.php");

class correosMandrill{

	private $_mandrill;
	private $_twitter;
	private $_facebook;
	private $_config;


	public function __construct(){
		//iniciamos mandrill
		$this->_mandrill = new Mandrill("7a_vHPi_REGrJ4st8aGe7w");
		$this->_facebook	=	"https://www.facebook.com/megaquinielas";
		$this->_twitter		=	"https://www.facebook.com/megaquinielas";

	}
	
	/**
	 * Crea y envía un correo de confirmación de recepción de pago.
	 * @param Default_Model_Usuario $usuario
	 */
	public function enviarConfirmacionPago($parametros){
		//Datos generales
		$this->_config["emailDestino"]  = 	$parametros["email"];
		$this->_config["nombreDestino"] =	$parametros["nombreUsuario"];
		$this->_config["idFacebook"]	=	$parametros["idFacebook"];
		$this->_config["titulo"]		=	"Confirmación de pago OXXO";
		$this->_config["monto"]			=	$parametros["monto"];
		$this->_config["idBoleta"]		=	$parametros["idBoleta"];
		$this->_config["idQuiniela"]	=	$parametros["idQuiniela"];
		//identificador para mandril ¡Muy importante para catalogar correos!
		$this->_config["identificador"]	=   "megaquinielas-pagoboletaoxxo";
		//Mensaje en HTML
		$this->_config["mensajeHTML"]="<p style='text-align:justify;'>
				Hemos recibido el pago de tu boleta en OXXO por <span style='color:#0b0;'><strong>MX$".number_format($this->_config["monto"]/100,2,'.',',')."</strong></span> pesos mexicanos 
				<br>
				Tu boleta ya esta participando ahora. ¡buena suerte!
				<br><br>
				<strong>Detalle de tu boleta:</strong>
				<br>
				<br>
				<strong>
				<a href='http://www.megaquinielabrasil2014.com/quiniela/".$this->_config["idQuiniela"]."/boleta/".$this->_config["idBoleta"]."'>Boleta # ".$this->_config["idBoleta"]."</a><br>
				<a href='http://www.megaquinielabrasil2014.com/quiniela/".$this->_config["idQuiniela"]."'>Quiniela: ".$this->_config["idQuiniela"]."</a><br>
				Costo de la boleta: MX$".number_format(($this->_config["monto"]/100),2,'.',',')."<br>
				</strong>
				<br>
				si tienes dudas o sugerencias sobre nuestra plataforma,por favor envíalas al siguiente correo electrónico<br>
				<a href='mailto:contacto@megaquinielabrasil2014.com' target='_top'>contacto@megaquinielabrasil2014.com</a>
				</p>";
		$this->sendTemplate();
	}
	
	
	public function sendTemplate(){
		$f=new DateTime('now');
		
		$template_name= "megaquinielabrasil2014-general";
		$template_content = array(
				array(
						"name" => "example name",
						"content" => "example content"
				)
		);
		$message = array(
				"subject"		=>	$this->_config["titulo"],
				"from_email"	=>	"no.reply@megaquinielabrasil2014.com",
				"from_name"		=>	"MegaquinielaBrasil2014.com",
				"to"			=>	array(
										array(
										"email"		=>		$this->_config["emailDestino"] ,
										"name"		=>		$this->_config["nombreDestino"] ,
										"type"		=>		"to"
									 	)
	        					  	),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array($this->_config["identificador"]),
				"global_merge_vars" => array(
						array("LIST:COMPANY"				=> "MegaquinielasBrasil2014")
				),
				"merge_vars" => array(
						array(
								"rcpt" => $this->_config["emailDestino"] ,
								"vars" => array(
										array(
												"name" => "COMPRADOR",
												"content" => $this->_config["nombreDestino"] 
										),
										array(
												"name" => "COMPANY",
												"content" => "MegaquinielasBrasil2014"
										),
										array(
												"name" => "CURRENT_YEAR",
												"content" => $f->format('Y')  
										),
										array(
												"name"		=>	"MENSAJE",
												"content"	=>	$this->_config["mensajeHTML"]
										),
										array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
										array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
								)
						)
				),
				"recipient_metadata" => array(
						array(
								"rcpt" => $this->_config["emailDestino"] ,
								"values" => array("user_id" => $this->_config["idFacebook"] )
						)
				),
		);// Fin array message
				
		$async = false;
		$ip_pool = "Main Pool";
		$send_at = date("Y-m-d hh:ii:ss",strtotime("-1 day"));
		try{
			$result = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
		} catch(Mandrill_Error $e) {
		
		}
	}
}
