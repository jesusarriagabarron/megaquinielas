<?php
/**
 * Devuelve la fecha en formato
 * @author plopez
 *
 */

class My_Helper_Sharingvertical extends Zend_View_Helper_Abstract{

	public function Sharingvertical($url=""){
		if(!$url)
			$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		
		
		
		$facebook      = '<div style="position:absolute;"><div class="fb-share-button" data-href="'.$url.'" data-type="button_count"></div></div>';
		$twitterlink   = '<div style="position:absolute;margin-left:125px;"><a href="'.$url.'" class="twitter-share-button">Tweet</a></div>';
		$twitterscript = "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
	 	$googlescript  = "<script type=\"text/javascript\">
					  window.___gcfg = {lang: 'es-419'};
					  (function() {
					    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
					    po.src = 'https://apis.google.com/js/platform.js';
					    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
					  })();
					</script>";
		$google        = '<div style="position:absolute;margin-left:220px;"><div class="g-plus" data-action="share" data-annotation="bubble"></div></div>';
		 
		$sharing =  $twitterlink.' '.$twitterscript.$facebook.$googlescript.$google;
		
		return $sharing;
	}
	
}