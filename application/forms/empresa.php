<?php
/**
 * Archivo utilizado para la creación de formulario de captura de empresa
 * persona Fisica y Moral
 * @author Pablo
 */

/**
 * Calase generadora de formulario registro empresa
 * @author Pablo
 *
 */

class Application_Form_empresa extends Zend_Form{
	public function init(){
		
		$this->setMethod('post');
		
		$nombre= new Zend_Form_Element_Text('nombre');
		$nombre->setLabel('Nombre: ')
			->setRequired(true)->addErrorMessage('Escribe el nombre de tu empresa o tu razón social')
			->addFilter('HtmlEntities');
		
		$rfc = new Zend_Form_Element_Text('rfc');
		$rfc->setLabel('rfc: ')
			->setRequired(true)->addErrorMessage('Escribe tu R.F.C.')
			->addFilter('HtmlEntities');
		
		$esquema = new Zend_Form_Element_Select('esquema');
		$esquema->setLabel('Esquema de facturación: ')
			->setMultiOptions(array('0'=>'Esquema de Facturación',
									 '1'=>'Comprobantes Fiscales Impresos con Código de Barras (CBB)', 
									 '2'=>'Comprobantes Fiscales Digitales por Internet(CFDI)'))
			->setRequired(true)->addValidator('NotEmpty', true);
			
		$comprobante = new Zend_Form_Element_Select('comprobante');
		$comprobante->setLabel('Comprobante a emitir: ')
			->setMultiOptions(array('0'=>'Comprobante a emitir',
									 '1'=>'Factura',
									 '2'=>'Recibo de onorarios'))
			->setRequired(true)->addValidator('NotEmpty', true);
		
		$regimen = new Zend_Form_Element_Text('regimen');
		$regimen->setLabel('Regimen Fiscal: ')
			->setRequired(true)->addErrorMessage('Escribe tu regimen fiscal')
			->addFilter('HtmlEntities');
		
		$cp = new Zend_Form_Element_Text('cp');
		$cp->setLabel('Código postal: ')
			->setRequired(true)->addErrorMessage('Escribe tu código postal')
			->addFilter('HtmlEntities');
		
		$estado = new Zend_Form_Element_Select('estado');
		$estado->setLabel('Estado: ')
			->setMultiOptions(array('0'=>'Selecciona un estado'))
			->setRequired(true)->addValidator('NotEmpty', true);
		
		$municipio = new Zend_Form_Element_Select('municipio');
		$municipio->setLabel('Municipio: ')
			->setMultiOptions(array('0'=>'Selecciona un municipio'))
			->setRequired(true)->addValidator('NotEmpty', true);
		
		$colonia = new Zend_Form_Element_Text('colonia');
		$colonia->setLabel('Colonia: ')
			->setRequired(true)->addErrorMessage('Escribe tu colonia')
			->addFilter('HtmlEntities');
		
		$calle = new Zend_Form_Element_Text('calle');
		$calle->setLabel('Calle: ')
			->setRequired(true)->addErrorMessage('Escribe tu calle')
			->addFilter('HtmlEntities');
		
		$noext = new Zend_Form_Element_Text('noexterior');
		$noext->setLabel('No. Exterior: ')
			->setRequired(true)->addErrorMessage('Escribe tu número Exterior')
			->addFilter('HtmlEntities');
		
		$nointerior = new Zend_Form_Element_Text('nointerior');
		$nointerior->setLabel('No. Interior: ')
			->addFilter('HtmlEntities');
		
		$localidad = new Zend_Form_Element_Text('localidad');
		$localidad->setLabel('Localidad: ')
			->setRequired(true)->addErrorMessage('Escribe tu localidad')
			->addFilter('HtmlEntities');
		
		$telefono = new Zend_Form_Element_Text('telefono');
		$telefono->setLabel('Telefono: ')
			->setRequired(true)->addErrorMessage('Escribe tu teléfono')
			->addFilter('HtmlEntities');
		
		$email = new Zend_Form_Element_Text('email');
		$colonia->setLabel('Correo electrónico')
			->setRequired(true)->addErrorMessage('Escribe tu correo electrónico')
			->addFilters(array('StringTrim', 'StripTags'))
			->addValidator('EmailAddress');
		
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Enviar');
		
		$this->addElements(array($nombre,
								$rfc,
								$esquema,
								$comprobante,
								$regimen,
								$cp,
								$estado,
								$municipio,
								$colonia,
								$calle,
								$noext,
								$nointerior,
								$localidad,
								$telefono,
								$email,
								$submit)); 
		
		$this->addDisplayGroup(array(
								'nombre',
								'rfc',
								'esquema',
								'comprobante',
								'regimen'
								),'empresa');
		
		$empresa = $this->getDisplayGroup('empresa');
		$empresa->setDecorators(array(
							'FormElements',
							'Fieldset',
							array('HtmlTag',array('tag'=>'div','style'=>'width:30%;;float:left;'))
		));
		
		$this->addDisplayGroup(array(
								'cp',
								'estado',
								'municipio',
								'colonia',
								'calle',
								'noext',
								'nointerior',
							),'direccion');
		$direccion = $this->getDisplayGroup('direccion');
		$direccion->setDecorators(array(
						'FormElements',
						'Fieldset',
						array('HtmlTag',array('tag'=>'div','style'=>'width:30%;;float:left;'))
		));
		
		$this->addDisplayGroup(array(
								'localidad',
								'telefono',
								'email',
								'submit'
							), 'direccion2');
		
		$direccion2 = $this->getDisplayGroup('direccion2');
		$direccion2->setDecorators(array(
							'FormElements',
							'Fieldset',
							array('HtmlTag',array('tag'=>'div','style'=>'width:30%;;float:left;'))
		));
		
	}
}