<?php
/**
 * Clase ventas 
 * @author jarriaga
 *
 */
class cuenta_NotificacionesController extends My_Controller_Action {
	/**
	 * Muestra la lista de notificaciones
	 */
    public function indexAction(){
    	$notificaciones = $this->_em->getRepository('Default_Model_Notificacion')
    					  ->findBy(array('usuario'=>$this->_auth['id']),array("fecha"=>"Desc"));
    	
    	$data  = array();
    	$count = 0;
    	foreach($notificaciones as $notificacion){
    		
    		$data[$count]['id']               = $notificacion->getId();
    		$data[$count]['fecha']            = $notificacion->getFechaNotificacion()->format('Y-m-d H:i:s');
    		$data[$count]['visto']            = $notificacion->getVisto();
    		
    		$data[$count]['tiponotificacion'] = $notificacion->getCatalogoNotificacion()->getId();
    		$data[$count]['params']           = $notificacion->getParams();
    		if($notificacion->getSolicitante() != null) {
    			$solicitante = $this->_em->find("Default_Model_Usuario",$notificacion->getSolicitante());
    			$data[$count]['solicitante']      = $solicitante->getNombreUsuario();
    		} else {
    			$data[$count]['solicitante']      = $notificacion->getUsuario()->getNombreUsuario();
    		}
    		 
    		$data[$count]['descripcion']      = $notificacion->getDescripcion();
    		
    		
    		
    		$count++;
    	}
    	$this->view->notificaciones = $data;
    }
    
    
    
    public function respuestaAction(){
    	$validar    = new My_Validador();
    	$id = $validar->intValido($this->getRequest()->getParam("id",0));
    	
    	if($id){
    		$notificacion = $this->_em->find("Default_Model_Notificacion",$id);
    		$quiniela     = $this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=> $notificacion->getQuiniela()));
    		
    		if($notificacion->getVisto()==0){
    			$notificacion->setVisto(1);
    			$this->_em->persist($notificacion);
    			$this->_em->flush();
    		}
    			
    		$this->view->notificacion = $notificacion;
    		$this->view->quiniela     = $quiniela;
    		
    	} else {
    		$this->_helper->flashMessenger->addMessage('error | URL no valida!');
    		$this->_redirect("/cuenta/control/");
    	}
    }
    
    
	public function responderAction(){
		$validar    = new My_Validador();
		$id = $validar->intValido($this->getRequest()->getParam("id",0));
		
		if($id){
			$notificacion = $this->_em->find("Default_Model_Notificacion",$id);
			$solicitante  = $this->_em->getRepository("Default_Model_Usuario")->findBy(array("id"=>$notificacion->getSolicitante()));
			$quiniela     = $this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=> $notificacion->getQuiniela()));
			
			$invitados    = $this->_em->getRepository("Default_Model_Invitados")->findBy(array("Quiniela"=>$quiniela[0]->getId(),"facebookid"=>$solicitante[0]->getfacebookid()));
			
			
			if($notificacion->getVisto()==0){
				$notificacion->setVisto(1);
				$this->_em->persist($notificacion);
				$this->_em->flush();
			}
			
			$this->view->aceptado = true;
			if(count($invitados)>0){
				$this->view->aceptado = false;
			}
			
			$this->view->notificacion = $notificacion;
			$this->view->solicitante  = $solicitante; 
			$this->view->quiniela     = $quiniela;
		} else {
			$this->_helper->flashMessenger->addMessage('error | URL no valida!');
			$this->_redirect("/cuenta/control/");
		}
	}
    
    /**
     * Registra la respuesta del usuario ante una solicitud de participación en una quiniela
     */
    public function solicitudAction(){
    	
    	if($this->getRequest()->isPost()){
    		$params = $this->getRequest()->getParams();
    		
    		$tiponotifica  = $this->_em->getRepository("Default_Model_CatalogoNotificacion")->findBy(array("id"=>2));
    		$quniela       = $this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$params['idquiniela']));
    		$usuario       = $this->_em->getRepository("Default_Model_Usuario")->findBy(array("id"=>$params['idusuario']));
    		
    		//Valida si el usuario ya esta registrado en la tabla de invitados
    		$registrado = $this->valdaInvitado($params['idusuario'], $quniela);
    		
    		if($registrado){
	    		$invitados = new Default_Model_Invitados();
	    		$invitados->setQuiniela($quniela[0]);
	    		$invitados->setIdUsuario($params['idusuario']);
	    		$invitados->setfacebookid($params['idusuariofb']);
	    		$invitados->setAceptado($params['aceptado']);
	    		
	    		$this->_em->persist($invitados);
	    		$this->_em->flush();
	    		
	    		if ($params['aceptado'] == 1) {
	    			$descripcion = "Tu solicitud a sido aceptada";
	    		} else {
	    			$descripcion = "Tu solicitud no fué aceptada";
	    		}
	    			
	    		$datanotifica = array('usuarioinvitado'=>'', 'titulo'=>'Respuesta solicitud', 'idquiniela' => $params['idquiniela'] );
	    		
	    		$notificacion = new Default_Model_Notificacion();
	    			
	    		$notificacion->setFechaNotificacion();
	    		$notificacion->setDescripcion($descripcion);
	    		$notificacion->setNotificarAlUsuario($usuario[0]);
	    		$notificacion->setVisto(0);
	    		$notificacion->setCatalogoNotificacion($tiponotifica[0]);
	    		$notificacion->setParams($datanotifica);
	    		$notificacion->setQuiniela($params['idquiniela']);
	    			
	    		$this->_em->persist($notificacion);
	    		$this->_em->flush();
	    			
	    		$this->_helper->flashMessenger->addMessage('success | '.$descripcion);
	    		$this->_redirect("/cuenta/notificaciones/");
    		} else {
    			$this->_helper->flashMessenger->addMessage('success | Respuesta enviada.');
    			$this->_redirect("/cuenta/notificaciones/");
    		}
    		
    	}
    	
    	die();
    }
    
    /**
     * Valida si el usuario ya esta registrado en la tabla de invitados
     * @param int $idUsuario
     * @param obj $idQuiniela
     */
    private function valdaInvitado($idUsuario, $idQuiniela){
    	$invitado = $this->_em->getRepository("Default_Model_Invitados")->findBy(array('idUsuario'=>$idUsuario, 'Quiniela'=>$idQuiniela));
    	
    	if(isset($invitado[0])) {
    		//Ya esta registrado el usuario en la tabla de invitados
    		return false;
    	} else {
    		return true;
    	}
    	
    }
    
}
