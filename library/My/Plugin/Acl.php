<?php 
/**
 * Plugin para comprobar los permisos
 * @author jarriaga
 *
 */
class My_Plugin_Acl extends Zend_Controller_Plugin_Abstract {
  private $_acl = null;
 
  public function __construct(Zend_Acl $acl) {
    $this->_acl = $acl;
  }
 
  public function preDispatch(Zend_Controller_Request_Abstract $request) {
  		$auth = Zend_Auth::getInstance();
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
			$x = $this->_auth;
		} else {
			$x= false;
		}

    if(!$x){
    	$role='guest';
   	}else{
   		switch($x["rol"]){
   			case 1:
   				$role	=	'user';
   				break;
   			case 2:
   				$role	=	'admin';
   				break;
   			default:
   				$role	=	'guest';
   				break;
   		}
   	}
		
 
    //For this example, we will use the controller as the resource:
    $modulo = $request->getModuleName();
    $controller = $request->getControllerName();
    $resource = strtolower($modulo.'-'.$controller);
    $action = $request->getActionName();
    
    if(!$this->_acl->isAllowed($role, $resource) && !$this->_acl->isAllowed($role, $resource,$action)) {
    	if(!$this->_auth){ $_SESSION["urlVisita"]=  $request->getRequestUri();}
      //If the user has no access we send him elsewhere by changing the request
   /*   $request->setModuleName('default')
              ->setControllerName('index')
              ->setActionName('index');
    	//$this->getResponse()->setRedirect("/",200);*/
    	$r = new Zend_Controller_Action_Helper_Redirector();
    	$r->gotoUrlAndExit("/");
    }
  }
}
