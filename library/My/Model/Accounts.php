<?php
/**
 * Clase para obtener y manejar informacion de fanpages
 * @author pablo
 *
 */

require_once 'facebookSDK/facebook.php';

class My_Model_Accounts {
	

	/**
	 * APPID variable de la aplicacion facebook
	 * @var text
	 */
	public $_appid;
	
	/**
	 * variable de la aplicacion facebook
	 * @var text
	 */
	public $_secret;
	
	
	/**
	 * Obtenemos las cuentas que el usuario tiene asociadas a su perfil de facebook
	 * Listado de Fanpage donde el usuario es administrador
	 */
	
	public function getAcounts(){
		
		$facebook = new Facebookphp(array(
				'appId'  => $this->_appid,
				'secret' => $this->_secret,
				'cookie' => true
		));
		
		$idUser = $facebook->getUser();
		
		if ($idUser) {
			$accounts = $facebook->api('/'.$idUser.'/accounts');
			return  $accounts['data'];
		} else {
			return false;
		}
		
	}
	
	/**
	 * Obtenemos el token long-live
	 * @param text $token
	 */
	
	private function getLongLiveTocken($token){
		$tokentLongLive = file_get_contents('https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id='.$this->_appid.'&client_secret='.$this->_secret.'&fb_exchange_token='.$token);
	}
	
	/**
	 * Obtenemos información de una FanPage
	 * @param text $idFanPage
	 * @param text $token
	 */
	
	public function getInfoFanPage($idFanPage) {
		
		$facebook = new Facebookphp(array(
				'appId'  => $this->_appid,
				'secret' => $this->_secret,
				'cookie' => true
		));
		
		
		
		$facebook->api('/'.$idFanPage);
		
		
		$accestoken = $facebook->getAccessToken();
	}
	
	
	public function getfriendsFb() {
		
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$fbkeys = $config->getOption('facebook');
		
		$this->_appid  = $fbkeys["appid"];
		$this->_secret = $fbkeys["appid"];
		
		$facebook = new Facebookphp(array(
				'appId'  => $this->_appid,
				'secret' => $this->_secret,
				'cookie' => true
		));
		 
		$idUser = $facebook->getUser();
		
		$params = array(
				'method' => 'fql.query',
				'query' => "SELECT uid,first_name,last_name,pic_square,name FROM user WHERE uid IN (SELECT uid1 FROM friend WHERE uid2=".$idUser.") ORDER BY first_name",
		);
		
		return   $facebook->api($params);
	}
	
	
	
	public function sendmessage($idquiniela, $idUsuario){
		$facebook = new Facebookphp(array(
				'appId'  => $this->_appid,
				'secret' => $this->_secret,
				'cookie' => true
		));
		
		$$access_token = $facebook->getAccessToken();
		
		$attachment =  array(
		
				'access_token' => $access_token,
				'message' => "$msg",
				'name' => "$name",
				'link' => "$link",
				'description' => "$desc",
		);
		
		$facebook->api('/'.$uesr_id.'/feed', 'POST', $attachment);
		
	}
	
}