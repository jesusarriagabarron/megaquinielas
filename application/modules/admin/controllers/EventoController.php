<?php
/**
 * Clase Evento controller 
 * @author Pablo I López
 *
 */

/**
 * Clase para manipulaciónd de datos de eventos 
 * @author Pablo I López
 *
 */

class admin_EventoController extends Zend_Controller_Action {
	
	private $_em;
	
	public function init(){
		//inicializamos doctrine 2
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
	}
	
	/**
	 * Lista de eventos
	 */
	public function indexAction(){
		$this->view->evento = $this->_em->getRepository("Default_Model_Partido")->findAll();
	}
	
	/**
	 * Alta de nuevos eventos
	 */
	public function nuevoAction(){
		
	}
}