<?php
class My_Model_Usuario extends Zend_Db_Table_Abstract{
	
	
	protected $_name = 'Usuario';
	protected $_primary = 'id';
	
	
	public function init() {
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
	}
	
	
	public function getAll() {
		$rows   = $this->fetchAll();
		return $rows->toArray();
	}
	
	
	
	public function comparePass($idUsuario, $pass){
		$usuario    =  $this->_em->find("Default_Model_Usuario",$idUsuario);
		$currenpass =  $usuario->getPassword();
		
		if($currenpass == $pass){
			return true;
		} else {
			return false;
		}
		
	}
	
	
	/**
	 * Actualiza el password del usuario
	 * @param int $idUsuario
	 * @param text $newpass
	 */
	public function savepass($idUsuario, $newpass) {
		$usuario =  $this->_em->find("Default_Model_Usuario",$idUsuario);
		$usuario->setPassword($newpass);
		
		try {
			$this->_em->persist($usuario);
			$this->_em->flush();
			return true;
		} catch (Zend_Exception $e) {
			return false;
		}
	}
	
	
	
	
	public function validarUsuario($email,$password){
		if(!$email && !$password){
			return false;
		}
		//instanciamos Zend_Auth
		$auth=Zend_Auth::getInstance();
		// sacar el adaptador
		$dbAdapter = $this->getAdapter();
		//Creamos el adaptador para la autentificación por base de datos
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter,'Usuario');
		//asignamos las columnas de autenticacion y la forma de codificar el password
		$authAdapter->setIdentityColumn('email')->setCredentialColumn('password')
		->setIdentity($email)->setCredential(hash("whirlpool",addslashes($password)));
		//autenticamos y obtenemos el objeto resultante
		$validar = $auth->authenticate($authAdapter);
		//comprobramos si fué valida la autenticación
		if($validar->isValid()){
			//guardamos la informacion de los campos de la tabla autenticada para la sesión
			$userInfo = $authAdapter->getResultRowObject(null,'password');
			$authStorge = $auth->getStorage();
			$authStorge->write($userInfo);
			return true;
		}
		else {
			return false;
		}
		
	}
	
	/**
	 * Logueo la contraseña viaja cifrada por eso ya no se encripta
	 * @param text $mail
	 * @param text $pass
	 */
	
	public function userloginflat($mail, $pass){
		
		$email     = addslashes(trim($mail));
		$password  = addslashes(trim($pass)); 
		
		if(!$email && !$password){
			return false;
		}
		
		//instanciamos Zend_Auth
		$auth=Zend_Auth::getInstance();
		// sacar el adaptador
		$dbAdapter = $this->getAdapter();
		//Creamos el adaptador para la autentificación por base de datos
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter,'Usuario');
		//asignamos las columnas de autenticacion 
		$authAdapter->setIdentityColumn('email')->setCredentialColumn('password')
		->setIdentity($email)->setCredential($password);
		//autenticamos y obtenemos el objeto resultante
		$validar = $auth->authenticate($authAdapter);
		
		//comprobramos si fué valida la autenticación
		if ($validar->isValid()) {
			//guardamos la informacion de los campos de la tabla autenticada para la sesión
			$userInfo = $authAdapter->getResultRowObject(null,'password');
			$authStorge = $auth->getStorage();
			$authStorge->write($userInfo);
			return true;
		} else {
			return false;
		}
	}
	
	
	public function userloginfb( $email, $userid){
		//instanciamos Zend_Auth
		$auth=Zend_Auth::getInstance();
		// sacar el adaptador
		$dbAdapter = $this->getAdapter();
		//Creamos el adaptador para la autentificación por base de datos
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter,'Usuario');
		//asignamos las columnas de autenticacion
		$authAdapter->setIdentityColumn('email')->setCredentialColumn('facebookId')
		->setIdentity($email)->setCredential($userid);
		//autenticamos y obtenemos el objeto resultante
		$validar = $auth->authenticate($authAdapter);
		
		if ($validar->isValid()) {
			//guardamos la informacion de los campos de la tabla autenticada para la sesión
			$userInfo = $authAdapter->getResultRowObject(null,'password');
			$authStorge = $auth->getStorage();
			$authStorge->write($userInfo);
			return true;
		} else {
			return false;
		}
	}
	
	
	public function getDataUserId($idUsuario){
		return  $this->_em->find("Default_Model_Usuario",$idUsuario);
	}
	
	
	public function getUserbyUserName($username) {
		$query =  $this->_em->createQuery('SELECT u FROM Default_Model_Usuario u WHERE u.nombreUsuario = :username');
		$query->setParameters(array('username' => $username));
		return  $usuario =  $query->getResult();
	}
	
	
	/**
	 * Actualiza datos de FB
	 * @param array $data
	 */
	
	public function updateUserFb($id,$data){
		
		$usuario =  $this->_em->find("Default_Model_Usuario",$id[0]['id']);
		$usuario->setNombreUsuario($data['username']);
		$usuario->setEmail($data['email']);
		$usuario->setUrlPerfil('/'.$data['username']);
		$usuario->setFacebookId($data['id']);
		$usuario->setRol(1);
		
		try{
			$this->_em->persist($usuario);
			$this->_em->flush();
			return true;
			
		} catch (Zend_Exception $e) {
			return false;
		}
	}
	
	
	
	/**
	 * Guarda datos de usuario de facebook
	 * @param array $data
	 */
	public function recorduserface($data) {
		$user =  $this->username($data['username']);
		$datausuario = $this->getUsuarioEmail($data['email']);
 		if(count($datausuario) == 0){
			$usuario = new Default_Model_Usuario();
			$usuario->setNombreUsuario($user);
			$usuario->setEmail($data['email']);
			$usuario->setFacebookId($data['id']);
			$usuario->setRol(1);
		
			try {
				$this->_em->persist($usuario);
				$this->_em->flush();
				$status = true;
				//enviamos correo de bienvenida
				$correoBienvenida 	=	new My_Model_CorreosMandrill();
				$correoBienvenida->enviarBienvenida($usuario);
			} catch(Zend_Exception $e) {
				$status = false;
			}
 		} else {
 			 $status = $this->updateUserFb($datausuario, $data);
 		}
		return $status;
	}
	
	
	/**
	 * Guardamos los datos
	 * @param array $data
	 */
	public function saveUser($data){
		
		$user =  $this->username($data['username']);
		
		$datausuario = $this->getUsuarioEmail($data['eamil']);
		if(count($datausuario) == 0){
			$usuario = new Default_Model_Usuario();
			$usuario->setNombreUsuario($user);
			$usuario->setEmail($data['eamil']);
			$usuario->setUrlPerfil('/'.$data['username']);
			$pass = hash("whirlpool",addslashes($data['password']));
			$usuario->setPassword($pass);
			//por default el rol es 1 = Usuario  ,   0 = Admin
			$usuario->setRol(1);
			try {
				$this->_em->persist($usuario);
				$this->_em->flush();
				return true;
			} catch(Zend_Exception $e) {
				return false;
			}
		} else {
			return 0;
		}
		
	}
	

	 private function username($username){
	 	$query =  $this->_em->createQuery('SELECT u FROM Default_Model_Usuario u WHERE u.nombreUsuario = :username');
	 	$query->setParameters(array('username' => $username));
	 	$usuario =  $query->getResult();
	 	
	 	if (count($usuario)) {
	 		return $username."_1";
	 	} else {
	 		return $username;
	 	}
	 	
	 	
	 }
	 
	
	
	
	
	/**
	 * Obtenemos el id de usuario filtrado por idFacebook
	 * @param int $idfb
	 */
	
	public function getuserbyidfb($idfb){
		try {
			$cols = array('id','email');
			$select = $this->select()->from('Usuario',$cols)->where('facebookId = "'.$idfb.'"');
			return $select->query()->fetchAll();
		} catch (Zend_Exeption  $e) {
			return false;
		}
	}
	
	
	/**
	 * Valida usuario por email
	 * @param text $email
	 */
	
	public function getUsuarioEmail($email){
		try{
			$cols = array('id');
			$select = $this->select()->from('Usuario',$cols)->where('email = "'.$email.'"');
			return $select->query()->fetchAll();
		} catch (Zend_Exception $e) {
			return false;
		}
	}
	
	/**
	 * Actualizamos los datos del perfil de usuario
	 * @param array $data
	 * @return boolean
	 */
	public function datosPerfil($data) {
		
		$usuario =  $this->_em->find("Default_Model_Usuario",$data['idUsario']);
		if($data['fotoperil'] != ''){ $usuario->setFotoPerfil($data['fotoperil']);}
		$usuario->setacercademi($data['acercademi']);
		$usuario->setNombreCompleto($data['nombreUsuario']);
		
		try{
			$this->_em->persist($usuario);
			$this->_em->flush();
			return true;
				
		} catch (Zend_Exception $e) {
			return false;
		}
	}
	
	
	/**
	 * Obtenemos los datos del perfil de usuario
	 * @param int $idUsuario
	 */
	public function getDatosPerfil($idUsuario) {
		$em = $this->_em;
		$query =  $em->createQuery('SELECT u FROM Default_Model_Usuario u WHERE u.id = :idusuario');
		$query->setParameters(array('idusuario' => $idUsuario));
		$usuario =  $query->getResult();
		return $usuario[0];
	}
}