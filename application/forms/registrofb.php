<?php

/**
 * Formulario para registro externo
 * @author Pablo
 *
 */

class Application_Form_registrofb extends Zend_Form{
	
	public function init(){
		$this->setMethod('post');
		// Datos principales de la cuenta
		$email = new Zend_Form_Element_Text('email');
		$email->setLabel('')->setAttrib('placeholder','Correo electrónico')
			->setOptions (array('class'=>'form-control'))
			->setRequired(true)->addErrorMessages(array())
			->addFilters(array('StringTrim', 'StripTags'))
			->addValidator('EmailAddress')
			->addValidator(new Zend_Validate_Db_NoRecordExists(array(
                                                                'field'=>'email',
                                                                'table'=>'Usuario'
                                                               )));
			
		$username = new Zend_Form_Element_Text('username');
		$username->setLabel('')->setAttrib('placeholder','Nombre de usuario')
			->setOptions(array('class'=>'form-control'))
			->setRequired(true)->addErrorMessages(array())
			->addFilters(array('StringTrim', 'StripTags'))
			->addValidator(new Zend_Validate_Db_NoRecordExists(array(
					'field'=>'nombreUsuario',
					'table'=>'Usuario'
			)));
		
		
		$name = new Zend_Form_Element_Hidden('name');
		$idfb = new Zend_Form_Element_Hidden('id');
		
		$submit = new Zend_Form_Element_Button('submit');
		$submit->setlabel('Entrar')
			->setOptions(array('class'=>'btn btn-success btn-lg'))
			->setAttrib('id',   'saveregistro')
			->setAttrib('type', 'submit');
		
		
		$this->addElements(array(
				$email,
				$username,
				$name,
				$idfb,
				$submit));
	}
}