<?php
/**
 * Devuelve una cantidad en tipo de moneda
 * @author jarriaga
 *
 */

class My_Helper_AlertBox extends Zend_View_Helper_Abstract{

	
	public function alertBox(){
		$messenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
		$messages =$messenger->getMessages();
		$html = "";
		if(count($messages)){
			foreach ($messages as $message) {
				$explode =  explode("|", $message);
				$tipo = "alert-info";
				$esmensaje = 0;
				switch (trim($explode[0])){
					case "error":
						$esmensaje=1;
						$tipo = "alert-danger alert-dismissable";
						$titulo="Error";break;
					case "warning":
						$esmensaje=1;
						$tipo = "alert-warning alert-dismissable";
						$titulo="Advertencia";break;
					case "success":
						$esmensaje=1;
						$tipo = "alert-success alert-dismissable";
						$titulo = "Éxito" ;break;
					case "info":
						$esmensaje=1;
						$tipo = "alert-info alert-dismissable";
						$titulo="Información";break;
				}
			$html.='<div id="messagebox" class="alert '.$tipo.' text-center" style="display: none;
  		margin-left:auto;margin-right:auto;width:100%;position: absolute;
z-index: 9999;
width: 100%;
top: 55px;">
				  		<button type="button" class="close" data-dismiss="alert">&times;</button>
				  		<strong >'.$titulo.'</strong><br>
				  		'.$explode[1].'
				  	</div>';
			}
		}
				
		
		return $html; 		
	}
}