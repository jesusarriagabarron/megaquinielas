$(document).ready(function(){
	var partidos = new Array();
	var totalpartidos = 0;
	var mindate	= -1;
	function timex(t){
		$.ajax({url:"/cuenta/control/fecha/time/"+(t/1000)})
		.done(function (data){
				$("#fechacierre").text(data);
				$("#fCierre").val(t/1000);
			});
	};
	
		$(".partido").click(function(){
				if(!$(this).hasClass('success'))
					{
						$(this).addClass('success');
						partidos.push($(this).attr('id'));
						datenuevo=$(this).attr('timestamp');

						if(mindate==-1){
									mindate=$(this).attr('timestamp');
									var date=new Date(mindate*1000);
									date = new Date(date.setDate(date.getDate()-1));
									timex(date.getTime());
						}else{
							if(mindate>datenuevo)
							{
								mindate=datenuevo;
								var date=new Date(mindate*1000);
								date = new Date(date.setDate(date.getDate()-1));
								timex(date.getTime());
							}
						}
					}
				else{
						$(this).removeClass('success');
						id=$(this).attr('id');
						var index = partidos.indexOf(id);
						if (index > -1) {partidos.splice(index, 1);}
						var seleccionados = $('tr.success');
						if (seleccionados.length>0){
							mindate=seleccionados.first().attr('timestamp');
							var date=new Date(mindate*1000);
							date = new Date(date.setDate(date.getDate()-1));
							timex(date.getTime());
							seleccionados.each(function(){
									if(mindate>$(this).attr('timestamp')){
										mindate=$(this).attr('timestamp');
										var date=new Date(mindate*1000);
										date = new Date(date.setDate(date.getDate()-1));
										timex(date.getTime());

									}
							});
							
						}else
						{mindate =-1;$('#fechacierre').text('');}
					}	
				
				var seleccionados = $('tr.success');
				totalpartidos	=	seleccionados.length;
				if(totalpartidos>=3)
					$("#btnCrear").removeAttr('disabled');
				else
					$("#btnCrear").attr('disabled','disabled');
					
				$("#numeroPartidos").text(seleccionados.length+' partidos');
				$("#partidosSeleccion").val(partidos);
			});

		$("#formCrear").submit(function(event){
			var error=0;
			$("#nombreQuiniela").removeClass('has-error');
			$("#precioBoleta").removeClass('has-error');
			$("#nombreQuiniela > span.helpBlock").text('');
			$("#precioBoleta  span.helpBlock").text('');
			
			if(!$("#inputnombreQuiniela").val()){
				$("#nombreQuiniela").addClass('has-error');
				$("#nombreQuiniela > span").text('Ingresa un nombre para tu quiniela');
				error=1;	
			}

			if(!$("#inputprecioBoleta").val() || isNaN($("#inputprecioBoleta").val() ) ){
				$("#precioBoleta").addClass('has-error');
				$("#precioBoleta > span").text('Ingresa el precio de la boleta');
				error=1;	
			}

			if(parseInt($("#inputprecioBoleta").val())<100){
				$("#precioBoleta").addClass('has-error');
				$("#precioBoleta > span").text('El precio debe ser mayor o igual 100');
				error=1;
				}

			if(partidos.length<3){
				event.preventDefault();
				alert('Selecciona tres partidos como mínimo');
				error=1;
			}
			 
			 if(error){
				 event.preventDefault();
			 }
		});
});
