<?php
/**
 * Devuelve la fecha en formato
 * @author jarriaga
 *
 */

class My_Helper_Escudo extends Zend_View_Helper_Abstract{

	public function Escudo($filename,$tipo=""){
		switch ($tipo){
			case "small":
						$img	=	"<img src='/img/flags/icon/".$filename."' style='border: 1px #ddd solid;'>";
						break;
			case "normal":
						$img	=	"<img src='/img/flags/".$filename."' style='width:20px;height:auto;border: 1px #ddd solid;'>";
						break;
			
		}
		return $img;
	}
	
}