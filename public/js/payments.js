var errorString = 	{
						"invalid_number"			:	"El número de la tarjeta es inválido.",
						"invalid_amount"			:	"La cantidad a cobrar no está presente o es inválida.",
						"invalid_payment_type"		:	"El tipo de pago no está presente o es inválido.",
						"missing_description"		:	"La descripción del cargo no está presente",
						"unsupported_currency"		:	"La divisa proporcionada no puede ser reconocida.",
						"invalid_expiry_month"		:	"El mes de expiración de la tarjeta es inválido.",
						"invalid_expiry_year"		:	"El año de expiración de la tarjeta es inválido.",
						"invalid_cvc"				:	"El código de seguridad (CVC) de la tarjeta es inválido.",
						"expired_card"				:	"La tarjeta ha expirado.",
						"card_declined"				:	"La tarjeta ha sido declinada.",
						"processing_error"			:	"Ha habido un error al momento de procesar la tarjeta. Ningún cargo ha sido realizado.",
						"insufficient_funds"		:	"El cargo no ha sido procesado porque la tarjeta no tiene fondos suficientes."
					};

$(document).ready(function() {
	 Conekta.setPublishableKey('key_G2TrWzsxNRmTAGLM');
	
	$('#cvcId').popover();
	$('#cvcId').click(function(e){
		 e.preventDefault();
	});
	
	$('#selectorTabPago a').click(function (e) {
		  e.preventDefault();
		  $(this).tab('show');
		});
    $('#selectorTabPago a:first ').tab('show');
    
    $('#btnDetalle').click(function(){
			$('#detalleCompra').slideToggle();
        });
    
    $('#inputMonth').val($('#cardmonth').val());
    $('#inputYear').val($('#cardyear').val());
    
    $('#cardyear').change(function (){$('#inputYear').val($('#cardyear').val()); });
    $('#cardmonth').change(function (){$('#inputMonth').val($('#cardmonth').val()); });

    $('#paymentFormTC').submit(function(event) {
    	$('#errorMessage').hide();
    	$('#errorMessage').html('');
    	$('#btnProcesarPagoTC').button('loading');
    	if(checkdata()){
    		$('#myModal').modal({keyboard: false});
    		 var $form = $(this);
    		 	 Conekta.token.create($form, conektaSuccessTC, conektaErrorTC);
    	}
    	return false;
    });
    
    
    function checkdata(){
    	var nombre = $('#cardNombre').val();
    	var calle  = $('#cardCalleNumero').val();
    	var colonia  = $('#cardColonia').val();
    	var ciudad  = $('#cardCiudad').val();
    	var codigopostal  = $('#cardCodigoPostal').val();
    	if( !(nombre.length>2))
    		$('#errorMessage').html('El nombre del titular de la tarjeta no puede estar vacío<br>');
    	if( !(calle.length>2))
    		$('#errorMessage').html('La calle y número no puede estar vacío<br>');
    	if( !(colonia.length>2))
    		$('#errorMessage').html('La colonia no puede estar vacía<br>');
    	if( !(ciudad.length>2))
    		$('#errorMessage').html('La ciudad o delegación no puede estar vacía<br>');
    	if( !(codigopostal.length>2))
    		$('#errorMessage').html('El codigo postal no puede estar vacío<br>');
    	if( !(nombre.length>2) || !(calle.length>2) || !(colonia.length>2) ||  !(ciudad.length>2) || !(codigopostal.length>2) ){
    		$('#errorMessage').show();
    		
    		$('#myModal').modal('hide');
    		$("#btnProcesarPagoTC").button('reset');
    		return false;
    	}else{
    		return true;
    	}
    		
    };
    
    var conektaSuccessTC = function(response) {
    	var $form = $('#paso3pagadoForm');
    	var token_id = response.id;
    	$form.append($('<input type="hidden" name="conektaTokenId" />').val(token_id));
    	$form.append($('<input type="hidden" name="tipoCobro" />').val('tarjeta'));
    	$form.get(0).submit();
    };
    
 
    
    var conektaErrorTC = function(response){
    	$('#myModal').modal('hide');
    	$("#btnProcesarPagoTC").button('reset');
    		var code = response.code;
    		if(	response.object=="error" ){
    			if(code){
    				if(errorString[code]){
    				$('#errorMessage').html(errorString[code]);
    				}else{$('#errorMessage').html('Ocurrió un problema al intentar procesar el pago, por favor reportalo');}
    				$('#errorMessage').show();
    			}else
    			if(response.type="invalid_parameter_error"){
    				var m='';
    				if(response.message=='can\'t be blank'){
    					m='No puede estar vacío';
    				}
    				$('#errorMessage').html(response.param+' '+m);
    				$('#errorMessage').show();
    			}
    		}else{
    			$("#errorMessage").text(response.error.message);
    			$('#errorMessage').show();
    		}
    };
});