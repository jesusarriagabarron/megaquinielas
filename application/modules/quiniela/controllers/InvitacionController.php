<?php
/**
 * Clase Centro de Control 
 * @author pablo
 *
 */
class quiniela_InvitacionController extends My_Controller_Action {
	
	public function solicitudAction(){
		$validar    = new My_Validador();
		$request    = $this->getRequest();
		$idQuiniela	= $validar->alphanumValido($request->getParam('idquiniela',''));
		
		$Quiniela      = $this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela));
		$tiponotifica  = $this->_em->getRepository("Default_Model_CatalogoNotificacion")->findBy(array("id"=>1));
		
		
		if ($Quiniela) {
			$this->view->quiniela = $Quiniela; 
		} else {
			$this->_helper->flashMessenger->addMessage('error | URL no valida!');
			$this->_redirect("/cuenta/control/");
		}
		
		if ($this->getRequest()->isPost()) {
			$params = $this->getRequest()->getParams();
			
			$validasolicitud = $this->validasolicitud($this->_auth["id"],$Quiniela[0]->getId());
			
			if($validasolicitud){
				$datanotifica = array('usuarioinvitado'=>$this->_auth["id"],
									  'titulo'=>'Solicitud de participación',
									  'idquiniela' => $Quiniela[0]->getId());
				
				$notificacion = new Default_Model_Notificacion();
				
				$notificacion->setFechaNotificacion();
				$notificacion->setDescripcion($params['msg-invitacion']);
				$notificacion->setNotificarAlUsuario($Quiniela[0]->getUsuario());
				$notificacion->setVisto(0);
				$notificacion->setCatalogoNotificacion($tiponotifica[0]);
				$notificacion->setParams($datanotifica);
				$notificacion->setQuiniela($Quiniela[0]->getId());
				$notificacion->setidSolicitante($this->_auth["id"]);
				$this->_em->persist($notificacion);
				$this->_em->flush();
				
				$this->_helper->flashMessenger->addMessage('success | La solicitud ha sido enviada correctamente!');
				$this->_redirect("/cuenta/control/");
			} else {
				$this->_helper->flashMessenger->addMessage('error | Ya enviaste una solicitud previamente!');
				$this->_redirect("/cuenta/control/");
			}
			
		}
	}
	
	
	private function validasolicitud($usuario, $quiniela){
		$notificacion = $this->_em->getRepository("Default_Model_Notificacion")->findBy(array("usuario"=>$usuario, "idquiniela"=>$quiniela));
		if(count($notificacion)>0){
			return false;
		} else {
			return true;
		}
	}
}