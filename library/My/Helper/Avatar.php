<?php
/**
 * Devuelve el html img del avatar de un usuario
 * @author jarriaga
 *
 */

class My_Helper_Avatar extends Zend_View_Helper_Abstract{
	
	
	public function avatar($fotoPerfil,$size="normal",$w=0,$h=0,$tipo=""){

		$class="";
		switch($tipo){
			case "img-circle": $class="class='img-circle'";break;
			case "img-rounded": $class="class='img-rounded'";break;
			case "img-polaroid": $class="class='img-polaroid'";break;
		}
		
		
		$medida=array('h'=>80,'w'=>80);
		if($w==0 && $h==0){
			switch ($size){
				case "normal":
					$medida=array('h'=>80,'w'=>80);
					break;
				case "small":
					$medida=array('h'=>24,'w'=>24);
					break;
				case "big":
					$medida=array('h'=>140,'w'=>140);
					break;
			}
		}else{
			$medida=array('h'=>$h,'w'=>$w);
		}
		if($fotoPerfil){
			$imagen = "<img ".$class." style='vertical-align:middle;' src='/file/perfil/".$fotoPerfil."' width='".$medida['w']."' height='".$medida['h']."'>";
		}else{
			$imagen = "<img ".$class." style='vertical-align:middle;' src='/img/profile.png' width='".$medida['w']."' height='".$medida['h']."'>";
		}
		
		return $imagen; 		
	}
}