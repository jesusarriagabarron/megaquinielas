<?php
/**
 * Esta clase controla el manejo de notificaciones de los usuarios
 * @author jarriaga
 *
 */

class My_Model_Quinielas {
	private $_em;
	private $_auth;
	
	public function __construct(){
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
		
		$auth = Zend_Auth::getInstance();
		
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {$
			$this->_auth = false;
		}
	}
	
	/**
	 * Modificacion de una quiniela
	 * @param unknown $params
	 * @return boolean|unknown
	 */
	public function modifyQuiniela($params){
		$params["fCierre"]				=	date("Y-m-d H:i:s",$params["fCierre"]);
		$fecha = new DateTime($params["fCierre"]);
		
		$filtro							=	new Zend_Filter_Digits();
		$params["precioBoleta"]			=	$filtro->filter($params["precioBoleta"]);
		$params["tipoQuiniela"]			=	$filtro->filter($params["tipoQuiniela"]);
		$filtro							= 	new Zend_Filter_Alnum();
		$filtro->allowWhiteSpace=true;
		$params["nombreQuiniela"]		=	$filtro->filter($params["nombreQuiniela"]);
		$params["idQ"]					=	$filtro->filter($params["idQ"]);
		try{
				
			$usuario 		= $this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
			$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")
    							->findBy(array("id"=>$params["idQ"],"usuario"=>$usuario,"activo"=>0));
			if(!$Quiniela)
				return false;
			
			$Quiniela=$Quiniela[0];
			$Quiniela->setDescripcion($params["descripcionQuiniela"]);
			$Quiniela->setPrecioBoleta($params["precioBoleta"]);
			$Quiniela->setTipo($params["tipoQuiniela"]);
			$Quiniela->setTitulo($params["nombreQuiniela"]);
			$Quiniela->setUsuario($usuario);
			$Quiniela->setPremio($params["precioBoleta"]);
			$Quiniela->setActivo(0);
				
			$this->_em->persist($Quiniela);
			$this->_em->flush();
				
		}catch(Exception $e){
			return false;
		}
		
		return $Quiniela;
	}
	

	/**
	 * Crear una quiniela nueva
	 * @param unknown $params
	 * @return boolean|Default_Model_Quiniela
	 */
	public function crearQuiniela($params){
		$params["fCierre"]				=	date("Y-m-d H:i:s",$params["fCierre"]);
		$fecha = new DateTime($params["fCierre"]);
		
		$filtro							=	new Zend_Filter_Digits();
		$params["precioBoleta"]			=	$filtro->filter($params["precioBoleta"]);
		$params["tipoQuiniela"]			=	$filtro->filter($params["tipoQuiniela"]);
		$filtro							= 	new Zend_Filter_Alnum();
		$filtro->allowWhiteSpace=true;
		$params["nombreQuiniela"]		=	$filtro->filter($params["nombreQuiniela"]);
		try{
			
			$usuario 		= $this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
			$Quiniela		=		new Default_Model_Quiniela();
			$Quiniela->setDescripcion($params["descripcionQuiniela"]);
			$Quiniela->setFechaCreacion();
			$Quiniela->setId(strtoupper($params["id"]));
			$Quiniela->setPrecioBoleta($params["precioBoleta"]);
			$Quiniela->setTipo($params["tipoQuiniela"]);
			$Quiniela->setTitulo($params["nombreQuiniela"]);
			$Quiniela->setUsuario($usuario);
			$Quiniela->setFechaCreacion();
			$Quiniela->setFechaCierre($fecha);
			$Quiniela->setPremio($params["precioBoleta"]);
			$Quiniela->setActivo(0);
			
			$this->_em->persist($Quiniela);
			$this->_em->flush();
			
		}catch(Exception $e){
			return false;
		}
		
		return $Quiniela;
	}
	
	public function insertarPartidoEnQuiniela(Default_Model_Quiniela $Quiniela,Default_Model_Partido $Partido){
		try{
	 	$nuevoQuinielaPartido = new Default_Model_QuinielaPartidos();
		$nuevoQuinielaPartido->setPartido($Partido);
		$nuevoQuinielaPartido->setQuiniela($Quiniela);
	 	
		$this->_em->persist($nuevoQuinielaPartido);
		$this->_em->flush();
		}catch(Exception $e){
			return false;
		}
		
		return true;
	}
}