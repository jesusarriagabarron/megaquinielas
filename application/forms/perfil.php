<?php
/**
 * Formulario para registro y actualización de datos de [Perfil]
 * @author Pablo
 *
 */

class Application_Form_perfil extends Zend_Form{
	
	private $_dataUsuario = array();
	
	public function init(){
		
		$this->getDataUser();
		$this->setMethod('post');
	
		$idUsuario = new Zend_Form_Element_Hidden('idUsuario');
		$idUsuario->setValue($this->_dataUsuario[0]['idUsuario']);
		
		// Datos principales de la cuenta
		$nombre = new Zend_Form_Element_Text('nombre');
		$nombre->setLabel('Representante legal: ')
		->setRequired(true)
		->addFilters(array('StringTrim', 'StripTags'))
		->setValue($this->_dataUsuario[0]['cNombre']);
	
		$apaterno = new Zend_Form_Element_Text('apaterno');
		$apaterno->setLabel('Apellido Paterno: ')
		->setRequired(true)
		->addFilters(array('StringTrim', 'StripTags'))
		->setValue($this->_dataUsuario[0]['cApaterno']);
	
		$amaterno = new Zend_Form_Element_Text('amaterno');
		$amaterno->setLabel('Apellido Materno: ')
		->addFilters(array('StringTrim', 'StripTags'))
		->setValue($this->_dataUsuario[0]['cAmaterno']);
	
		$email = new Zend_Form_Element_Text('email');
		$email->setLabel('Correo electronico: ')
		->addValidator('EmailAddress');
		
		$fNacimiento = new Zend_Form_Element_Text('fNacimiento');
		$fNacimiento->setLabel('Fecha de nacimiento')
		->addValidator('Date');
		
		$sexo = new Zend_Form_Element_Select('sexo');
		$sexo->setLabel('Sexo')
		->setMultiOptions(array(
				'1'=>'Masculino',
				'2'=>'Femenino'));
	
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setlabel('Guardar');
	
	
		$this->addElements(array(
				$idUsuario,
				$nombre,
				$apaterno,
				$amaterno,
				$email,
				$fNacimiento,
				$sexo,
				$submit));
	
	}
	
	private function getDataUser(){
		$sesion = new Zend_Session_Namespace('UsuarioActivo');
		$auth   = Zend_Auth::getInstance();
		
		if ($auth->hasIdentity()) {
			$usuario = new  Application_Model_Usuario();
			$this->_dataUsuario = $usuario->getInfoPerfilUsuario($sesion->usuario['idUsuario']);
		}
	}

}