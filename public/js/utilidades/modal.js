$(function() {
		$( "#dialog-form" ).dialog({
			dialogClass: "no-close",
			autoOpen: false,
			height: 565,
			width: 833,
			modal: true,
			show: {
		        effect: "blind",
		        duration: 1000
		    },
		    hide: {
		        effect: "explode",
		        duration: 1000
		    },
			buttons: {
				"Guardar" : function() {},
				"Cancelar": function() {$( this ).dialog( "close" );}
			},
			close: function() {
			}
		});
		
		$( "#create-user" )
		      .button()
  			  .click(function() {
	              $("#dialog-form").html('<iframe id="modalIframeId" width="100%" height="100%" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="auto" />').dialog( "open" );
	              $("#modalIframeId").attr("src","/users/registro");
	     });
});
