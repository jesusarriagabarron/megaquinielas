<?php
/**
 * Esta clase controla el manejo de notificaciones de los usuarios
 * @author pablo
 *
 */

class My_Model_Invitados {

	private $_em;
	private $_auth;
	
	public function __construct(){
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
	
		$auth = Zend_Auth::getInstance();
	
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {$
		$this->_auth = false;
		}
	}
	
	/**
	 * 
	 * @param int $idUsuario    Usuario que invita
	 * @param int $idquiniela   Quiniela a la que fué invitado
	 * @param int $idfacebook   Usuario invitado
	 * 
	 * Guarda en base invitaciónes
	 */
	public function saveInvitados($idUsuario, $idquiniela, $idfacebook, $mensaje){
		$validar = new My_Validador();
		
		//Si el usuario ya está registrado se le envia una notificación
		$registrado =  $this->usuarioregistrado($idfacebook);
		if ($registrado) {
			//Enviar notificacion
		}
		
		try {
			//Validamos que la invitación no este duplicada
			$duplicado = $this->validainvitacion($validar->alphanumValido($idquiniela), $validar->intValido($idfacebook));
			if($duplicado) {
				$quiniela = $this->_em->getRepository('Default_Model_Quiniela')->findBy(array('id'=>$validar->alphanumValido($idquiniela)));
				if(count($quiniela)) {
					$invitados = new Default_Model_Invitados();
					$invitados->setIdUsuario  ($validar->intValido($idUsuario));
					$invitados->setfacebookid ($validar->intValido($idfacebook));
					$invitados->setQuiniela   ($quiniela[0]);
					$invitados->setMensaje    ($mensaje);
					$invitados->setAceptado   ("1");
					$this->_em->persist($invitados);
					$this->_em->flush();
				}
			}
			
		} catch(Zend_Excpetion $e) {
			return false;
		}
	}
	
	
	public function sendNotificacion($idUsuario, $idquiniela, $idfacebook) {
		
	}
	
	/**
	 * Validamos que no se dupliquen las invitaciones
	 * @param string $idQuiniela
	 * @param int $idFacebook
	 */
	
	private function validainvitacion($idQuiniela, $idFacebook) {
		$invitacion = $this->_em->getRepository('Default_Model_Invitados')->findBy(array('Quiniela'=>$idQuiniela, 'facebookid' => $idFacebook));
		
		if(count($invitacion) > 0){
			return false;
		} else {
			return true;
		}
	}
	
	
	/**
	 * Comprueba si un usuario está registrado
	 * @param int $idfb
	 */
	private function usuarioregistrado($idfb){
		$suario = $this->_em->getRepository('Default_Model_Usuario')->findBy(array('facebookId'=>$idfb));
		
		if (count($suario)> 0) {
			return $suario[0]->getId();
		} else {
			return false;
		}
	}
}