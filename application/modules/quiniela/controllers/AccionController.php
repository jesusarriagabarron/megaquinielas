<?php
/**
 * Clase Centro de Control 
 * @author jarriaga
 *
 */
class quiniela_accionController extends My_Controller_Action {
	/**
	 * Muestra la lista de notificaciones
	 */

	
	public function detalleAction(){
		$request		=	$this->getRequest();
		$idQuiniela		=	$request->getParam('id','');
		$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela,"activo"=>0));
		if(!$Quiniela)
		{
			
		}
		$this->view->Quiniela 	=	$Quiniela;
		
	}
	
	
	public function imprimirOxxoAction(){
		$idBoleta	=	$this->getRequest()->getParam("boleta","");
		$boleta 	=	$this->_em->find("Default_Model_Boleta",$idBoleta);
		//si no hay boleta, a la goma
		if(!$boleta)
			$this->_redirect("/");
		//obtenemos el cargo
		$cargo = $boleta->getCargo();
		if(!$cargo)
			$this->_redirect("/");
		//si no es oxxo
		if($cargo->getPaymentMethod()<>"oxxo")
			$this->_redirect("/");
		
		if($boleta->getUsuario()->getId()<>$this->_auth["id"])
			$this->_redirect("/");
		
		
		$this->_helper->layout()->disableLayout();
		$pdf = Zend_Pdf::load(APPLICATION_PATH."/../public/pdf/codigoOxxo.pdf");
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
		$pagina1=$pdf->pages[0];
		$pagina1->setFont($font,10);
		$pagina1->drawText("Código de Barras: ",150,453,'UTF-8');
		$pagina1->drawText($cargo->getBarcode(),230,430,'UTF-8');
		$pagina1->setFont($font,14);
		$pagina1->drawText("A pagar: MX$".number_format(($cargo->getAmount()/100),2,'.',','),250,350,'UTF-8');
		$pagina1->setFont($font,12);
		$pagina1->drawText("Boleta # ".$boleta->getId()." de la Quiniela ".$boleta->getQuiniela()->getId(),200,320,'UTF-8');
		//obtenemos el codigo
		$imagePath = APPLICATION_PATH."/../public/pdf/codebar/".$cargo->getBarcode().".jpg";
		//si no existe el archivo lo descargamos
		if(!file_exists($imagePath)){
			
			// Only the text to draw is required 
			$barcodeOptions = array('text' => $cargo->getBarcode()); 

			// No required options 
			$rendererOptions = array(); 
	
			// Draw the barcode in a new image, 
			$imageResource = Zend_Barcode::draw( 
			    'code128', 'image', $barcodeOptions, $rendererOptions 
			); 

			imagejpeg($imageResource, $imagePath, 100); 

			// Free up memory 
			imagedestroy($imageResource); 


			/*
			$ch = curl_init();
			$timeout = 60;
			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); // check common name and verify with host name
			curl_setopt($ch, CURLOPT_SSLVERSION,3); // verify ssl version 2 or 3
			$image = curl_exec($ch);
			curl_close($ch);
			$savefile = fopen($imagePath, 'w');
			fwrite($savefile, $image);
			fclose($savefile);*/
		}
		
		$image = Zend_Pdf_Image::imageWithPath($imagePath);
		$pagina1->drawImage($image, 200,380,442,420);
		$pdf->pages[0]=$pagina1;
		$this->getResponse()->setHeader('Content-type', 'application/pdf', true);
		$this->getResponse()->setHeader('Content-disposition', 'inline; filename=PagoBoletaOxxo.pdf', true);
		$this->getResponse()->setBody($pdf->render());
		
	}
	
}
