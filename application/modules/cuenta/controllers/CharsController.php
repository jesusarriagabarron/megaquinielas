<?php
/**
 * Clase chars  
 * @author pablo
 *
 */
class cuenta_CharsController extends My_Controller_Action {

	
	private $_totalPartidos = 0;
	
	/**
	 * Genera una grafica de aciertos por participante
	 */
	
	public function ganadoresAction(){
		$usuario  = $this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
		$dataq    = $usuario->getQuinielasParticipando();
		$this->view->quinielas 	=	$dataq;
		
		$validar    = new My_Validador();
		$idquiniela = $validar->alphanumValido($this->getRequest()->getParam("quiniela",""));
		
		$this->view->show = false;
		
		if($idquiniela){
			$quiniela   = $this->_em->find("Default_Model_Quiniela",$idquiniela);
			
			$resultados = $this->getResultados($quiniela);
			$json = $this->getJsonChar($resultados);
			
			$this->view->show       = true;
			$this->view->quiniela   = $quiniela;
			$this->view->json       = $json;
			$this->view->maximum    = $this->_totalPartidos;
			 
			
		}
	}
	
	/**
	 * Regresa los datos necesarios para armar la grafica 
	 * @param array $resultados
	 */
	private function getJsonChar($resultados){
		$data  = array();
		$count = 0;
		foreach($resultados as $elemento){
			
			$data[$count]['name']   = $elemento['usuario'];
			$data[$count]['points'] = $elemento['aciertos'];
			$data[$count]['color']  = "#00FF02";
			$data[$count]['bullet'] = "http://graph.facebook.com/".$elemento['facebookid']."/picture?width=24&height=24&type=square";
			$count ++;
		}
		
		return json_encode($data);
	}
	
	
	/**
	 * Obtenemos los resultados por boleta
	 * @param object $quiniela
	 */
	private function getResultados($quiniela){
		$resultados = array();
		//Obtenemos las boletas de la quiniela
		
		$boleta = $quiniela->getobjBoletas();
		$count = 0;
		foreach($boleta as $item){
			//Valida que la boleta este activa
			if($item->getStatus()){
				$resultados[$count]['idBoleta']   =  $item->getId();
				$resultados[$count]['idUsuario']  =  $item->getUsuario()->getId();
				$resultados[$count]['usuario']    =  $item->getUsuario()->getNombreUsuario();
				$resultados[$count]['facebookid'] =  $item->getUsuario()->getfacebookid();
				//Obtenemos el pronostico  y lo comparamos contra el resultado en caso de existir
				$boletaresultado = $this->_em->getRepository("Default_Model_Resultados")->findBy(array("boleta"=>$item->getId()));
				$this->_totalPartidos = count($boletaresultado);
				$c = 0;
				$aciertos   =  0;
				$resultados[$count]['aciertos'] = $aciertos ;
				foreach($boletaresultado as $row){
					$partido = $this->_em->find("Default_Model_Partido", $row->getPartido()->getId());
					$resultados[$count][$c]['idPartido']  = $row->getPartido()->getId();
					$resultados[$count][$c]['pronostico'] = $row->getPronostico();
					$resultados[$count][$c]['resultado']  = $partido->getResultadoPartido();
					
					if($resultados[$count][$c]['resultado'] == $resultados[$count][$c]['pronostico']){
						$resultados[$count]['aciertos'] += 1;  
					}
					$c ++;
				}
			}
			$count ++;
		}
		return $resultados; 
	}
}