<?php
/**
 * Panel de administración
 * 
 * @author jarriaga
 *
 */


class admin_ControlController extends My_Controller_Action {
	
	/**
	 * Guarda un resultado via post
	 */
	public function saveResultAction(){
		$request		=		$this->getRequest();
		$data = "error";
		if($request->isPost()){
			$idPartido	=	$request->getParam("idPartido",0);
			$filtro 	=	new Zend_Filter_Digits();
			$local		= $filtro->filter($request->getParam("local",9999));
			$visita		= $filtro->filter($request->getParam("visita",9999));
			
			if($idPartido){
				$partido	=	$this->_em->getRepository("Default_Model_Partido")->findBy(array("id"=>$idPartido));
				$partido	=	$partido[0];
				if($partido){
					//Opciones Local=L		VISITA=V		EMPATE=E       NO JUGADO / ACTIVO = N
					$result = "N";
					if($local==9999 || $visita==9999){
						$result="N";
						$local=null;
						$visita=null;
						$data=$result;
					}else{
						if($local>$visita){$result="L";}
						if($local<$visita){$result="V";}
						if($local==$visita){$result="E";}
						$data=$result;
					}
					//guardamos el resultado y el marcador 
					$partido->setResultadoPartido($result);
					$partido->setGolesLocal($local);
					$partido->setGolesVisita($visita);
					$this->_em->persist($partido);
					$this->_em->flush();
				}
			}
		}
		$this->_helper->json->sendJson($data);
	}
	/**
	 * plantilla para carga de resultados
	 */
	public function resultadosAction(){
		$tablaPartidos			=	new My_Model_Partidos();
		$partidos				=	$tablaPartidos->getPartidosResultados();
		$this->view->equipos	=	$partidos;			
	}
	
	public function indexAction(){
		
	}
	
	/**
	 * muetra los webhooks
	 */
	public function webhooksAction(){
		$this->view->webhooks 	=	$this->_em->getRepository("Default_Model_Webhook")->findAll();
	}
	
	public function jsonCodeAction(){
		$id = $this->getRequest()->getParam("id","");
		$filtro	=	new Zend_Filter_Digits();
		$id = $filtro->filter($id);
		$wh 	=	$this->_em->getRepository("Default_Model_Webhook")->findBy(array("id"=>$id));
		if($wh){
			$this->view->conekta = $wh[0]->getData();
			$this->view->id		 = $wh[0]->getChargeId();
		}
	}
	
	/**
	 * muestra los usuarios regisrados
	 */
	public function usersAction(){
		$this->view->usuarios	=	$this->_em->getRepository("Default_Model_Usuario")->findAll();
	}
	
	/**
	 * Muestra las quinielas registradas
	 */
	public function quinielasAction(){
		$this->view->quinielas = $this->_em->getRepository("Default_Model_Quiniela")->findAll();
	}
	

	
	public function detallequinielaAction(){
		$params  = $this->getRequest()->getParams();
		$validar = new My_Validador();
		
		$idquiniela = $validar->alphanumValido($params['id']);

		if($idquiniela != '') {
			$quiniela = $this->_em->find("Default_Model_Quiniela", $idquiniela);
			
			if($quiniela) {
				$partidos = $this->_em->getRepository("Default_Model_QuinielaPartidos")->findBy(array("Quiniela"=>$quiniela));
				if($partidos){
					$this->view->partidos = $partidos;
				}
				$this->view->quiniela = $quiniela;
				
			}
		}
	}

	/**
	 * Muestra el catálogo de eventos
	 */
	public function catalogoEventosAction(){
		$eventos 			=		$this->_em->getRepository("Default_Model_Evento")->findAll();	
		$this->view->eventos		=		$eventos;
	}
	
	
	/**
	 * Guarda un evento nuevo
	 */
	public function saveEventoAction(){
		$request	=	$this->getRequest();
		if($request->isPost()){
			$filtro 		=		new Zend_Filter_Alnum(array('allowwhitespace' => true));
			$nombreEvento	=		$filtro->filter($request->getParam("nombreEvento"));
			$filtro			=		new Zend_Filter_StripTags();
			$nombreEvento	=		$filtro->filter($nombreEvento);
			if(strlen($nombreEvento)>3){
				$evento		=		new Default_Model_Evento();
				$evento->setFechacreacion(new DateTime("now"));
				$evento->setNombreEvento($nombreEvento);
				$evento->setDespublicar();
				$this->_em->persist($evento);
				$this->_em->flush();
				$this->_helper->flashMessenger->addMessage('success | Se ha creado exitósamente el evento:  '.strtoupper($evento->getNombreEvento()));
				$this->_redirect("/admin/control/catalogo-eventos");
			}
		}
		
		$this->_helper->flashMessenger->addMessage('error | Error no se pudo crear el evento ');
		$this->_redirect("/admin/control/catalogo-eventos");
		
		$this->_helper->json->sendJson("ok");
	}
	
	public function catalogoPartidosAction(){
		
	}
	
	

	/**
	 * Activa o desactiva un partido
	 */
	public function activarPartidoAction(){
		$request	=	$this->getRequest();
		$idPartido	=	 (int) $request->getParam("id",0);
		$idEvento	=	(int)	$request->getParam("evento",0);
		$s			=	(int) $request->getParam("s",0);
	
		$partido		=	$this->_em->getRepository("Default_Model_Partido")->findBy(array("id"=>$idPartido));
		$result		=	0;
		if($partido){
			$partido	=	$partido[0];
			if($s==1){
				$this->_helper->flashMessenger->addMessage('success | Partido ACTIVADO exitosamente');
				$partido->setActivo(1);}
				else{
					$this->_helper->flashMessenger->addMessage('success | Partido DESACTIVADO exitosamente - ');
					$partido->setActivo(0);}
						
					$this->_em->persist($partido);$this->_em->flush();
					$result	=	1;
						
					$this->_redirect("/admin/control/cargar-partidos/id/".$idEvento);
		}
	
		$this->_helper->flashMessenger->addMessage('error | Error al publicar el partido - '.$evento->getNombreEvento());
		$this->_redirect("/admin/control/");
	
		$this->_helper->json->sendJson($result);
	}

	
	/**
	 * Activa o desactiva un evento
	 */
	public function activarEventoAction(){
		$request	=	$this->getRequest();
		$idEvento	=	 (int) $request->getParam("id",0);
		$s			=	(int) $request->getParam("s",0);
		
		$evento		=	$this->_em->getRepository("Default_Model_Evento")->findBy(array("id"=>$idEvento));
		$result		=	0;
		if($evento){
			$evento	=	$evento[0];
			if($s==1){
				$this->_helper->flashMessenger->addMessage('success | Evento PUBLICADO exitosamente - '.$evento->getNombreEvento());
				$evento->setPublicar();}
			else{
				$this->_helper->flashMessenger->addMessage('success | Evento DESACTIVADO exitosamente - '.$evento->getNombreEvento());
				$evento->setDespublicar();}
			
			$this->_em->persist($evento);$this->_em->flush();
			$result	=	1;
			
			$this->_redirect("/admin/control/catalogo-eventos");
		}
		
		$this->_helper->flashMessenger->addMessage('error | Error al publicar el evento - '.$evento->getNombreEvento());
		$this->_redirect("/admin/control/");
		
		$this->_helper->json->sendJson($result);
	}
	
	/**
	 * Carga los partidos a un evento específico
	 */
	public function cargarPartidosAction(){
		$request		=		$this->getRequest();
		$idEvento		=		$request->getParam("id",0);
		$evento		=	$this->_em->getRepository("Default_Model_Evento")->findBy(array("id"=>$idEvento));
		if(!$evento){
			$this->_helper->flashMessenger->addMessage('error | No existe el evento');
			$this->_redirect("/admin/control/");
		}
				
		$partidos	=		$this->_em->getRepository("Default_Model_Partido")->findBy(array("evento"=>$idEvento));
		
		$rowset		=	array();
		$row		=	array();
		
		foreach ($partidos as $partido){
			$row["activo"]	=	$partido->getActivo();
			$row["id"]		=	$partido->getId();
			$row["fecha"]	=	$partido->getFechaPartido();
			
			$equipoL		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoLocalId());
			$equipoLocal["nombre"]		=	$equipoL->getNombre();
			$equipoLocal["nombreCorto"]	=	$equipoL->getNombreCorto();
			$equipoLocal["escudo"]		=	$equipoL->getEscudo();
			
			$equipoV		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoVisitaId());
			$equipoVisita["nombre"]		=	$equipoV->getNombre();
			$equipoVisita["nombreCorto"]	=	$equipoV->getNombreCorto();
			$equipoVisita["escudo"]		=	$equipoV->getEscudo();
				
			$row["tipo"]			=	$partido->getTipoPartido();
			$row["equipoLocal"]		=	$equipoLocal;
			$row["equipoVisita"]	=	$equipoVisita;
			
			$rowset[]	=	$row;
			
		}
		
		
		
		$evento	=	$evento[0];
		$this->view->evento		=		$evento;
		$this->view->equipos	=		$rowset;
		
	}
	
	/**
	 * Guardar el partido al evento
	 */
	public function savePartidoAction(){
		$request	=	$this->getRequest();
		if($request->isPost()){
			$equipoLocalId			=		$request->getParam("equipoLocalId",0);
			$equipoVisitanteId		=		$request->getParam("equipoVisitaId",0);
			$fechahora				=		$request->getParam("fechahora");
			$eventoId				=		$request->getParam("eventoId",0);
			$tipoPartido			=		$request->getParam("tipoPartido","");
			
			$evento					=		$this->_em->find("Default_Model_Evento",$eventoId);
			
			$nuevoPartido		=		new Default_Model_Partido();
			$nuevoPartido->setActivo(0);
			$nuevoPartido->setEquipoLocalId($equipoLocalId);
			$nuevoPartido->setEquipoVisitaId($equipoVisitanteId);
			$nuevoPartido->setFechaPartido(new DateTime($fechahora));
			$nuevoPartido->setEvento($evento);
			$nuevoPartido->setTipoPartido(strtoupper($tipoPartido));
			
			$this->_em->persist($nuevoPartido);
			$this->_em->flush();
			
			$this->_helper->flashMessenger->addMessage('success | Partido creado exitosamente ');
			$this->_redirect("/admin/control/cargar-partidos/id/".$eventoId);
			
			
		}
	}
	
	/**
	 * Guarda un equipo nuevo
	 */
	public function saveEquipoAction(){
		$request 		=		$this->getRequest();
		if($request->isPost()){
			$equipo						=		new Default_Model_Equipo();
			$filtro						= 		new Zend_Filter_Alnum();
			$filtro->allowWhiteSpace 	= true;
			
			$equipo->setEscudo($request->getParam("nombrearchivo"));
			$equipo->setNombre(strtoupper($this->stripAccents($request->getParam("nombre"))));
			$equipo->setNombreCorto(strtoupper($request->getParam("nombreCorto")));
			$equipo->setPais($request->getParam("pais"));
			$equipo->setFechaCreacion();
			
			$this->_em->persist($equipo);
			$this->_em->flush();
			
			$this->_helper->flashMessenger->addMessage('success | Equipo creado exitosamente');
			$this->_redirect("/admin/control/catalogo-equipos");
			
		}
		$this->_helper->json->sendJson('ok');
	}

	public function catalogoEquiposAction(){
		
		$equipos 	=	$this->_em->getRepository("Default_Model_Equipo")->findBy(array(),array('nombre'=>'ASC'));
		$this->view->equipos =	$equipos;
	}
	
	public function getEquiposJsonAction(){
		$this->_helper->layout()->disableLayout();
		$term	=	$this->getRequest()->getParam("term","");
		$term = trim(strip_tags($term));
		
		$query 		= 	$this->_em->createQuery("SELECT e FROM Default_Model_Equipo e WHERE e.nombre like :termino");
		$query->setParameter("termino","%".$term."%");
		
		$equipos 	=	$query->getResult();
		$arregloT=array();
		foreach($equipos as $equipo){
			$arreglo["value"]	= $equipo->getNombre();
			$arreglo["image"]	= $equipo->getEscudo();
			$arreglo["id"]	= $equipo->getId();
			$arregloT[]=$arreglo;
		}
		
	
		return $this->_helper->json->sendJson($arregloT);
	}

		function stripAccents($cadena){
			 	$cadena=str_replace("á", "a", $cadena);
			    $cadena=str_replace("é", "e", $cadena);
			    $cadena=str_replace("í", "i", $cadena);
			    $cadena=str_replace("ó", "o", $cadena);
			    $cadena=str_replace("ú", "u", $cadena);
			    $cadena=str_replace("Á", "A", $cadena);
			    $cadena=str_replace("É", "E", $cadena);
			    $cadena=str_replace("Í", "I", $cadena);
			    $cadena=str_replace("Ó", "O", $cadena);
		    	$cadena=str_replace("Ú", "U", $cadena);
		    	return $cadena;   
		}
	
}


