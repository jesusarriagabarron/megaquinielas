<?php
/** @Entity */
//require "Doctrine/Common/Collections/ArrayCollection.php";
/** @Entity
 * @Table(name="Quiniela")
 * 
*/
class Default_Model_Quiniela{

    /**
    * @Id
    * @Column(type="string")
    */
    private $id;
    
    /** @Column(type="string",length=100) **/
    private $titulo;
    
	/** @Column(type="text",length=300,nullable=true)**/
    private $descripcion;
    
    // 0	= 	CERRADO
    // 1	=	ABIERTO
	/** @Column(type="integer",options={"default":1})**/
    private $status=1;

    //  0 = PRIVADO
    //	1 = PUBLICO
    /** @Column(type="integer",options={"default":1})**/
    private $tipo=1;
    
    /** @Column(type="datetime")  **/
    private $fechaCreacion;
    
    /** @Column(type="datetime")  **/
    private $fechaCierre;
    
    /** @Column(type="decimal") **/
    private $premio;

    /** @Column(type="decimal") **/
    private $precioBoleta;
    
    /** @Column(type="integer",options={"default":0}) **/
    private $activo=0;
    
    /** @Column(type="integer",options={"default":1}) **/
    private $aceptaterminos=1;
 
    /**
     * @ManyToOne(targetEntity="Default_Model_Usuario",inversedBy="quinielas")
     * @JoinColumn(name="idUsuario",referencedColumnName="id")
     */
    private $usuario;
    
    /**
     * @OneToMany(targetEntity="Default_Model_QuinielaPartidos",mappedBy="Quiniela")
     * 
     **/
    private $Quinielapartidos;
    
    
    /**
     * @OneToMany(targetEntity="Default_Model_Invitados",mappedBy="Quiniela")
     **/
    private $invitados;
    
    /**
     * @OneToMany(targetEntity="Default_Model_Boleta",mappedBy="quiniela")
     **/
    private $boletas;
    
    public function setId($id)					    {		$this->id=$id;									}
    public function setStatus($status=1)			{		$this->status=$status;    	                 	}
    public function setTipo($tipo=1)				{		$this->tipo=$tipo;  	  	                 	}
    public function setTitulo($titulo)			    {    	$this->titulo=$titulo;	                     	}
    public function setFechaCreacion()			    {    	$this->fechaCreacion = new DateTime("now");  	}
    public function setFechaCierre($fechaCierre)	{   	$this->fechaCierre = $fechaCierre;    			}
    public function setPremio($monto)				{    	$this->premio= $monto;				 		 	}
    public function setPrecioBoleta($precioBoleta)  {    	$this->precioBoleta= $precioBoleta;	 		 	}
   	public function setDescripcion($descripcion)	{  		$this->descripcion = $descripcion;				}
   	public function setActivo($activo)			    {		$this->activo	=	$activo;					}
   	public function setInvitado(Default_Model_Invitados $invitado)		    {		$this->invitados[]=$invitado;}
   	
   	
   	public function setUsuario(Default_Model_Usuario $usuario){
   		if($this->usuario !== $usuario ){
   			$this->usuario=$usuario;
   			$usuario->setQuiniela($this);
   		}
   	}
   	
   	public function getId()							    {		return 	$this->id;				 }
   	public function getStatus()						    {		return 	$this->status;			 }
   	public function getTipo()							{		return 	$this->tipo;			 }
   	public function getTitulo()						    {		return	$this->titulo;			 }
   	public function getFechaCreacion()				    {		return 	$this->fechaCreacion;	 }
   	public function getFechaCierre()					{		return 	$this->fechaCierre;		 }
   	public function getPremio()						    {		return 	$this->premio;			 }
   	public function getPrecioBoleta()					{		return	$this->precioBoleta;	 }
   	public function getDescripcion()					{		return 	$this->descripcion;		 }
   	public function getUsuario()						{		return 	$this->usuario;			 }
    public function getActivo()						    {		return  $this->activo;			 }
    public function getQuinielaPartidos()				{		return  $this->Quinielapartidos; }
    public function getNumeroBoletas()				    {		return  count($this->boletas);	 }
    public function getInvitados()						{		return  $this->invitados;		 }
    public function getobjBoletas()                     {       return $this->boletas;           }
    
    public function verificaInvitacion($idUsuario)		{
    	foreach ($this->invitados as $invitado){
    		if($invitado->getIdUsuario()==$idUsuario){
    			return true;
    		}
    	}
    	return false;
    }
    
    public function getUsuariosParticipando(){
    	$usuariosParticipando = array();
    	$objUsuario= array();
    	foreach ($this->boletas as $boleta){
    		if($boleta->getStatus()==1){
	    		$usuariosParticipando[]=$boleta->getUsuario()->getId();
	    		$objUsuario[$boleta->getUsuario()->getId()]= $boleta->getUsuario();
    		}
    	}
    	$usuariosParticipando = array_unique($usuariosParticipando);
    	$usuariosFinales = array();
    	foreach($usuariosParticipando as $usuario ){
    		$usuariosFinales[]=$objUsuario[$usuario];
    	}
		return $usuariosFinales;    	
    }
    
    public function getNumeroBoletasParticipando()		{
    	$boletasParticipando=0;	
    			foreach($this->boletas as $boleta ){
    					if($boleta->getStatus()==1)
    						$boletasParticipando++;
    			} 	
    	return $boletasParticipando;		
    }
    
    public function actualizaPremio($em){
    	$this->premio	= $this->getNumeroBoletasParticipando()*$this->precioBoleta;
    	$em->persist($this);
    	$em->flush();
    }
    
    public function getBoletasActivas(){
    	$data = array();
    	$count = 0;
    	foreach($this->boletas as $boleta){
    		if($boleta->getStatus() == 1){
    			$data[$count]['id']             = $boleta->getId();
    			$data[$count]['cargo']          = $boleta->getCargo();
    			$data[$count]['fecahacreacion'] = $boleta->getFechaCreacion();
    			$count++;
    		}
    	}
    	return $data;
    }
    
    public function getBoletas(){
    	$data = array();
    	$count = 0;
    	foreach($this->boletas as $boleta){
    		$data[$count]['id']             = $boleta->getId();
    		$data[$count]['cargo']          = $boleta->getCargo();
    		$data[$count]['fecahacreacion'] = $boleta->getFechaCreacion();
    		$data[$count]['status']         = $boleta->getStatus();
    		$data[$count]['usuario']        = $boleta->getUsuario()->getNombreUsuario();
    		$count++;
    	}
    	
    	return $data;
    }
    public function getNumeroPartidos()				{		return count($this->Quinielapartidos);	}
}