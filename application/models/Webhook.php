<?php
/** @Entity
 * @Table(name="Webhook")
 * */
class Default_Model_Webhook {
	
	/**
	 * @Id
	 * @GeneratedValue(strategy="AUTO")
	 * @Column(type="integer")
	 */
	private $id;
	/**
	 * @Column(type="string")
	 */
	private $chargeId;
	/** @Column(type="string",length=50) **/
	private $type;
	/** @Column(type="integer",nullable=true) **/
	private $created_at;
	/** @Column(type="string",length=50,nullable=true) **/
	private $object_id;
	/** @Column(type="string",length=50,nullable=true) **/
	private $object_type;
	/** @Column(type="string",length=50,nullable=true) **/
	private $object_livemode;
	/** @Column(type="integer",nullable=true) **/
	private $object_created_at;
	/** @Column(type="string",length=50,nullable=true) **/
	private $object_status;
	/** @Column(type="string",length=200,nullable=true) **/
	private $object_description;
	/** @Column(type="string",length=200,nullable=true) **/
	private $object_reference_id;
	/** @Column(type="integer",nullable=true) **/
	private $object_amount;
	/** @Column(type="integer",nullable=true) **/
	private $object_fee;
	/** @Column(type="string",length=50,nullable=true) **/
	private $object_payment_object;
	/** @Column(type="string",length=50,nullable=true) **/
	private $object_payment_type;
	/** @Column(type="string",length=50,nullable=true) **/
	private $object_previous_status;
	/** @Column(type="array") **/
	private $data;
	
	// SET
	public function setChargeId		($cid){						$this->chargeId				=			$cid;				}
	public function setType		($type){					$this->type				=			$type;				}
	public function setCreateAt	($created_at){				$this->created_at		=			$created_at;		}
	public function setObjectType($object_type){			$this->object_type		=			$object_type;		}
	public function setObjectId ($object_id){				$this->object_id		=			$object_id;			}
	public function setObjectLivemode($object_livemode){	$this->object_livemode	=			$object_livemode;	}
	public function setObjectCreatedAt($object_created_at){	$this->object_created_at=			$object_created_at;	}
	public function setObjectStatus($object_status){		$this->object_status	=			$object_status;		}
	public function setObjectDescription($object_description){$this->object_description=		$object_description;}
	public function setObjectReferenceId($object_reference_id){$this->object_reference_id=		$object_reference_id;}
	public function setObjectAmount($object_amount){		$this->object_amount	=			$object_amount;		}
	public function setObjectFee($object_fee){				$this->object_fee		=			$object_fee;		}
	public function setObjectPaymentObject($object_payment_object){	$this->object_payment_object=$object_payment_object;}
	public function setObjectPaymentType($object_payment_type){	$this->object_payment_type=		$object_payment_type;}
	public function setObjectPreviousStatus($object_previous_status){$this->object_previous_status	=$object_previous_status;}
	public function setData($object){						$this->data				=	serialize($object);			}
	
	// GET
	public function getId(){						return 		$this->id;											}
	public function getChargeId()	{				return 		$this->chargeId;									}
	public function getType()		{				return 		$this->type;										}
	public function getCreatedAt()	{				return 		$this->created_at;									}
	public function getObjectType()	{				return 		$this->object_type;									}
	public function getObjectId()	{				return 		$this->object_id;									}
	public function getObjectLivemode(){			return 		$this->object_livemode;								}
	public function getObjectCreatedAt(){			return 		$this->object_created_at;							}
	public function getObjectStatus(){				return 		$this->object_status;								}
	public function getObjectDescription(){			return 		$this->object_description;							}
	public function getObjectReferenceId(){			return 		$this->object_reference_id;							}
	public function getObjectAmount(){				return 		$this->object_amount;								}
	public function getObjectFee(){					return 		$this->object_fee;									}
	public function getObjectPaymentObject(){		return 		$this->object_payment_object;						}
	public function getOBjectPaymentType(){			return 		$this->object_payment_type;							}
	public function getObjectPreviousStatus(){		return 		$this->object_previous_status;						}
	public function getData()	{			return 		unserialize($this->data);							}
	
	
	
}
