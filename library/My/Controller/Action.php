<?php 

/**
 * Definición de clase de controlador genérico
 *
 * @package library.My.Controller
 * @author pil
 */


require_once 'facebookSDK/facebook.php';

class My_Controller_Action extends Zend_Controller_Action 
{
	/**
	 * Contextos, se pueden modificar en cada uno de los controladores hijos
	 *
	 * @var $contexts: arreglo de contextos
	 */
	public $contexts = array( );
	public $_auth;
	public $_em;
	public $_appid;
	public $_secret;
	public $_facebook;
	
	public function init() {
		$this->view->doctype('XHTML1_RDFA');
		date_default_timezone_set('America/Mexico_City');
		
		if($this->_request->isXmlHttpRequest())
			$this->_request->setParam("format","json");
	
		$contextSwitch = $this->_helper->contextSwitch();
		$contextSwitch->initContext();
		
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
		
		$this->getAuth();
		
		
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$fbkeys = $config->getOption('facebook');
		$this->_appid  = $fbkeys['appid'];
		$this->_secret = $fbkeys['appsecret'];
		
		$this->_facebook = new Facebookphp(array(
				'appId'  => $this->_appid,
				'secret' => $this->_secret
		));
		
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$ck = $config->getOption("conekta");
		$this->view->conektapublic	=	$ck["public"];
		
		$this->view->urlLoginFb = $this->_facebook->getLoginUrl(array('redirect_uri' => $fbkeys['redirect_uri'],
								'scope'=>'email, publish_actions'));
		$idUserFB =  $this->_facebook->getUser();
	}
	

	private function getAuth() {
		
		$auth = Zend_Auth::getInstance();
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
			$this->view->auth = $this->_auth;
		} else {
			$this->_auth = false;
		}
		
	}
	
}