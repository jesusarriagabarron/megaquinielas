<?php

/**
 * Manejo de envío de correos 
 */
require_once("mandrillSDK/Mandrill.php");

class My_Model_CorreosMandrill	{

	private $_em;
	private $_auth;
	private $_mandrill;
	private $_twitter;
	private $_facebook;
	private $_config;
	private $_host="";

	public function __construct(){
		
		$registro = Zend_Registry::getInstance();
		$this->_em	=	$registro->entitymanager;
		$auth = Zend_Auth::getInstance();
		if($auth->getStorage()->read())
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		//iniciamos mandrill
		$this->_mandrill = new Mandrill("7a_vHPi_REGrJ4st8aGe7w");
		$this->_facebook	=	"https://www.facebook.com/megaquinielas";
		$this->_twitter		=	"https://www.facebook.com/megaquinielas";
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$url = $request->getScheme() . '://' . $request->getHttpHost() ;
		$this->_host		=	$url;
		
	}
	
	/**
	 * Crea un correo con la plantilla GENERAL ... solo cambia los parámetros y crea tu propio mensaje...
	 * 
	 * @param Default_Model_Usuario $usuario
	 */
	public function enviarBienvenida(Default_Model_Usuario $usuario){
		//Datos generales
		$this->_config["emailDestino"]  = 	$usuario->getEmail();
		$this->_config["nombreDestino"] =	$usuario->getNombreUsuario();
		$this->_config["idFacebook"]	=	$usuario->getfacebookid();
		$this->_config["titulo"]		=	"Bienvenido a MegaQuinielaBrasil2014";
		//identificador para mandril ¡Muy importante para catalogar correos!
		$this->_config["identificador"]	=   "megaquinielas-bienvenida";
		//Mensaje en HTML
		$this->_config["mensajeHTML"]="<p style='text-align:justify;'>
				En horabuena, te damos la bienvenida a nuestro sistema de quinielas sociales online, recuerda que
				 nosotros somos mediadores para organizar tu quiniela con tus amigos, haz la tuya ahora ¿Qué esperas?.
				<br>
				Demuestra que tanto sabes de futbol y gánale a tus amigos.
				<br>
				<br>
				si tienes dudas o sugerencias sobre nuestra plataforma,por favor envíalas al siguiente correo electrónico<br> 
				<a href='mailto:contacto@megaquinielabrasil2014.com' target='_top'>contacto@megaquinielabrasil2014.com</a>
				</p>";
		$this->sendTemplate();
	}
	
	
	/**
	 * Nueva Bolea en una quiniela
	 * @param Default_Model_Usuario $usuario
	 * @param Default_Model_Boleta $boleta
	 */
	public function nuevaBoletaEnQuiniela(Default_Model_Usuario $usuario, Default_Model_Boleta $boleta){
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$url = $request->getScheme() . '://' . $request->getHttpHost() ;
		$this->_host		=	$url;
		//Datos generales
		$this->_config["emailDestino"]  = 	$usuario->getEmail();
		$this->_config["nombreDestino"] =	$usuario->getNombreUsuario();
		$this->_config["idFacebook"]	=	$usuario->getfacebookid();
		$this->_config["titulo"]		=	"Tu Quiniela tiene una nueva Boleta";
		//identificador para mandril ¡Muy importante para catalogar correos!
		$this->_config["identificador"]	=   "megaquinielas-BoletaEnQuiniela";
		//Mensaje en HTML
		$this->_config["mensajeHTML"]="<p style='text-align:justify;'>
				Tu quiniela sigue creciendo, te confirmamos que se ha agregado una nueva boleta a tu quiniela. 
				 no dejes de invitar a tus amigos para que se registren en tu quiniela y la bolsa acumulada se incremente.
				<br>
				Puedes ver las boletas y usuarios registrados de tu quiniela en el 
				<a href='".$this->_host."/cuenta/control'>centro de control</a><br>
				No. de boletas participando hasta el momento: ".$boleta->getQuiniela()->getNumeroBoletasParticipando()."		 
				<br>
				<br>
				si tienes dudas o sugerencias sobre nuestra plataforma,por favor envíalas al siguiente correo electrónico<br>
				<a href='mailto:contacto@megaquinielabrasil2014.com' target='_top'>contacto@megaquinielabrasil2014.com</a>
				</p>";
		$this->sendTemplate();
	}
	
	
	
	/**
	 * Envía un aviso de compra de boleta por oxxo
	 * 
	 * @param Default_Model_Usuario $usuario
	 * @param Default_Model_Boleta $boleta
	 */
	public function enviarAvisoOxxo(Default_Model_Usuario $usuario,Default_Model_Boleta $boleta){
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$url = $request->getScheme() . '://' . $request->getHttpHost() ;
		$this->_host		=	$url;
		//Datos generales
		$this->_config["emailDestino"]  = 	$usuario->getEmail();
		$this->_config["nombreDestino"] =	$usuario->getNombreUsuario();
		$this->_config["idFacebook"]	=	$usuario->getfacebookid();
		$this->_config["titulo"]		=	"Codigo de barras tiendas OXXO";
		//identificador para mandril ¡Muy importante para catalogar correos!
		$this->_config["identificador"]	=   "megaquinielas-codigooxxo";
		//Mensaje en HTML
		$this->_config["mensajeHTML"]="
				<p style='text-align:justify;'>
				Has solicitado tu forma de pago vía tiendas oxxo, te recordamos que puedes 
				pagar en cualquier tienda oxxo del país.<br>
				Una vez realizado el pago, deberás esperar 24 horas para la confirmación del pago de tu boleta,
				en cuanto sea válida la recepción del pago, tu boleta estará participando en la quiniela que seleccionaste. y 
				te avisaremos vía mail del status de tu participación. 
				<br><br>
				Si deseas reimprimir tu código de barras, da click en el siguiente enlace <br>
				<strong><a href='".$this->_host."/quiniela/accion/imprimir-oxxo/boleta/".$boleta->getId()."' target='_blank'>Imprimir código OXXO</a></strong>
				<br>
				<br>
				si tienes dudas o sugerencias sobre nuestra plataforma,por favor envíalas al siguiente correo electrónico<br>
				<a href='mailto:contacto@megaquinielabrasil2014.com' target='_top'>contacto@megaquinielabrasil2014.com</a>
				</p>";
		$this->sendTemplate();
	}
	
	/**
	 * Crea y envía un correo de confirmación de recepción de pago.
	 * @param Default_Model_Usuario $usuario
	 */
	public function enviarConfirmacionPagoOxxo($parametros){
		//Datos generales
		$this->_config["emailDestino"]  = 	$parametros["email"];
		$this->_config["nombreDestino"] =	$parametros["nombreUsuario"];
		$this->_config["idFacebook"]	=	$parametros["idFacebook"];
		$this->_config["titulo"]		=	"Confirmación de pago OXXO";
		$this->_config["monto"]			=	$parametros["monto"];
		$this->_config["idBoleta"]		=	$parametros["idBoleta"];
		$this->_config["idQuiniela"]	=	$parametros["idQuiniela"];
		//identificador para mandril ¡Muy importante para catalogar correos!
		$this->_config["identificador"]	=   "megaquinielas-pagoboletaoxxo";
		//Mensaje en HTML
		$this->_config["mensajeHTML"]="<p style='text-align:justify;'>
				Hemos recibido el pago de tu boleta en OXXO por <span style='color:#0b0;'><strong>MX$".number_format($this->_config["monto"]/100,2,'.',',')."</strong></span> pesos mexicanos
				<br>
				Tu boleta ya esta participando ahora. ¡buena suerte!
				<br><br>
				<strong>Detalle de tu boleta:</strong>
				<br>
				<br>
				<strong>
				<a href='http://www.megaquinielabrasil2014.com/quiniela/".$this->_config["idQuiniela"]."/boleta/".$this->_config["idBoleta"]."'>Boleta # ".$this->_config["idBoleta"]."</a><br>
				<a href='http://www.megaquinielabrasil2014.com/quiniela/".$this->_config["idQuiniela"]."'>Quiniela: ".$this->_config["idQuiniela"]."</a><br>
				Costo de la boleta: MX$".number_format(($this->_config["monto"]/100),2,'.',',')."<br>
				</strong>
				<br>
				si tienes dudas o sugerencias sobre nuestra plataforma,por favor envíalas al siguiente correo electrónico<br>
				<a href='mailto:contacto@megaquinielabrasil2014.com' target='_top'>contacto@megaquinielabrasil2014.com</a>
				</p>";
		$this->sendTemplate();
	}
	/**
	 * Crea y envía un correo de confirmación de recepción de pago.
	 * @param Default_Model_Usuario $usuario
	 */
	public function enviarConfirmacionPago(Default_Model_Usuario $usuario,$monto,Default_Model_Boleta $boleta){
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$url = $request->getScheme() . '://' . $request->getHttpHost() ;
		$this->_host		=	$url;
		//Datos generales
		$this->_config["emailDestino"]  = 	$usuario->getEmail();
		$this->_config["nombreDestino"] =	$usuario->getNombreUsuario();
		$this->_config["idFacebook"]	=	$usuario->getfacebookid();
		$this->_config["titulo"]		=	"Confirmación de pago de boleta";
		//identificador para mandril ¡Muy importante para catalogar correos!
		$this->_config["identificador"]	=   "megaquinielas-pagoboleta";
		//Mensaje en HTML
		$this->_config["mensajeHTML"]="<p style='text-align:justify;'>
				Hemos recibido el pago de tu boleta por <span style='color:#0b0;'><strong>MX$".number_format($monto/100,2,'.',',')."</strong></span> pesos mexicanos 
				<br>
				Tu boleta ya esta participando ahora. ¡buena suerte!
				<br><br>
				<strong>Detalle de tu boleta:</strong>
				<br>
				<br>
				<strong>
				Boleta # <a href='".$this->_host."/quiniela/".$boleta->getQuiniela()->getId()."/boleta/".$boleta->getId()."'>".$boleta->getId()."</a><br>
				Quiniela: <a href='".$this->_host."/quiniela/".$boleta->getQuiniela()->getId()."'>".$boleta->getQuiniela()->getId()."</a><br>
				Costo de la boleta: MX$".number_format($boleta->getQuiniela()->getPrecioBoleta(),2,'.',',')."<br>
				No. de boletas participando hasta el momento: ".$boleta->getQuiniela()->getNumeroBoletasParticipando()."<br>
				</strong>
				<br>
				si tienes dudas o sugerencias sobre nuestra plataforma,por favor envíalas al siguiente correo electrónico<br>
				<a href='mailto:contacto@megaquinielabrasil2014.com' target='_top'>contacto@megaquinielabrasil2014.com</a>
				</p>";
		$this->sendTemplate();
	}
	
	
	public function sendTemplate(){
		$f=new DateTime('now');
		
		$template_name= "megaquinielabrasil2014-general";
		$template_content = array(
				array(
						"name" => "example name",
						"content" => "example content"
				)
		);
		$message = array(
				"subject"		=>	$this->_config["titulo"],
				"from_email"	=>	"no.reply@megaquinielabrasil2014.com",
				"from_name"		=>	"MegaQuinielaBrasil2014.com",
				"to"			=>	array(
										array(
										"email"		=>		$this->_config["emailDestino"] ,
										"name"		=>		$this->_config["nombreDestino"] ,
										"type"		=>		"to"
									 	)
	        					  	),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array($this->_config["identificador"]),
				"global_merge_vars" => array(
						array("LIST:COMPANY"				=> "MegaquinielasBrasil2014")
				),
				"merge_vars" => array(
						array(
								"rcpt" => $this->_config["emailDestino"] ,
								"vars" => array(
										array(
												"name" => "COMPRADOR",
												"content" => $this->_config["nombreDestino"] 
										),
										array(
												"name" => "COMPANY",
												"content" => "MegaquinielasBrasil2014"
										),
										array(
												"name" => "CURRENT_YEAR",
												"content" => $f->format('Y')  
										),
										array(
												"name"		=>	"MENSAJE",
												"content"	=>	$this->_config["mensajeHTML"]
										),
										array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
										array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
								)
						)
				),
				"recipient_metadata" => array(
						array(
								"rcpt" => $this->_config["emailDestino"] ,
								"values" => array("user_id" => $this->_config["idFacebook"] )
						)
				),
		);// Fin array message
				
		$async = false;
		$ip_pool = "Main Pool";
		$send_at = date("Y-m-d hh:ii:ss",strtotime("-1 day"));
		try{
			$result = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
		} catch(Mandrill_Error $e) {
		
		}
	}
	
	

}