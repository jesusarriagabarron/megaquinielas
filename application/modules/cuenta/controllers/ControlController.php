<?php
/**
 * Clase Centro de Control 
 * @author jarriaga
 *
 */
class cuenta_ControlController extends My_Controller_Action {
	
	/**
	 * Muestra la lista de notificaciones
	 */
    public function indexAction(){
    	$usuario = $this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$q 	= $usuario->getQuinielasParticipando();
    	$this->view->quinielas 	=	$q;
    }
    
    public function misPagosAction(){
    	$usuario						=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$this->view->boletas			=	$usuario->getBoletas();
    }
    
    /**
     * Muestra los partidos para una nueva quiniela
     */
    public function nuevaQuinielaAction(){
    	$request 	= 	$this->getRequest();
    	$param 		=	$request->getParam('selecciona','');
    	$array		=	array();
    	//selecciona los equipos
    	if($param=="equipos"){
    		$evento					=		$this->_em->getRepository('Default_Model_Evento')->findAll();
    		$partidosactivos	=	
    				$this->_em->createQuery("SELECT p FROM
    								Default_Model_Partido p JOIN	p.evento e WHERE
    								e.publicado=1 AND p.activo=1 ORDER BY p.fechaPartido ASC
						   			");
    		
    		$array	=	$partidosactivos->getResult();
    		$rowset	=	$this->partidosArray($array);
    		
    		$this->view->partidos 	=	$rowset;
    		
    	}
    }
    
    /**
     * Muestra el preview de los partidos seleccionados
     */
    public function previewAction(){
    	$request		=	$this->getRequest();
    	$idQuiniela		=	$request->getParam('id','');
    	$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")->findBy(array("id"=>$idQuiniela,"usuario"=>$usuario,"activo"=>0));
    	if(!$Quiniela)
    	{
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no existe o ya esta Activa');
    		$this->_redirect("/cuenta/control");
    	}
    	
    	$this->view->Quiniela	=	$Quiniela[0];
    	$p=array();
    	foreach($Quiniela[0]->getQuinielaPartidos() as $qp){$p[]=$qp->getPartido();}
    	$rowset	=	$this->partidosArray($p);
    	
    	$this->view->partidos = $rowset;

    }
    
    /**
     * Edita una Quiniela no activa
     */
    public function editAction(){
    	$request		=	$this->getRequest();
    	$idQuiniela		=	$request->getParam('id','');
    	$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
    	$Quiniela		=	$this->_em->getRepository("Default_Model_Quiniela")
    	->findBy(array("id"=>$idQuiniela,"usuario"=>$usuario,"activo"=>0));
    	if(!$Quiniela)
    	{
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no existe ó no es activa');
    		$this->_redirect("/cuenta/control");
    	}
    	
    	$this->view->Quiniela	=	$Quiniela[0];
    	$partidos				=	array();
    	foreach($Quiniela[0]->getQuinielaPartidos() as $partido)
    		$partidos[]=$partido->getPartido();
    	$this->view->partidosSeleccionados	=	$partidos;
    	//obtenemos los partidos activo del evento
    	$evento					=		$this->_em->find('Default_Model_Evento',1);
    	$this->view->partidos	=		$evento->getPartidosActivos();
    }
    
    
    public function modifyAction(){
    	//si NO es un post redireccionamos con un error
    	if(!$this->getRequest()->isPost()){
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no puede ser modificada');
    		$this->_redirect("/cuenta/control");
    	}
    	$params 						=	$this->getRequest()->getParams();
    	$params["partidosSeleccion"] 	= 	explode(",",$params["partidosSeleccion"]);
    	//leemos los parametros y verificamos que los partidos sean minimo 3 seleccionados
    	if(!count($params["partidosSeleccion"])>2)
    	{
    		//si no hay 3 partidos como mínimo, retornamos error
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no puede ser creada, selecciona mínimo 3 partidos');
    		$this->_redirect("/cuenta/control");
    	}
    	//El modelo es creado para la tabla quiniela
    	$tablaQuiniela					=	new My_Model_Quinielas();
    	//se crea una nueva quiniela con los params
    	$Quiniela 						=	$tablaQuiniela->modifyQuiniela($params);
    	//si se grabó la quiniela
    	if($Quiniela){
    		//asignamos la quiniela a la vista
    		$this->view->quiniela			=	$Quiniela;
    		//eliminamos todos los partidos actuales
    		$partidosActuales				=	$this->_em->getRepository("Default_Model_QuinielaPartidos")->findBy(array("Quiniela"=>$Quiniela)); 		
    		foreach($partidosActuales as $pa){
    			$this->_em->remove($pa);
    		}
    		$this->_em->flush();
    		
    		$fechasPartidos		=	array();
    		foreach ($params["partidosSeleccion"] as $idPartido){
    			$partido	 =	$this->_em->find('Default_Model_Partido',$idPartido);
    			$tablaQuiniela->insertarPartidoEnQuiniela($Quiniela, $partido);
    			$fechasPartidos[] = ($partido->getFechaPartido()->getTimestamp() );
    		}
    		//grabamos la fecha  primera que es la fecha de cierre
    		$fechas	=  array_unique($fechasPartidos);
    		sort($fechas);
    		$fechas[0];
    		$fechaCierre	=	new DateTime();
    		$fechaCierre->setTimestamp($fechas[0]);
    		$fechaCierre->modify('-1 day');
    		$Quiniela->setFechaCierre($fechaCierre);
    		$this->_em->persist($Quiniela);
    		$this->_em->flush();
    		//redireccionamos al preview
    		$this->_redirect("/cuenta/control/preview/id/".$Quiniela->getId());
    	}else{
    		//si no hay quiniela redireccionamos con un error
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no puede ser creada');
    		$this->_redirect("/cuenta/control");
    	}
    }
    
    
    /**
     *	Guarda la quiniela y guarda los partidos asociados a la quiniela 
     */
    public function saveAction(){
    	//si NO es un post redireccionamos con un error
    	if(!$this->getRequest()->isPost()){
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no puede ser creada');
    		$this->_redirect("/cuenta/control");
    	}
    	$params 						=	$this->getRequest()->getParams();
    	$params["partidosSeleccion"] 	= 	explode(",",$params["partidosSeleccion"]);
    	//leemos los parametros y verificamos que los partidos sean minimo 3 seleccionados
    	if(count($params["partidosSeleccion"])>2)
    		$params["id"]				=	uniqid();
    	else{
    		//si no hay 3 partidos como mínimo, retornamos error
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no puede ser creada, selecciona mínimo 3 partidos');
    		$this->_redirect("/cuenta/control/nueva-quiniela/selecciona/equipos");
    	}
    	
    	if($params["precioBoleta"]<100){
    		//si el precio es menor a 100
    		$this->_helper->flashMessenger->addMessage('error | El precio de la quiniela debe ser como mínimo 100 pesos');
    		$this->_redirect("/cuenta/control/nueva-quiniela/selecciona/equipos");
    	}
    	
    	//El modelo es creado para la tabla quiniela
    	$tablaQuiniela					=	new My_Model_Quinielas();
    	//se crea una nueva quiniela con los params
    	$Quiniela 						=	$tablaQuiniela->crearQuiniela($params);
    	//si se grabó la quiniela
    	if($Quiniela){
    		//asignamos la quiniela a la vista
    		$this->view->quiniela			=	$Quiniela;
    		//mandamos los partidos seleccionados a la vista
    		$fechasPartidos		=	array();
    		foreach ($params["partidosSeleccion"] as $idPartido){
    			$partido	 =	$this->_em->find('Default_Model_Partido',$idPartido);
    			$tablaQuiniela->insertarPartidoEnQuiniela($Quiniela, $partido);
    			$fechasPartidos[] = ($partido->getFechaPartido()->getTimestamp() );
    		}
    		//grabamos la fecha  primera que es la fecha de cierre
    		$fechas	=  array_unique($fechasPartidos);
    		sort($fechas);
    		$fechas[0];
    		$fechaCierre	=	new DateTime();
    		$fechaCierre->setTimestamp($fechas[0]);
    		$fechaCierre->modify('-1 day');
    		$Quiniela->setFechaCierre($fechaCierre);
    		$this->_em->persist($Quiniela);
    		$this->_em->flush();
    		//redireccionamos al preview
    		$this->_redirect("/cuenta/control/preview/id/".$Quiniela->getId());
    	}else{
    		//si no hay quiniela redireccionamos con un error
    		$this->_helper->flashMessenger->addMessage('error | La quiniela no puede ser creada');
    		$this->_redirect("/cuenta/control");
    	}
    }
    
    
    /**
     * Retorna la fecha via json
     */
    
    public function fechaAction(){
    	$request 	= 	$this->getRequest();
    	$param 		=	(int)$request->getParam('time',0);
    	$f=date("Y-m-d H:i:s",$param);
    	$fecha		=	$this->fecha($f,"es_MX","fullhoras");
    	$this->_helper->json->sendJson($fecha);
    }
    
    
    
    
    public function fecha($fecha,$pais="es_MX",$tipo = "full"){
    	if($fecha != null) {
    		$fecha = new Zend_Date($fecha);
    		if($tipo!="full"){
    			switch ($tipo){
    				case "short":
    					$fecha = $fecha->get(Zend_Date::DATE_MEDIUM,$pais);
    					break;
    				case "medium":
    					$fecha = strtoupper($fecha->get(Zend_Date::MONTH_NAME_SHORT,$pais)." ".$fecha->get(Zend_Date::DAY,$pais).", ".$fecha->get(Zend_Date::YEAR_SHORT,$pais). " - ".$fecha->get(Zend_Date::HOUR).":".$fecha->get(Zend_Date::MINUTE));
    					break;
    				case "fullhoras":
    					$fecha = $fecha->get(Zend_Date::DATE_FULL,$pais).", a las ".$fecha->get(Zend_Date::TIME_SHORT). " horas";
    					break;
    				case "iso":
    					$fecha	=	$fecha->getIso();
    					break;
    				case "timestamp":
    					$fecha =	$fecha->getTimestamp();
    					break;
    			}
    		}
    		else{
    			$fecha = $fecha->get(Zend_Date::DATE_FULL,$pais);
    		}
    		return $fecha;
    	} else {
    		return false;
    	}
    }
    
    /**
     * Muestra las quinielas generadas por el usuario que está actualmente logueado
     * @todo mostrar las quinielas en las que el usuario esta participando
     */
    
    public function misquinielasAction(){
    	$quiniela = $this->_em->getRepository('Default_Model_Quiniela')->findBy(array('usuario'=>$this->_auth["id"]));
    	$this->view->quiniela = $this->ordenaElementos($quiniela);
    }
    
    /**
     * Buscador de quinielas
     */
    public function buscadorquinielasAction() {
    	$parametros = array();
    	
    	if($this->getRequest()->isPost()){
    		$values = $this->getRequest()->getPost();
    		($values['nombre_quiniela']!= "")? $parametros['nombrequiniela'] = $values['nombre_quiniela']:''; 
    	}
    	
    	if(count($parametros) == 0 ){
    		
    		$quiniela = $this->_em->getRepository('Default_Model_Quiniela')->findBy(array('activo'=>1));
    		$this->view->quiniela =  $this->ordenaElementos($quiniela);
    		
    	} else {
    		
    		if($parametros['nombrequiniela'] != '')
    			
    			$quiniela = $this->_em->getRepository('Default_Model_Quiniela')->createQueryBuilder('q')
    			  			  ->where('q.titulo LIKE :name AND q.activo=1')
    			  			  ->setParameter('name','%'.trim($parametros['nombrequiniela']).'%')
    						  ->getQuery()
    						  ->getResult();
    		
    			$this->view->quiniela =  $this->ordenaElementos($quiniela);
    	}
    }
    
    /**
     * Ordena los elementos de la quiniela para mandar los datos a la vista
     * @param object $quiniela
     */
    private function ordenaElementos($quiniela){
    	$datosQuiniela = array();
    	$count = 0;
    	foreach($quiniela as $row){
    		$datosQuiniela[$count]['invitado']    = $row->verificaInvitacion($this->_auth["id"]);
    		$datosQuiniela[$count]['idquiniela']  = $row->getId();
    		$datosQuiniela[$count]['titulo']      = $row->getTitulo();
    		$datosQuiniela[$count]['descripcion'] = $row->getDescripcion();
    		$datosQuiniela[$count]['status']      = $row->getStatus();
    		$datosQuiniela[$count]['tipo']        = $row->getTipo();
    		$datosQuiniela[$count]['premio']      = $row->getPremio();
    		$datosQuiniela[$count]['precio']      = $row->getPrecioBoleta();
    		
    		$usuario = $row->getUsuario();
    		$datosQuiniela[$count]['idUsuario']      = $usuario->getId();
    		$datosQuiniela[$count]['idFacebook']     = $usuario->getfacebookid();
    		$datosQuiniela[$count]['activo']         = $row->getActivo();
    		$datosQuiniela[$count]['boletas']        = $row->getNumeroBoletasParticipando();
    		$datosQuiniela[$count]['participantes']  = count($row->getUsuariosParticipando());

    		$datosQuiniela[$count]['partidos'] = $row->getNumeroPartidos();
    		
    		$count++;
    	}
    	return $datosQuiniela;
    }
    
    /**
     * Modulo para invitar a tus amigos a participar en una Megaquiniela.
     */
    public function invitaramigosAction(){
    	
    	$validar = new My_Validador();
        $params = $this->getRequest()->getParams();
        
        $idQuinela = $validar->alphanumValido($params['invitar']);
        //Validamos que al entrar a la sección de invitar tengamos el parametro idQuiniela.
        if(isset($idQuinela) && $idQuinela != '') {
            //Validamos que idQuiniela exista.
            $quiniela = $this->_em->find('Default_Model_Quiniela', $idQuinela);
            
            if(count($quiniela) > 0) {
            	$this->view->quiniela = $quiniela; 
                $this->view->idquiniela = $idQuinela;
                $this->view->invitados = $this->_em->getRepository("Default_Model_Invitados")->findBy(array("Quiniela"=>$idQuinela));
                
                //Obtenemos el listado de amigos de facebook
                $acounts = new My_Model_Accounts();
                $this->view->frnds = $acounts->getfriendsFb();
            }  else {
                $this->_helper->flashMessenger->addMessage('error | Selecciona una quiniela valida');
                $this->_redirect("/cuenta/control");    
            }
        } else {
            $this->_redirect("/cuenta/control");
        }
    }
    
    /**
     * Envia invitación a tus amigos de facebook
     **/
    public function sendinvitacionAction() {

        $validar = new My_Validador();
    	if(!$this->getRequest()->isPost()){
    		$this->_helper->flashMessenger->addMessage('error | No fué posible enviar invitaciones');
    		$this->_redirect("/cuenta/control");
    	}
		
    	
    	$params = $this->getRequest()->getParams();
    	$params["invitados"] 	= 	explode(",",$params["invitados"]);
    	
        if(count($params["invitados"])<1){
            $this->_helper->flashMessenger->addMessage('error | Selecciona almenos un amigo para mandar la invitación');
            $this->_redirect("/cuenta/control/invitaramigos/invitar/".$params['invitar']);
        }

		$mensaje = $validar->wysiwyg($params['mensaje-invitacion']);
        $invitados = new My_Model_Invitados();
        
        //Si el usuario es propietario de la quiniela la invitación se registra automaticamente
        $quiniela = $this->_em->getRepository('Default_Model_Quiniela')->findBy(array('id'=>$params['idQuiniela'], 'usuario'=> $this->_auth["id"]));
        if (count($quiniela)>0) {
        	foreach ($params['invitados'] as $data) {
            	$invitados->saveInvitados($this->_auth["id"], $params['idQuiniela'], $data, $mensaje);
        	}
        	$this->_helper->flashMessenger->addMessage('success | Las invitaciones se enviarión correctamente.');
        	$this->_redirect("/cuenta/control");
        } else { }

        die();
    }
    
    

   
 
   
   private function partidosArray($partidos){
   	$rowset = array();
   	foreach ($partidos as $partido){
   		$row["activo"]	=	$partido->getActivo();
   		$row["id"]		=	$partido->getId();
   		$row["fecha"]	=	$partido->getFechaPartido();
   		$row["tipo"]	=	$partido->getTipoPartido();
   	
   		$equipoL		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoLocalId());
   		$equipoLocal["nombre"]		=	$equipoL->getNombre();
   		$equipoLocal["nombreCorto"]	=	$equipoL->getNombreCorto();
   		$equipoLocal["escudo"]		=	$equipoL->getEscudo();
   	
   		$equipoV		=	$this->_em->find("Default_Model_Equipo",$partido->getEquipoVisitaId());
   		$equipoVisita["nombre"]		=	$equipoV->getNombre();
   		$equipoVisita["nombreCorto"]	=	$equipoV->getNombreCorto();
   		$equipoVisita["escudo"]		=	$equipoV->getEscudo();
   	
   	
   		$row["equipoLocal"]		=	$equipoLocal;
   		$row["equipoVisita"]	=	$equipoVisita;
   	
   		array_push($rowset, $row);
   	}   	
   	return $rowset;
   }
}