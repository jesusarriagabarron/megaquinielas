<?php

/**
 * Clase para menejo de perfil de usuario [publico y privado]
 * @author pablo
 *
 */

class users_PerfilController extends My_Controller_Action
{
	private $_filename;
	
	
	public function indexAction()
	{
		$validador = new My_Validador();
		$usuario   = new My_Model_Usuario();
		
		$request = $this->getRequest();
		if ($this->getRequest()->isPost()) {
			$profile = $request->getPost();
			$images = new My_Model_Images();
			$this->_filename = $images->uploadfile($this->_auth['id']);
			
			$permitidos = array(' ','@','_','-','"','.');
			
			$data = array(
					'idUsario'     => $validador->intValido($this->_auth['id']),
					'fotoperil'    => $validador->alphanumValido($this->_filename,$permitidos),
					'acercademi'   => $validador->wysiwyg($profile['user_profile_description'], $permitidos),
					'nombreUsuario'=> $validador->alphanumValido($profile['user_profile_fullname'], $permitidos), 
					);
			$usuario->datosPerfil($data);
		}
		
		$datauser = $usuario->getDatosPerfil($this->_auth['id']);
		
		if ($datauser) {
			$this->view->nombreUsuario  = $datauser->getNombreUsuario();
			$this->view->nombreCompleto = $datauser->getNombreCompleto();
			$this->view->urlfotoperfil  = $datauser->getFotoPerfil();
			$this->view->acercademi     = $datauser->getacercademi();
		}
	}
	
	
	public function cambiocontrasenaAction(){
		$usuario   = new My_Model_Usuario();
		$request = $this->getRequest();
		
		if ($this->getRequest()->isPost()) {
			$profile = $request->getPost();
			
			$password = hash("whirlpool",addslashes(addslashes(trim($profile['user_current_password']))));
			
			if(isset($profile['user_current_password_acutal'])){
				$curren_pass = hash("whirlpool",addslashes(addslashes(trim($profile['user_current_password_acutal']))));
				$valido =  $usuario->comparePass($this->_auth['id'], $curren_pass);
				if($valido) { 
					$result =  $usuario->savepass($this->_auth['id'], $password);
					$this->_helper->flashMessenger->addMessage('success | La contrase&ntilde;a se cambio con &eacute;xito');
					$this->_redirect('/users/perfil/cambiocontrasena/');
				} else {
					$this->_helper->flashMessenger->addMessage('error | La contrase&ntilde;a actual no coincide');
					$this->_redirect('/users/perfil/cambiocontrasena/');
				}
			} else {
				$usuario->savepass($this->_auth['id'], $password);
				$this->_helper->flashMessenger->addMessage('success | La contrase&ntilde;a se asigno con &eacute;xito');
				$this->_redirect('/users/perfil/cambiocontrasena/');
			}
			
		}
		
		$datauser =  $this->_em->find("Default_Model_Usuario",$this->_auth['id']);
		$pass     =  $datauser->getPassword();
		$getFBid  =  $datauser->getfacebookid();
		
		if($pass === null && $getFBid != '' ){
			$this->view->facebookUser = '1';
		}
	}
	
	
	public function publicoAction(){
		$validador = new My_Validador();
		$username  = $validador->alphaValido($this->getRequest()->getUserParam('username',''));
		$usuario   = new My_Model_Usuario();
		
		$objUser   = $usuario->getUserbyUserName($username);
		
		if(!$objUser) {
			$this->_redirect('/');
		}
		
		$this->view->username       = $objUser[0]->getNombreUsuario();
		$this->view->nombrecompleto = $objUser[0]->getNombreCompleto();
		$this->view->acercademi     = $objUser[0]->getacercademi();
		$this->view->fotoperfil     = $objUser[0]->getFotoPerfil();
		
		
		$pub = new  My_Model_Espaciopublicitario();
		$this->view->pubs = $this->_em->getRepository('Default_Model_Espaciopublicitario')->findBy(array('usuario'=>$objUser[0]->getId()));
		
	}
} 