<?php 
defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));

require_once(CRON_PATH."/../library/conektaSDK/Conekta.php");
require_once ("correosMandrill.php");

class oxxoCheck {
	
	private $_mysql;
	private $_desarrollo=false;
	
	
	public function conectarbase(){
		if($this->_desarrollo)
			$this->_mysql	=	new mysqli("127.0.0.1", "root", "root", "megaquinielasDB");
		else
			$this->_mysql	=	new mysqli("megaquinielas.db.9633276.hostedresource.com", "megaquinielas", "m3gAq1n3!as", "megaquinielas");
	}	
	
	
	public function __construct(){
		$this->conectarbase();
		//PRUEBAS
		//Conekta::setApiKey("key_1Zd4q1qWPdUmMP1Z");
		
		//PRODUCCION
		Conekta::setApiKey("key_oLPpbuwWrxXz9h3x");
		
		if($this->_mysql->connect_errno){
				throw new Exception("Error al conectar la base ".$this->_mysql->error);
		}
	}
	
	/**
	 * Devuelve todos los pagos de Oxxo donde las boletas estan inactivas
	 */
	public function getPagosOxxo24horas(){
		$sql	=	"	SELECT  		B.id as idBoleta,B.status,B.idQuiniela,B.chargeId,C.status as statusCargo,
										C.payment_method,C.expires_at,C.amount,B.idUsuario
						FROM 			Boleta B inner join Cargo C 
						ON 				B.chargeId = C.chargeId
						WHERE			C.payment_method = 'oxxo'
						AND				B.status=0";
		
		$pagosOxxo		=		$this->_mysql->query($sql);
		return $pagosOxxo;		
	}
	
	/** verifica el status de los pagos en Conekta 
	 * 
	 */
	public function verificaPagos(){
		//obtenemos los pagos oxxos de boletas inactivas
		$pagos 	= 	$this->getPagosOxxo24horas();
		$pagos->data_seek(0);
		//recorremos los pagos
		$correo	=	new correosMandrill();
		while($pago	=	$pagos->fetch_assoc()){
			//regresamos el cargo de conecta
			$charge = Conekta_Charge::find($pago["chargeId"]);
			//guardamos las variables
			$chargeId		=	$charge->id;
			$idBoleta		=	$pago["idBoleta"];
			$idQuiniela		=	$pago["idQuiniela"];
			$statusCargo	=	$charge->status;
			$montoPago		=	$pago["amount"];
			$idUsuario		=	$pago["idUsuario"];
			//Obtenemos los datos del usuario
			$sql = " 	SELECT	nombreUsuario,email,facebookid FROM Usuario WHERE id={$idUsuario} LIMIT 1";
			$usuarioRow	=	$this->_mysql->query($sql);
			$user = $usuarioRow->fetch_assoc();
			$parametros["email"]			=	$user["email"];
			$parametros["nombreUsuario"]	=	$user["nombreUsuario"];
			$parametros["idFacebook"]		=	$user["facebookid"];
			$parametros["monto"]			=	$montoPago;
			$parametros["idBoleta"]			=	$idBoleta;
			$parametros["idQuiniela"]		=	$idQuiniela;
			
			//si el cargo esta pagado
			if($statusCargo=="paid"){
				//activamos la boleta
				
				$sql	=	"	UPDATE 	Boleta
								SET		status=1
								WHERE	id={$idBoleta}
								LIMIT	1";
				$this->_mysql->query( $sql );
				
				//actualizamos el cargo
				$sql	=	"	UPDATE 	Cargo
								SET		status='{$statusCargo}'
								WHERE	chargeId='{$pago["chargeId"]}'
								LIMIT	1";
				$this->_mysql->query( $sql );
				
				//Actualizamos el PREMIO de la quiniela por el numero de boletas activas
				$sql	=	"	UPDATE	Quiniela
								SET		premio	=	((	SELECT		count(id) as boletas 
														FROM 		Boleta 
														WHERE 		status=1 
														AND			idQuiniela='{$idQuiniela}'  
													 )* precioBoleta )
										,
										activo	=	1
								WHERE	id='{$idQuiniela}'";
				$this->_mysql->query($sql);
				
				//Enviamos el correo de confirmación
				$correo->enviarConfirmacionPago($parametros);
				
			}//fin   IF
		}//fin  FOREACH
	}
	
}

$oxxo 	= new oxxoCheck();
$oxxo->verificaPagos();
